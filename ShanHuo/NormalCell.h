//
//  NormalCell.h
//  FreelanceProject
//
//  Created by heyong on 7/20/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (copy, nonatomic) NSString *task;

@end
