//
//  SHUser.m
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHUser.h"
#import "SHTag.h"

@implementation SHUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"userID": @"id",
             @"username": @"username",
             @"name": @"name",
             @"tags": @"tags",
             @"rank": @"rank",
             @"gender": @"gender",
             @"age": @"age",
             @"allowContacts": @"allowContacts",
             @"cityID": @"city_id",
             @"districtID": @"district_id",
             @"provinceID": @"province_id",
             @"vipExpireDate": @"vip_expire"
    };
}

- (instancetype)initWithGKUser:(GKUser *)user {
    self = [super init];
    if (self) {
        _userID = @(user.userID);
        _username = user.username;
        _name = user.name;
    }
    return self;
}

+ (NSValueTransformer *)tagsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[SHTag class]];
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    dateFormatter.locale = [NSLocale currentLocale];
    return dateFormatter;
}

+ (NSValueTransformer *)vipExpireDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

@end
