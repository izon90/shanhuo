//
//  PushView.m
//  FreelanceProject
//
//  Created by heyong on 7/22/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "PushView.h"

@implementation PushView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.cancelButton.layer.borderColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f].CGColor;
    self.cancelButton.layer.borderWidth = 0.5;
    self.viewButton.layer.borderWidth = 0.5;
    self.viewButton.layer.borderColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f].CGColor;
    self.viewButton.backgroundColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
    [self.viewButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}

@end
