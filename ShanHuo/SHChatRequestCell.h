//
//  SHChatRequestCell.h
//  ShanHuo
//
//  Created by heyong on 15/8/5.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RongIMKit.h>

@class SHChatRequestCell;

typedef void(^ChatRequestCellAgreeBlock)(SHChatRequestCell *cell, UIView *agreeView);
typedef void(^ChatRequestCellRefuseBlock)(SHChatRequestCell *cell, UIView *refuseView);

@interface SHChatRequestCell : RCConversationBaseCell

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *agreeView;
@property (weak, nonatomic) IBOutlet UIView *refuseView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;

@property (copy, nonatomic) ChatRequestCellAgreeBlock agreeBlock;
@property (copy, nonatomic) ChatRequestCellRefuseBlock refuseBlock;

@end
