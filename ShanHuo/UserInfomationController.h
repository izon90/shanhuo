//
//  TableViewController.h
//  Demo
//
//  Created by heyong on 15/7/28.
//  Copyright (c) 2015年 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHUser.h"

@class UserInfomationController;

@protocol UserInfomationControllerDelegate <NSObject>

- (void)userInformationControllerDidChangeUser:(UserInfomationController *)controller;

@end

@interface UserInfomationController : UITableViewController

@property (strong, nonatomic) SHUser *user;
@property (strong, nonatomic) NSArray *datasource;

@property (weak, nonatomic) id <UserInfomationControllerDelegate> delegate;

@end
