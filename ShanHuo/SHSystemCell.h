//
//  SHSystemCell.h
//  ShanHuo
//
//  Created by heyong on 15/8/6.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RongIMKit.h>

@interface SHSystemCell : RCConversationBaseCell
@property (weak, nonatomic) IBOutlet UIImageView *systemImageView;
@property (weak, nonatomic) IBOutlet UILabel *systemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *systemMessageLabel;

@end
