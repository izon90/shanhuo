//
//  SHTag.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SHTag : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *tagID;
@property (nonatomic, copy, readonly) NSString *fullname;

@end
