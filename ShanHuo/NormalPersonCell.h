//
//  NormalPersonCell.h
//  ShanHuo
//
//  Created by heyong on 15/8/14.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalPersonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@end
