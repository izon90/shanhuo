//
//  NormalCell.m
//  FreelanceProject
//
//  Created by heyong on 7/20/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "NormalCell.h"

@implementation NormalCell

- (void)setTask:(NSString *)task {
    if (_task != task) {
        _task = task;
        self.actionLabel.text = _task;
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
