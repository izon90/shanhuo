//
//  SHContactRequest.h
//  ShanHuo
//
//  Created by heyong on 15/8/11.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SHContactRequest : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *senderUserID;
@property (nonatomic, copy, readonly) NSNumber *targetUserID;
@property (nonatomic, copy, readonly) NSNumber *status;
@property (nonatomic, copy, readonly) NSNumber *requestID;

@end
