//
//  ChangeNameController.m
//  ShanHuo
//
//  Created by heyong on 7/27/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "ChangeNameController.h"

@interface ChangeNameController ()

@property (weak, nonatomic) IBOutlet UITextField *changeNameTextField;

@end

@implementation ChangeNameController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(confirmChangeNameAction)];
}

- (void)confirmChangeNameAction {
    if ([self.changeNameTextField.text length] > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(changeNameController:didChangeName:)]) {
            [self.delegate changeNameController:self didChangeName:self.changeNameTextField.text];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return @"最长不能超过10个字哦";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
