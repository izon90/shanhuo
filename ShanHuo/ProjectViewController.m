//
//  CaseViewController.m
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "ProjectViewController.h"
#import "CaseCell.h"
#import <JTSImageViewController/JTSImageViewController.h>
#import "ImageTapGestureRecognizer.h"
#import <MediaPlayer/MediaPlayer.h>
#import "SHMoviePlayerViewController.h"
#import "SHNetworkEngine.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "SHProject.h"
#import "CreateProjectViewController.h"
#import "UIImageView+WebCache.h"

@interface ProjectViewController () <UITableViewDataSource, UITableViewDelegate, JTSImageViewControllerDismissalDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *datasource;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayerController;

@end

@implementation ProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didCreateNewProject:) name:@"DidCreateNewProject" object:nil];
    
    self.edgesForExtendedLayout = UIRectEdgeBottom;
    
    NSNumber *currentUserID = @([App shared].currentUser.userID);
    
    self.title = [self.user.userID intValue] == [currentUserID intValue] ? @"我的案例" : @"他的案例";
    
    if ([self.user.userID intValue] == [currentUserID intValue]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布案例" style:UIBarButtonItemStylePlain target:self action:@selector(createNewProject)];
    }
    
    self.tableView.layer.borderColor = [UIColor colorWithRed:0.910f green:0.918f blue:0.922f alpha:1.0f].CGColor;
    self.tableView.layer.borderWidth = 0.5;
    
    [self reloadData];
}

- (void)reloadData {
    __weak typeof(self) wself = self;
    
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getUserProjectsWithUserID:self.user.userID block:^(NSArray *objects, NSError *error) {
        if (objects && [objects count] > 0) {
            wself.datasource = objects;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = @"没有数据";
                hud.margin = 10.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            });
        }
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = @"网络错误";
                hud.margin = 10.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            });
        }
    }];
}

- (void)didCreateNewProject:(NSNotification *)notif {
    [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.tabBarController.tabBar.hidden) {
        [self.tabBarController.tabBar setHidden:NO];
    }
}

- (void)createNewProject {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second" bundle:nil];
    CreateProjectViewController *createProjectViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProjectViewController"];
    [self.navigationController pushViewController:createProjectViewController animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.datasource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CaseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[CaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    SHProject *project = self.datasource[indexPath.section];
    cell.contentLabel.text = project.content ? project.content : @"";
    NSArray *files = project.files;
    
    NSInteger numberOfImages = [files count];
    
    CGFloat imageWidth = cell.imageViews.frame.size.width/3;
    
    cell.imageViews.contentSize = CGSizeMake(numberOfImages * (imageWidth + 5), imageWidth);
    
    CGRect previousImageViewFrame = CGRectZero;
    BOOL previousImageViewExist = NO;
    
    for (int i = 0; i < numberOfImages; i++) {
        SHFile *file = files[i];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        ImageTapGestureRecognizer *tap = [[ImageTapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTap:)];
        
        NSString *path = file.path;
        NSString *qt = [path substringWithRange:NSMakeRange(path.length-2, 2)];
        
        NSString *imagePath;
        
        if ([qt isEqualToString:@"qt"]) {
            tap.isMovie = [NSNumber numberWithBool:YES];
            tap.movieURL = [NSURL URLWithString:[[GKConfig config] URLStringWithPath:file.path]];
            imagePath = [[GKConfig config] URLStringWithPath:file.preview];
        } else {
            imagePath = [[GKConfig config] URLStringWithPath:file.path];
            tap.isMovie = [NSNumber numberWithBool:NO];
        }
        
        imageView.userInteractionEnabled = YES;
        [imageView addGestureRecognizer:tap];
        
        if (previousImageViewExist) {
            imageView.frame = CGRectMake(previousImageViewFrame.origin.x + previousImageViewFrame.size.width + 5, 0, imageWidth, imageWidth);
        } else {
            imageView.frame = CGRectMake(0, 0, imageWidth, imageWidth);
        }
        
        NSURL *imageURL = [NSURL URLWithString:imagePath];
        [imageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"building.png"]];
        
        previousImageViewFrame = imageView.frame;
        previousImageViewExist = YES;
        [cell.imageViews addSubview:imageView];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SHProject *project = self.datasource[indexPath.section];
    NSString *content = project.content ? project.content : @"";
    CGFloat contentHeight = 0;
    if ([content length] > 0) {
        CGRect rect = [content boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.tableView.bounds)-24, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil];
        contentHeight = rect.size.height;
    }
    CGFloat imageHeight = self.tableView.bounds.size.width/3 - 15;
    return imageHeight + contentHeight + 36;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SHProject *project = self.datasource[section];

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width - 24, 44)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, view.bounds.size.width, view.bounds.size.height)];
    label.text = project.title ? project.title : @"";
    [view addSubview:label];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)imageViewTap:(ImageTapGestureRecognizer *)sender {
    UIImageView *imageView = (UIImageView *)sender.view;

    if ([sender.isMovie boolValue]) {
//        MPMoviePlayerController *playerController = [[MPMoviePlayerController alloc] init];
//        playerController.movieSourceType = MPMovieSourceTypeStreaming;
//        playerController.contentURL = [NSURL URLWithString:@"http://km.support.apple.com/library/APPLE/APPLECARE_ALLGEOS/HT1211/sample_iTunes.mov"];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
//        self.moviePlayerController = playerController;
//        [playerController.view setFrame:self.view.bounds];
//        [playerController prepareToPlay];
//        [self.view addSubview:playerController.view];
//        [playerController play];
        SHMoviePlayerViewController *playerViewController = [[SHMoviePlayerViewController alloc] init];
        playerViewController.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
        playerViewController.moviePlayer.contentURL = sender.movieURL;
        [playerViewController.moviePlayer shouldAutoplay];
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:playerViewController];
//        [self presentViewController:nav animated:YES completion:nil];
        [self.navigationController pushViewController:playerViewController animated:YES];
    } else {
        JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
        imageInfo.image = imageView.image;
        imageInfo.referenceRect = imageView.frame;
        imageInfo.referenceView = self.view;
        
        JTSImageViewController *imageViewController = [[JTSImageViewController alloc] initWithImageInfo:imageInfo mode:JTSImageViewControllerMode_Image backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
        imageViewController.dismissalDelegate = self;
        [imageViewController showFromViewController:self transition:JTSImageViewControllerTransition_FromOffscreen];
    }
}

#pragma mark - JTSImageViewControllerDismissalDelegate

- (void)imageViewerDidDismiss:(JTSImageViewController *)imageViewer {
//    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
