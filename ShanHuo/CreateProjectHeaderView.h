//
//  CreateProjectHeaderView.h
//  ShanHuo
//
//  Created by heyong on 15/7/31.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHTextField.h"
#import <SZTextView/SZTextView.h>

@interface CreateProjectHeaderView : UIView

@property (weak, nonatomic) IBOutlet SHTextField *titleTextField;
@property (weak, nonatomic) IBOutlet SZTextView *contentTextView;

@end
