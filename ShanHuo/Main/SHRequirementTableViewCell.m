//
//  SHRequirementTableViewCell.m
//  ShanHuo
//
//  Created by 小悟空 on 6/24/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHRequirementTableViewCell.h"
#import "SHTag.h"
#import "SHTagView.h"

@implementation SHRequirementTableViewCell

- (IBAction)followBriefAction:(id)sender {
    if (self.followBriefBlock) {
        self.followBriefBlock(self, self.brief);
    }
}
- (IBAction)requestToJoinAction:(id)sender {
    if (self.requestToJoinBriefBlock) {
        self.requestToJoinBriefBlock(self, self.brief);
    }
}

- (void)setBrief:(SHBrief *)brief {
    if (_brief != brief) {
        _brief = brief;
    }
    self.userNameLabel.text = _brief.user.name ? _brief.user.name : _brief.user.username;
    self.contentLabel.text = _brief.content;
    self.startDateLabel.text = [[SHBrief dateFormatter] stringFromDate:_brief.startAt];
    self.budgetLabel.text = [NSString stringWithFormat:@"¥ %@", _brief.price];
}

- (void)setBriefTags:(NSArray *)briefTags {
    if (_briefTags != briefTags) {
        _briefTags = briefTags;
    }
    NSLog(@"%@", _requirementTags);
    self.requirementTags.orientation  = CSLinearLayoutViewOrientationHorizontal;
    
    CGFloat totalWidth = 0;
    
    [self.requirementTags removeAllItems];
    
    for (int i = 0; i < [_briefTags count]; i++) {
        SHTag *tag = _briefTags[i];
        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag.fullname];
        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} range:NSMakeRange(0, tag.fullname.length)];
        CGRect requirementRect = [attribtedTag boundingRectWithSize:CGSizeMake(self.requirementTags.frame.size.width, 16) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        if (totalWidth + requirementRect.size.width <= 250) {
            [self.requirementTags addItem:[self requirementLayoutItemWithTag:tag]];
//            [self.employerTags addItem:[self layoutItemWithTag:tag]];
            totalWidth += requirementRect.size.width + 8;
        } else {
            break;
        }
    }
}

//- (void)setUserTags:(NSArray *)userTags {
//    if (_userTags != userTags) {
//        _userTags = userTags;
//    }
//    NSLog(@"%@", _userTags);
//    self.employerTags.orientation = CSLinearLayoutViewOrientationHorizontal;
//    
//    CGFloat totalWidth = 0;
//    
//    [self.employerTags removeAllItems];
//    
//    for (int i = 0; i < [_userTags count]; i++) {
//        SHTag *tag = _userTags[i];
//        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag.fullname];
//        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} range:NSMakeRange(0, tag.fullname.length)];
//        CGRect requirementRect = [attribtedTag boundingRectWithSize:CGSizeMake(self.employerTags.frame.size.width, 16) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//        if (totalWidth + requirementRect.size.width <= 250) {
//            [self.employerTags addItem:[self layoutItemWithTag:tag]];
//            totalWidth += requirementRect.size.width + 8;
//        } else {
//            break;
//        }
//    }
//}

- (void)awakeFromNib {
    // Initialization code
    [self layoutIfNeeded];
}

- (CSLinearLayoutItem *)layoutItemWithTag:(SHTag *)tag {
    SHTagView *tagView = [[SHTagView alloc] initWithTag:tag];
    tagView.borderColor = SHColor.tintColor;
    tagView.borderWidth = 1;
    CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:tagView];
    item.padding = CSLinearLayoutMakePadding(0.0, 0.0, 0.0, 5.0);
    item.fillMode = CSLinearLayoutItemFillModeNormal;
    item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    return item;
}

- (CSLinearLayoutItem *)requirementLayoutItemWithTag:(SHTag *)tag {
    SHTagView *tagView = [[SHTagView alloc] initWithTag:tag];
    tagView.backgroundColor = SHColor.tintColor;
    tagView.cornerRadius = 3.0f;
    tagView.tagLabel.textColor = UIColor.whiteColor;
    CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:tagView];
    item.padding = CSLinearLayoutMakePadding(0.0, 0.0, 0.0, 5.0);
    item.fillMode = CSLinearLayoutItemFillModeNormal;
    item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    return item;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
