//
//  SHRequirementTableViewCell.h
//  ShanHuo
//
//  Created by 小悟空 on 6/24/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHBrief.h"
#import "SHTag.h"
#import "HCSStarRatingView.h"

@class SHRequirementTableViewCell;

typedef void(^RequstToJoinBriefBlock)(SHRequirementTableViewCell *cell, SHBrief *brief);
typedef void(^FollowBriefBlock)(SHRequirementTableViewCell *cell, SHBrief *brief);

@interface SHRequirementTableViewCell : UITableViewCell

//@property (strong, nonatomic) IBOutlet CSLinearLayoutView *employerTags;
@property (strong, nonatomic) IBOutlet CSLinearLayoutView *requirementTags;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;

@property (strong, nonatomic) SHBrief *brief;
@property (copy, nonatomic) NSArray *briefTags;
//@property (copy, nonatomic) NSArray *userTags;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *budgetLabel;
@property (weak, nonatomic) IBOutlet UIButton *requestToJoinButton;
@property (weak, nonatomic) IBOutlet UIButton *followBriefButton;

@property (copy, nonatomic) FollowBriefBlock followBriefBlock;
@property (copy, nonatomic) RequstToJoinBriefBlock requestToJoinBriefBlock;

- (CSLinearLayoutItem *)requirementLayoutItemWithTag:(SHTag *)tag;

@end
