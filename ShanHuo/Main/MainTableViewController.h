//
//  MainTableViewController.h
//  ShanHuo
//
//  Created by 小悟空 on 6/15/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LibraryListTypeRequirement,
    LibraryListTypePeople
} LibraryListType;

@interface MainTableViewController : UITableViewController
@property (strong, nonatomic) id<GKUserService> userService;

@property (assign, nonatomic) LibraryListType libraryListType;
- (IBAction)segmentedControlEventValueChanged:(id)sender;
@end
