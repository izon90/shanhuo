//
//  MainTableViewController.m
//  ShanHuo
//
//  Created by 小悟空 on 6/15/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "MainTableViewController.h"
#import "SHRequirementTableViewCell.h"
#import "MainPeopleTableViewCell.h"
#import "SHNetworkEngine.h"
#import "SHUser.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <MJRefresh/MJRefresh.h>
#import "ProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "CreateBriefViewController.h"
#import <RongIMKit/RongIMKit.h>
#import "GKConfig.h"

@interface MainTableViewController () <RCIMUserInfoDataSource>

@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) NSMutableArray *briefs;
@property (copy, nonatomic) NSNumber *currentPage;
@property (strong, nonatomic) MBProgressHUD *progressHud;

@end

@implementation MainTableViewController

objection_requires_sel(@selector(userService))

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.libraryListType = LibraryListTypeRequirement;
    }
    return self;
}

- (NSMutableArray *)users {
    if (!_users) {
        _users = [NSMutableArray new];
    }
    return _users;
}

- (NSMutableArray *)briefs {
    if (!_briefs) {
        _briefs = [NSMutableArray new];
    }
    return _briefs;
}

- (void)viewDidAppear:(BOOL)animate {
    [super viewDidAppear:animate];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirstRun"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:@"UserDidLogin" object:nil];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    [[JSObjection defaultInjector] injectDependencies:self];
    
    if (![[self.userService restore] authorized]) {
        [self.navigationController performSegueWithIdentifier:@"Login" sender:self];
    } else {
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"RongCloudToken"];
        if (token) {
            [self connectRongCloudWithToken:token];
        }
    }
    
    self.title = @"快活";
    
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        NSInteger page = [self.currentPage integerValue];
        page += 1;
        self.currentPage = @(page);
        [self loadData];
    }];
    [self refresh:refreshControl];
}

- (void)connectRongCloudWithToken:(NSString *)token {
    [[RCIM sharedRCIM] connectWithToken:token success:^(NSString *userId) {
        
        NSLog(@"success connect to cloud with userId: %@", userId);
    } error:^(RCConnectErrorCode status) {
        NSLog(@"error: %@", @(status));
    } tokenIncorrect:^{
        NSLog(@"token incorrect");
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)createBreif {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second" bundle:nil];
    CreateBriefViewController *createBrief = [storyboard instantiateViewControllerWithIdentifier:@"CreateBriefViewController"];
    [self.navigationController pushViewController:createBrief animated:YES];
}

- (void)userDidLogin:(NSNotification *)notif {
    [self refresh:self.refreshControl];
}

- (void)refresh:(UIRefreshControl *)control {
    if ([App shared].currentUser) {
        [[RCIM sharedRCIM] setUserInfoDataSource:self];
        self.currentPage = @(1);
        [self loadData];
    }
}

- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion {
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([userId intValue]) block:^(SHUser *user, NSError *error) {
        RCUserInfo *userInfo = [[RCUserInfo alloc] init];
        userInfo.userId = [NSString stringWithFormat:@"%@", user.userID];
        userInfo.name = user.name ? user.name : user.username;
        userInfo.portraitUri = [NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID];
        completion(userInfo);
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        });
    }];
}

- (void)loadData {
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    
    __weak typeof(self) wself = self;
    
    __block dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_enter(group);
    [[SHNetworkEngine sharedEngine] getAllUsersWithPage:self.currentPage block:^(NSArray *objects, NSError *error) {
        if (objects) {
            if ([self.currentPage integerValue] == 1) {
                wself.users = [objects mutableCopy];
            } else {
                [wself.users addObjectsFromArray:objects];
            }
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);
    [[SHNetworkEngine sharedEngine] getAllBriefsWithPage:self.currentPage block:^(NSArray *objects, NSError *error) {
        if (objects) {
            if ([self.currentPage integerValue] == 1) {
                wself.briefs = [objects mutableCopy];
            } else {
                [wself.briefs addObjectsFromArray:objects];
            }
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [wself.refreshControl endRefreshing];
        if ([self.currentPage integerValue] > 1) {
            [self.tableView.footer endRefreshing];
        }
        [self.tableView reloadData];
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    switch (self.libraryListType) {
        case LibraryListTypeRequirement:
            return [self.briefs count];
            break;
        case LibraryListTypePeople:
        default:
            return [self.users count];
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier;
    switch (self.libraryListType) {
        case LibraryListTypeRequirement:
        {
            identifier = @"MainRequirementTableViewCell";
            SHRequirementTableViewCell *requirementCell =
            (SHRequirementTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier
                                                                          forIndexPath:indexPath];
            SHBrief *brief = self.briefs[indexPath.row];
            requirementCell.brief = brief;
            requirementCell.briefTags = brief.tags;
            requirementCell.ratingView.enabled = NO;
            requirementCell.ratingView.minimumValue = 0.0;
            requirementCell.ratingView.maximumValue = 5.0;
            requirementCell.ratingView.value = [brief.user.rank floatValue];
            NSURL *avatarURl = [NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, brief.user.userID]];
            NSLog(@"%@", avatarURl);
            [requirementCell.avatarImageView sd_setImageWithURL:avatarURl placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
            
            requirementCell.requestToJoinBriefBlock = ^(SHRequirementTableViewCell *cell, SHBrief *brief) {
                [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
                [[SHNetworkEngine sharedEngine] getJoinBriefStatusWithOwerID:brief.user.userID userID:@([App shared].currentUser.userID) briefID:brief.briefID block:^(SHBriefRequest *request, NSError *error) {
                    if (request) {
                        [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                        switch ([request.status integerValue]) {
                            case 0:
                                [self alertWithTitle:@"正在申请" cancelButton:NO okBlock:^(UIAlertController *sender) {
                                    [sender dismissViewControllerAnimated:YES completion:nil];
                                }];
                                break;
                            case 1:
                                [self alertWithTitle:@"已经同意" cancelButton:NO okBlock:^(UIAlertController *sender) {
                                    [sender dismissViewControllerAnimated:YES completion:nil];
                                }];
                                break;
                            case 2:
                                [self alertWithTitle:@"已经拒绝" cancelButton:NO okBlock:^(UIAlertController *sender) {
                                    [sender dismissViewControllerAnimated:YES completion:nil];
                                }];
                                break;
                            default:
                                break;
                        }
                    } else {
                        if (error.code == 1000) {
                            [self alertWithTitle:@"请求加入需求" cancelButton:YES okBlock:^(UIAlertView *sender) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
                                });
                                [[SHNetworkEngine sharedEngine] requestToJoinBrief:brief.briefID status:@(0) block:^(SHBriefRequest *request, NSError *error) {
                                    if (request) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                                            [[SHHelper sharedHelper] showHudWithMessage:@"申请成功" fromView:self.navigationController.view];
                                        });
                                    } else {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                                            [[SHHelper sharedHelper] showHudWithMessage:@"网络错误" fromView:self.navigationController.view];
                                        });
                                    }
                                }];
                            }];
                        } else {
                            [[SHHelper sharedHelper] showHudWithMessage:@"网络错误" fromView:self.tableView];
                        }
                    }
                }];
            };
            
            requirementCell.followBriefBlock = ^(SHRequirementTableViewCell *cell, SHBrief *brief) {
                
            };
            
            return requirementCell;
        }
            break;
        case LibraryListTypePeople:
        default:
        {
            identifier = @"MainPeopleTableViewCell";
            MainPeopleTableViewCell *cell =
            (MainPeopleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier
                                                                       forIndexPath:indexPath];
            SHUser *user = self.users[indexPath.row];
            NSURL *avatarURl = [NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID]];
            [cell.avatarImageView sd_setImageWithURL:avatarURl placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
            [cell setUser:user];
            return cell;
        }
            break;
    }
}

- (void)alertWithTitle:(NSString *)title cancelButton:(BOOL)cancelButton okBlock:(void(^)(id sender))okBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    if (cancelButton) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:cancelAction];
    }
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        okBlock(alert);
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.libraryListType) {
        case LibraryListTypeRequirement:
            return 240;
            break;
            
        default:
            return 60;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.libraryListType) {
        case LibraryListTypeRequirement:
            
            break;
        case LibraryListTypePeople:
        default:
        {
            ProfileViewController *profile = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            SHUser *user = self.users[indexPath.row];
            profile.user = user;
            [self.navigationController pushViewController:profile animated:YES];
        }
            break;
    }
}

- (IBAction)segmentedControlEventValueChanged:(id)sender
{
    NSInteger selected = ((UISegmentedControl *)sender).selectedSegmentIndex;
    
    if (selected != self.libraryListType) {
        switch (selected) {
            case 0: {
                self.libraryListType = LibraryListTypeRequirement;
                break;
            }
            case 1:
            default: {
                self.libraryListType = LibraryListTypePeople;
                break;
            }
        }
        
        [self.tableView reloadData];
    }
}

@end
