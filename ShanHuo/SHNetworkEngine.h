//
//  SHNetworkEngine.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHProject.h"
#import "Tag.h"
#import "SHContactRequest.h"
#import "SHBriefRequest.h"

//typedef enum : NSUInteger {
//    JoinBriefStatusOngoing,
//    JoinBriefStatusApprove,
//    JoinBriefStatusDeny
//} JoinBriefStatus;

@class SHUser;
@class SHBrief;

typedef void(^UsersBlock)(NSArray *objects, NSError *error);
typedef void(^BriefsBlock)(NSArray *objects, NSError *error);
typedef void(^ProjectsBlock)(NSArray *objects, NSError *error);
typedef void(^UserBlock)(SHUser *user, NSError *error);
typedef void(^UserMatchCountBlock)(NSNumber *totalCount, NSNumber *matchCount, NSError *error);
typedef void(^SuccessBlock)(BOOL success, NSError *error);
typedef void(^BriefBlock)(SHBrief *brief, NSError *error);
typedef void(^ProjectFileBlock)(NSArray *files, NSError *error);
typedef void(^ProjectBlock)(SHProject *project, NSError *error);
typedef void(^FilesBlock)(NSArray *files, NSError *error);
typedef void(^RequestBlock)(SHBriefRequest *request, NSError *error);
typedef void(^ContactUserBlock)(SHContactRequest *request, NSError *error);

@interface SHNetworkEngine : NSObject

+ (instancetype)sharedEngine;

- (void)getUserInfoWithUserID:(NSNumber *)userID block:(UserBlock)block;

- (void)getAllUsersWithPage:(NSNumber *)page block:(UsersBlock)block;
- (void)getAllBriefsWithPage:(NSNumber *)page block:(BriefsBlock)block;
- (void)getProjectsWithPage:(NSNumber *)page block:(ProjectsBlock)block;

- (void)getUserBriefsWithUserID:(NSNumber *)userID page:(NSNumber *)page block:(BriefsBlock)block;
- (void)getUserProjectsWithUserID:(NSNumber *)userID block:(ProjectsBlock)block;

- (void)updateUserInfoWithName:(NSString *)name age:(NSNumber *)age gender:(NSNumber *)gender provinceID:(NSNumber *)provinceID cityID:(NSNumber *)cityID districtID:(NSNumber *)districtID block:(UserBlock)block;

- (void)getuserMathWithBriefID:(NSNumber *)briefID renew:(NSNumber *)renew block:(UsersBlock)block;
- (void)getUserMatchCountWithBriefID:(NSNumber *)briefID block:(UserMatchCountBlock)block;

- (void)uploadUserAvatarWithImage:(UIImage *)image block:(SuccessBlock)block;

- (void)createNewBriefWithContent:(NSString *)content price:(NSNumber *)price startAt:(NSDate *)startat block:(BriefBlock)block;

- (void)touchNewBriefWithBlock:(BriefBlock)block;

- (void)uploadBriefFileWithFiles:(NSArray *)files resourceID:(NSNumber *)resourceID resourceType:(NSString *)type preview:(UIImage *)previewImage block:(ProjectFileBlock)block;
- (void)uploadProjectFileWithFiles:(NSArray *)files projectID:(NSNumber *)projectID block:(FilesBlock)block;
- (void)rateUserID:(NSNumber *)userID rank:(NSNumber *)rank content:(NSString *)content block:(UserBlock)block;

- (void)createProjectTouchWithTitle:(NSString *)title content:(NSString *)content block:(ProjectBlock)block;
- (void)updateProjectWithProjectID:(NSNumber *)projectID title:(NSString *)title content:(NSString *)content block:(ProjectBlock)block;

- (void)requestToJoinBrief:(NSNumber *)briefID status:(NSNumber *)status block:(RequestBlock)block;
- (void)approveOrDenyRequestToJoinBrief:(NSNumber *)briefID status:(NSNumber *)status completion:(RequestBlock)block;
- (void)userContactRequestWithSenderUserId:(NSNumber *)senderUserId targetUserId:(NSNumber *)targetUserId status:(NSNumber *)status completion:(ContactUserBlock)block;
- (void)userContactRequestQueryWithUserId:(NSNumber *)userId block:(ContactUserBlock)block;

- (void)getJoinBriefStatusWithOwerID:(NSNumber *)ownerID userID:(NSNumber *)userID briefID:(NSNumber *)briefID block:(RequestBlock)block;

- (void)attachTag:(Tag *)tag toUser:(GKUser *)user completion:(void(^)(Tag *tag, NSError *error))completion;

@end
