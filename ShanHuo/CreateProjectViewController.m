//
//  CreateProjectViewController.m
//  ShanHuo
//
//  Created by heyong on 15/7/31.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "CreateProjectViewController.h"
#import <FSMediaPicker/FSMediaPicker.h>
#import "ProjectFileCell.h"
#import "SHNetworkEngine.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CreateProjectHeaderView.h"
#import "CreateProjectFooterView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MediaImage.h"
#import "MediaMovie.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <AVFoundation/AVFoundation.h>

@interface CreateProjectViewController () <FSMediaPickerDelegate, UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray *fileURLs;
@property (strong, nonatomic) NSMutableArray *datasource;
@property (assign, nonatomic) BOOL shouldHideFooterView;
@property (copy, nonatomic) NSString *projectContent;
@property (copy, nonatomic) NSString *projectTitle;
@property (strong, nonatomic) UIButton *uploadButton;
@property (strong, nonatomic) SZTextView *textView;
@property (strong, nonatomic) UITextField *titleTextField;

@end

@implementation CreateProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableViewTapped:)]];
    
    self.datasource = [NSMutableArray array];
    [self.datasource addObject:@""];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ProjectFileCell" bundle:nil] forCellReuseIdentifier:@"ProjectFileCell"];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    self.shouldHideFooterView = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddMedia:) name:@"DidAddMedia" object:nil];
}

- (void)didAddMedia:(NSNotification *)notif {
    if ([self.projectTitle length] > 0) {
        self.titleTextField.text = self.projectTitle;
    }
    if ([self.projectContent length] > 0) {
        self.textView.text = self.projectContent;
    }
    if ([self.projectTitle length] > 0 && [self.projectContent length] > 0) {
        [self setUploadButtonEnable:YES];
    }
}

- (void)keyboardWillShowNotification:(NSNotification *)notif {
    self.shouldHideFooterView = YES;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (void)keyboardWillHideNotification:(NSNotification *)notif {
    self.shouldHideFooterView = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (NSMutableArray *)fileURLs {
    if (!_fileURLs) {
        _fileURLs = [NSMutableArray new];
    }
    return _fileURLs;
}

- (void)tableViewTapped:(UITapGestureRecognizer *)sender {
    [self.tableView endEditing:YES];
}

- (UIImage *)moviePreviewImageWithVideoURL:(NSURL *)videoURL {
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    return thumb;
}

- (void)mediaPicker:(FSMediaPicker *)mediaPicker didFinishWithMediaInfo:(NSDictionary *)mediaInfo {
    NSString *extension = [mediaInfo[UIImagePickerControllerMediaType] pathExtension];
    NSURL *referenceURL = mediaInfo[UIImagePickerControllerReferenceURL];
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    [assetsLibrary assetForURL:referenceURL resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *representation = [asset defaultRepresentation];
        CGImageRef imageRef = representation.fullResolutionImage;
        UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
        
        if ([extension isEqualToString:@"movie"]) {
            NSURL *mediaURL = mediaInfo[UIImagePickerControllerMediaURL];
            UIImage *preview = image;
            MediaMovie *mediaMovie = [MediaMovie new];
            mediaMovie.mediaURL = mediaURL;
            mediaMovie.previewImage = preview;
            NSString *fileName = [representation filename] ? [representation filename] : @"video";
            mediaMovie.filename = fileName;
            UIImage *thumbnail = [UIImage imageWithCGImage:asset.thumbnail];
            NSData *thumbnailData;
            if (thumbnail) {
                thumbnailData = UIImageJPEGRepresentation(thumbnail, 0.5);
            } else {
                thumbnailData = UIImageJPEGRepresentation([self moviePreviewImageWithVideoURL:mediaURL], 0.5);
            }
            mediaMovie.thumbnailData = thumbnailData;
            [self.datasource insertObject:mediaMovie atIndex:0];
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else {
            MediaImage *mediaImage = [MediaImage new];
            NSData *imageData = UIImageJPEGRepresentation(mediaInfo[UIImagePickerControllerEditedImage], 0.5);
            mediaImage.data = imageData;
            NSString *fileName = [representation filename];
            mediaImage.filename = fileName;
            [self.datasource insertObject:mediaImage atIndex:0];
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DidAddMedia" object:nil];
            });
        }
    } failureBlock:^(NSError *error) {
        
    }];
    
}

- (NSString *)imagePathSavedToTempFolder:(UIImage *)image {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *timeString = [NSString stringWithFormat:@"%@", @([[NSDate date] timeIntervalSince1970])];
    path = [path stringByAppendingString:[NSString stringWithFormat:@"/image-%@.jpg", timeString]];
    NSData *data = UIImageJPEGRepresentation(image, 0.5);
    [data writeToFile:path atomically:YES];
    return path;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.datasource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [self.datasource count]-1) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.text = @"点击选择添加文件";
        cell.textLabel.userInteractionEnabled = YES;
        [cell.textLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicker:)]];
        return cell;
    } else if ([self.datasource count] > 1) {
        ProjectFileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProjectFileCell" forIndexPath:indexPath];
        id media = self.datasource[indexPath.row];
        if ([media isKindOfClass:[MediaImage class]]) {
            MediaImage *mediaImage = (MediaImage *)media;
            cell.filePreviewImageView.image = [UIImage imageWithData:mediaImage.data];
            cell.fileNameLabel.text = mediaImage.filename ? mediaImage.filename : @"";
        } else {
            MediaMovie *mediaMovie = (MediaMovie *)media;
            cell.filePreviewImageView.image = mediaMovie.previewImage;
            cell.fileNameLabel.text = mediaMovie.filename ? mediaMovie.filename : @"";
        }
        cell.filePreviewImageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.deleteView.userInteractionEnabled = YES;
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteFile:)];
        [cell.deleteView addGestureRecognizer:gesture];
        
        return cell;
    }
    return [[UITableViewCell alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return self.shouldHideFooterView ? 0 : 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 180;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CreateProjectHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:@"CreateProjectHeaderView" owner:self options:nil] firstObject];
    self.textView = view.contentTextView;
    self.titleTextField = view.titleTextField;
    view.contentTextView.delegate = self;
    view.titleTextField.delegate = self;

    RACSignal *projectTitleValidSignal = [view.titleTextField.rac_textSignal map:^id(id value) {
        return @([value length] > 0);
    }];
    RACSignal *projectContentValidSigna = [view.contentTextView.rac_textSignal map:^id(id value) {
        return @([value length] > 0);
    }];
    RACSignal *uploadValidSignal = [RACSignal combineLatest:@[projectContentValidSigna, projectTitleValidSignal] reduce:^id(NSNumber *titleValid, NSNumber *contentValid) {
        return @([titleValid boolValue] && [contentValid boolValue]);
    }];
    
    [uploadValidSignal subscribeNext:^(NSNumber *validSignal) {
        if ([validSignal boolValue]) {
//            self.uploadButton.enabled = YES;
        }
    }];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    CreateProjectFooterView *view = [[[NSBundle mainBundle] loadNibNamed:@"CreateProjectFooterView" owner:self options:nil] firstObject];
    [view.uploadButton addTarget:self action:@selector(confirmUpload:) forControlEvents:UIControlEventTouchUpInside];
//    view.uploadButton.enabled = NO;
    self.uploadButton = view.uploadButton;
    [self setUploadButtonEnable:NO];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)presentPicker:(UITapGestureRecognizer *)sender {
    FSMediaPicker *picker = [[FSMediaPicker alloc] init];
    picker.mediaType = FSMediaTypeAll;
    picker.delegate = self;
    [picker showFromView:self.tableView];
}

- (void)deleteFile:(UITapGestureRecognizer *)sender {
    
}

- (void)confirmUpload:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    __weak typeof(self) wself = self;
    [[SHNetworkEngine sharedEngine] createProjectTouchWithTitle:self.projectTitle content:self.projectContent block:^(SHProject *project, NSError *error) {
        __strong typeof(self) sself = wself;
        if (project) {
            [[SHNetworkEngine sharedEngine] uploadProjectFileWithFiles:sself.datasource projectID:project.projectID block:^(NSArray *files, NSError *error) {
                if (files) {
                    NSLog(@"%@", self);
                    [[SHNetworkEngine sharedEngine] updateProjectWithProjectID:project.projectID title:sself.projectTitle content:sself.projectContent block:^(SHProject *project, NSError *error) {
                        if (project) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
                                [self.navigationController popViewControllerAnimated:YES];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"DidCreateNewProject" object:nil];
                            });
                        } else {
                            [self showInternetErrorHUD];
                        }
                    }];
                } else {
                    [self showInternetErrorHUD];
                }
            }];
        } else {
            [self showInternetErrorHUD];
        }
    }];
}

- (void)showInternetErrorHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"网络错误";
        hud.margin = 10.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    });
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.projectContent = textView.text;
    [self setUploadButtonEnable:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.projectTitle = textField.text;
    [self setUploadButtonEnable:YES];
}

- (void)setUploadButtonEnable:(BOOL)enable {
    if (enable) {
        if ([self.projectContent length] > 0 && [self.projectTitle length] > 0) {
            self.uploadButton.enabled = YES;
            [self.uploadButton setBackgroundColor:[UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f]];
        }
    } else {
        self.uploadButton.enabled = NO;
        [self.uploadButton setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
