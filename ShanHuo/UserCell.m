//
//  UserCell.m
//  FreelanceProject
//
//  Created by heyong on 7/20/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (void)setUser:(SHUser *)user {
    if (_user != user) {
        _user = user;
        self.usernameLabel.text = _user.username;
        self.ratingView.enabled = NO;
        self.ratingView.minimumValue = 0.0;
        self.ratingView.maximumValue = 5.0;
        self.ratingView.allowsHalfStars = YES;
        self.ratingView.value = [_user.rank floatValue];
//        self.userLocationLabel.text = _user.userLocation;
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
