//
//  SHHelper.m
//  ShanHuo
//
//  Created by heyong on 15/7/30.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "SHHelper.h"
#import "MBProgressHUD.h"

@implementation SHHelper

+ (instancetype)sharedHelper {
    static SHHelper *_sharedHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHelper = [[SHHelper alloc] init];
    });
    return _sharedHelper;
}

- (void)archiverObject:(id)object toUserDefaultsWithName:(NSString *)name {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:name];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)unarchiveObjectWithUserDefaultsName:(NSString *)name {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:name];
    id object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return object;
}

- (void)showHudWithMessage:(NSString *)message fromView:(UIView *)view {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = message;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:2];
}

@end
