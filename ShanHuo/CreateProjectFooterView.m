//
//  CreateProjectFooterView.m
//  ShanHuo
//
//  Created by heyong on 8/1/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "CreateProjectFooterView.h"

@implementation CreateProjectFooterView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.uploadButton.layer.cornerRadius = 2;
    self.uploadButton.clipsToBounds = YES;
}

@end
