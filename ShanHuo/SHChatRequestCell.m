//
//  SHChatRequestCell.m
//  ShanHuo
//
//  Created by heyong on 15/8/5.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "SHChatRequestCell.h"

@implementation SHChatRequestCell

- (void)awakeFromNib {
    self.agreeView.userInteractionEnabled = YES;
    self.refuseView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *agreeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agreeGesture:)];
    [self.agreeView addGestureRecognizer:agreeGesture];
    
    UITapGestureRecognizer *refuseeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refuseGesture:)];
    [self.refuseView addGestureRecognizer:refuseeGesture];
}

- (void)agreeGesture:(UITapGestureRecognizer *)sender {
    if (self.agreeBlock) {
        self.agreeBlock(self, self.agreeView);
    }
}

- (void)refuseGesture:(UITapGestureRecognizer *)sender {
    if (self.refuseBlock) {
        self.refuseBlock(self, self.refuseView);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
