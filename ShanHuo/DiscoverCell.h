//
//  DiscoverCell.h
//  FreelanceProject
//
//  Created by heyong on 7/22/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIView *tagView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *requestLabel;

@end
