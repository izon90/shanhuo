//
//  SHFile.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SHFile : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *path;
@property (nonatomic, copy, readonly) NSNumber *fileID;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSNumber *size;
@property (nonatomic, copy, readonly) NSString *preview;

@end
