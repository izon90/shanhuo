//
//  ProjectFileCell.h
//  ShanHuo
//
//  Created by heyong on 15/7/31.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectFileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *filePreviewImageView;
@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *deleteView;

@end
