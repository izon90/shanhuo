//
//  PersonViewController.m
//  FreelanceProject
//
//  Created by heyong on 7/22/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "PersonViewController.h"
#import "PersonCell.h"
#import <RongIMKit/RongIMKit.h>
#import "SHNetworkEngine.h"
#import "SHUser.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "SHTag.h"
#import "UIImageView+WebCache.h"

@interface PersonViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *datasource;

@end

@implementation PersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PersonCell" bundle:nil] forCellReuseIdentifier:@"PersonCell"];

    __weak typeof(self) wself = self;
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getuserMathWithBriefID:self.brief.briefID renew:[NSNumber numberWithBool:false] block:^(NSArray *objects, NSError *error) {
        if (objects) {
            wself.datasource = objects;
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
                [self.tableView reloadData];
            });
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.tabBarController.tabBar.hidden) {
        [self.tabBarController.tabBar setHidden:NO];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.datasource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PersonCell *cell = (PersonCell *)[tableView dequeueReusableCellWithIdentifier:@"PersonCell" forIndexPath:indexPath];
    SHUser *user = self.datasource[indexPath.row];
    
    cell.usernameLabel.text = user.name ? user.name : user.username;
    [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID]] placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];

    NSArray *tags = user.tags;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGRect previousLabelFrame = CGRectZero;
    BOOL previousLabelExist = NO;
    
    for (int i = 0; i < [tags count]; i++) {
        SHTag *tagObject = tags[i];
        NSString *tag = tagObject.fullname;
        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag];
        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, tag.length)];
        CGRect rect = [attribtedTag boundingRectWithSize:CGSizeMake(cell.tagView.frame.size.width, 20) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        UILabel *label = [[UILabel alloc] init];
        
        if (previousLabelExist) {
            label.frame = CGRectMake(previousLabelFrame.origin.x + previousLabelFrame.size.width + 5, 0, rect.size.width+10, 20);
        } else {
            label.frame = CGRectMake(0, 0, rect.size.width+10, 20);
        }
        
        label.text = tag;
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        label.textAlignment = NSTextAlignmentCenter;
        
        previousLabelFrame = label.frame;
        previousLabelExist = YES;
        
        [cell.tagView addSubview:label];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    UIButton *randomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    randomButton.frame = view.bounds;
    [randomButton setTitle:@"重新随机（加付10%手续费）" forState:UIControlStateNormal];
    [randomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [randomButton addTarget:self action:@selector(randomButton:) forControlEvents:UIControlEventTouchUpInside];
    [randomButton setBackgroundColor:[UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f]];
    randomButton.clipsToBounds = YES;
    randomButton.layer.cornerRadius = 2;
    [view addSubview:randomButton];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SHUser *user = self.datasource[indexPath.row];
    
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] userContactRequestQueryWithUserId:user.userID block:^(SHContactRequest *request, NSError *error) {
        if (request) {
            NSNumber *status = request.status;
            if (status) {
                if (status.intValue == 1) {
                    RCConversationViewController *conversationVC = [[RCConversationViewController alloc]init];
                    conversationVC.conversationType = ConversationType_PRIVATE;
                    conversationVC.targetId = [NSString stringWithFormat:@"%@", user.userID];
                    conversationVC.userName = user.name ? user.name : user.username;
                    conversationVC.title = @"";
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                        [self.tabBarController.tabBar setHidden:YES];
                        [self.navigationController pushViewController:conversationVC animated:YES];
                    });
                } else if (status.intValue == 0) {
                    [self alertWithTitle:@"是否发送请求" cancelButton:YES okBlock:^(id sender) {
                        [self sendContactRequestWithUser:user];
                    }];
                } else if (status.intValue == 2) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                        [self alertWithTitle:@"对方拒绝" cancelButton:NO okBlock:^(UIAlertController *sender) {
                            [sender dismissViewControllerAnimated:YES completion:nil];
                        }];
                    });
                }
            } else {
                [self alertWithTitle:@"是否发送请求" cancelButton:YES okBlock:^(id sender) {
                    [self sendContactRequestWithUser:user];
                }];
            }
        }
    }];
}

- (void)sendContactRequestWithUser:(SHUser *)user {
    NSInteger currentUserID = [App shared].currentUser.userID;
    [[SHNetworkEngine sharedEngine] userContactRequestWithSenderUserId:@(currentUserID) targetUserId:user.userID status:@(0) completion:^(SHContactRequest *request, NSError *error) {
        if (request) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                [self alertWithTitle:@"请求已经发出" cancelButton:NO okBlock:^(UIAlertController *sender) {
                    [sender dismissViewControllerAnimated:YES completion:nil];
                }];
            });
        } else {
            NSString *errorMessage;
            if (error.code == 403) {
                errorMessage = error.userInfo[@"error"];
            } else {
                errorMessage = @"网络错误";
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                [self alertWithTitle:errorMessage cancelButton:NO okBlock:^(UIAlertController *sender) {
                    [sender dismissViewControllerAnimated:YES completion:nil];
                }];
            });
        }
    }];
}

- (void)alertWithTitle:(NSString *)title cancelButton:(BOOL)cancelButton okBlock:(void(^)(id sender))okBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    if (cancelButton) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
            if ([MBProgressHUD HUDForView:self.tableView]) {
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
            }
        }];
        [alert addAction:cancelAction];
    }
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        okBlock(alert);
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)randomButton:(UIButton *)sender {
    __weak typeof(self) wself = self;
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getuserMathWithBriefID:self.brief.briefID renew:[NSNumber numberWithBool:true] block:^(NSArray *objects, NSError *error) {
        if (objects) {
            wself.datasource = objects;
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
                [self.tableView reloadData];
            });
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
