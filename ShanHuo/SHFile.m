//
//  SHFile.m
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHFile.h"

@implementation SHFile

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"path": @"path",
             @"fileID": @"id",
             @"name": @"name",
             @"size": @"size",
             @"preview": @"preview"
             };
}

@end
