//
//  TableViewController.m
//  Demo
//
//  Created by heyong on 15/7/28.
//  Copyright (c) 2015年 heyong. All rights reserved.
//

#import "UserInfomationController.h"
#import <FSMediaPicker/FSMediaPicker.h>
#import "ChangeNameController.h"
#import "SHNetworkEngine.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIImageView+WebCache.h"
#import "GKConfig.h"

@interface UserInfomationController () <UIPickerViewDataSource, UIPickerViewDelegate, FSMediaPickerDelegate, ChangeNameControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UITableViewCell *genderPickerViewCell;
@property (weak, nonatomic) IBOutlet UIPickerView *genderPickerView;
@property (weak, nonatomic) IBOutlet UITableViewCell *agePickerViewCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *locationPickerViewCell;
@property (weak, nonatomic) IBOutlet UIPickerView *agePickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *locationPickerView;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (strong, nonatomic) NSArray *genders;
@property (strong, nonatomic) NSArray *ages;
@property (strong, nonatomic) NSArray *locations;

@property (copy, nonatomic) NSString *changedUserName;

@property (assign, nonatomic) BOOL genderPikcerViewHidden;

@property (copy, nonatomic) NSArray *provinces;
@property (copy, nonatomic) NSArray *cites;
@property (copy, nonatomic) NSArray *districts;


@property (assign, nonatomic) NSInteger selectedProvinceIndex;
@property (assign, nonatomic) NSInteger selectedCityIndex;
@property (assign, nonatomic) NSInteger selectedDistrictIndex;

@property (strong, nonatomic) UIImage *selectedNewImage;

@end

@implementation UserInfomationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的信息";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(confirmChangeAction)];
    
    self.genderPikcerViewHidden = YES;
    
    self.genders = @[@"男", @"女"];
    
    self.genderPickerView.tag = 101;
    self.agePickerView.tag = 102;
    self.locationPickerView.tag = 103;
    
    self.avatarImageView.userInteractionEnabled = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, self.user.userID]];
    [self.avatarImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userAvatarImageViewPressed:)];
    [self.avatarImageView addGestureRecognizer:tap];
    
    self.nicknameLabel.text = self.user.name ? self.user.name : self.user.username;
    self.genderLabel.text = [self.user.gender intValue] == 0 ? @"女" : @"男";
    self.ageLabel.text = self.user.age ? [NSString stringWithFormat:@"%@", self.user.age] : @"18";
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    });
    
    self.selectedCityIndex = 0;
    self.selectedDistrictIndex = 0;
    self.selectedProvinceIndex = 0;
    
    self.datasource = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"locations" ofType:@"plist"]];
}

- (void)changeNameController:(ChangeNameController *)controller didChangeName:(NSString *)name {
    self.nicknameLabel.text = name;
}

- (void)confirmChangeAction {
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    __weak typeof(self) wself = self;
    
    dispatch_group_t group = dispatch_group_create();
    
    if (self.selectedNewImage) {
        dispatch_group_enter(group);
        [[SHNetworkEngine sharedEngine] uploadUserAvatarWithImage:self.selectedNewImage block:^(BOOL success, NSError *error) {
            if (success) {
                NSLog(@"success upload image");
            } else {
                NSLog(@"%@", error);
            }
            dispatch_group_leave(group);
        }];
    }

    dispatch_group_enter(group);

    NSString *provinceID = self.datasource[self.selectedProvinceIndex][@"provinceID"];
    NSString *cityID = self.datasource[self.selectedProvinceIndex][@"cities"][self.selectedCityIndex][@"cityID"];
    NSString *districtID = self.datasource[self.selectedProvinceIndex][@"cities"][self.selectedCityIndex][@"districts"][self.selectedDistrictIndex][@"districtID"];
    
    [[SHNetworkEngine sharedEngine] updateUserInfoWithName:self.nicknameLabel.text age:@([self.ageLabel.text intValue]) gender:@([self.ageLabel.text intValue]) provinceID:@([provinceID intValue]) cityID:@([cityID intValue]) districtID:@([districtID intValue]) block:^(SHUser *user, NSError *error) {
        if (user) {
            wself.user = user;
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
            [self.navigationController popViewControllerAnimated:YES];
            if (self.delegate && [self.delegate respondsToSelector:@selector(userInformationControllerDidChangeUser:)]) {
                [self.delegate userInformationControllerDidChangeUser:self];
            }
        });
    });
}

- (void)mediaPicker:(FSMediaPicker *)mediaPicker didFinishWithMediaInfo:(NSDictionary *)mediaInfo {
    self.avatarImageView.image = mediaInfo.editedImage;
    self.selectedNewImage = mediaInfo.editedImage;
}

- (void)userAvatarImageViewPressed:(UITapGestureRecognizer *)sender {
    FSMediaPicker *picker = [[FSMediaPicker alloc] init];
    picker.delegate = self;
    [picker showFromView:self.tableView];
}

- (NSArray *)ages {
    if (!_ages) {
        NSMutableArray *mutableAges = [NSMutableArray new];
        for (int i = 16; i < 81; i++) {
            [mutableAges addObject:[NSString stringWithFormat:@"%@", @(i)]];
        }
        _ages = [NSArray arrayWithArray:mutableAges];
    }
    return _ages;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 101) {
        return 2;
    }
    else if (pickerView.tag == 102) {
        return [self.ages count];
    }
    else if (pickerView.tag == 103) {
        NSInteger rows = 0;
        switch (component) {
            case 0:
                rows = [self.datasource count];
                break;
            case 1:
            {
                NSArray *cities = self.datasource[self.selectedProvinceIndex][@"cities"];
                rows = [cities count];
            }
                break;
            case 2:
            {
                NSArray *districts = self.datasource[self.selectedProvinceIndex][@"cities"][self.selectedCityIndex][@"districts"];
                rows = [districts count];
            }
                break;
            default:
                break;
        }
        return rows;
    }
    else {
        return 0;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView.tag == 103) {
        return 3;
    } else {
        return 1;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == 101) {
        return self.genders[row];
    }
    else if (pickerView.tag == 102) {
        return self.ages[row];
    }
    else if (pickerView.tag == 103) {
        NSString *title = @"";
        switch (component) {
            case 0:
                title = self.datasource[row][@"province"];
                break;
            case 1:
                title = self.datasource[self.selectedProvinceIndex][@"cities"][row][@"city"];
                break;
            case 2:
                title = self.datasource[self.selectedProvinceIndex][@"cities"][self.selectedCityIndex][@"districts"][row][@"district"];
                break;
            default:
                break;
        }
        return title;
    }
    else {
        return @"";
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == 101) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = self.genders[row];
    }
    else if (pickerView.tag == 102) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:1];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = self.ages[row];
    }
    else if (pickerView.tag == 103) {
        switch (component) {
            case 0:
                self.selectedProvinceIndex = [self.locationPickerView selectedRowInComponent:0];
                [pickerView reloadAllComponents];
                break;
            case 1:
                self.selectedCityIndex = [self.locationPickerView selectedRowInComponent:1];
                [pickerView reloadComponent:1];
                [pickerView reloadComponent:2];
                break;
            case 2:
                self.selectedDistrictIndex = [self.locationPickerView selectedRowInComponent:2];
                [pickerView reloadComponent:2];
                break;
                
            default:
                break;
        }
        
        NSString *province = self.datasource[self.selectedProvinceIndex][@"province"];
        NSString *city = self.datasource[self.selectedProvinceIndex][@"cities"][self.selectedCityIndex][@"city"];
        NSString *district = self.datasource[self.selectedProvinceIndex][@"cities"][self.selectedCityIndex][@"districts"][self.selectedDistrictIndex][@"district"];
        NSString *location = [NSString stringWithFormat:@"%@ %@ %@", province, city, district];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:1];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = location;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 1) {
        self.genderPikcerViewHidden = !self.genderPikcerViewHidden;
        self.genderPickerViewCell.hidden = !self.genderPickerViewCell.hidden;
    }
    else if (indexPath.row == 2 && indexPath.section == 1) {
        self.agePickerViewCell.hidden = !self.agePickerViewCell.hidden;
    }
    else if (indexPath.row == 4 && indexPath.section == 1) {
        self.locationPickerViewCell.hidden = !self.locationPickerViewCell.hidden;
    }
    else if (indexPath.row == 1 && indexPath.section == 0) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ChangeNameController *changeNameController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ChangeNameController"];
        changeNameController.delegate = self;
        [self.navigationController pushViewController:changeNameController animated:YES];
    }
    [tableView beginUpdates];
    [tableView endUpdates];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 0) {
        return 60;
    } else if (indexPath.row == 1 && indexPath.section == 1) {
        if (!self.genderPikcerViewHidden) {
            return 160;
        } else {
            return 0;
        }
    } else if (indexPath.row == 3 && indexPath.section == 1) {
        if (!self.agePickerViewCell.hidden) {
            return 160;
        } else {
            return 0;
        }
    } else if (indexPath.row == 5 && indexPath.section == 1) {
        if (!self.locationPickerViewCell.hidden) {
            return 160;
        } else {
            return 0;
        }
    } else {
        return 44.0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
