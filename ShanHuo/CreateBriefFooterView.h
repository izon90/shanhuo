//
//  CreateBriefFooterView.h
//  ShanHuo
//
//  Created by heyong on 8/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateBriefFooterView : UIView

@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end
