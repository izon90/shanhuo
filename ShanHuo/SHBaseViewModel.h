//
//  SHBaseViewModel.h
//  ShanHuo
//
//  Created by 小悟空 on 6/20/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHBaseViewModel : NSObject

- (void)boolToNumber:(BOOL)boolValue;
@end
