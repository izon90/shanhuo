//
//  SHBaseViewModel.m
//  ShanHuo
//
//  Created by 小悟空 on 6/20/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHBaseViewModel.h"

@implementation SHBaseViewModel

- (NSNumber *)boolToNumber:(BOOL)boolValue
{
    return [NSNumber numberWithBool:boolValue];
}
@end
