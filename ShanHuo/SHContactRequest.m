//
//  SHContactRequest.m
//  ShanHuo
//
//  Created by heyong on 15/8/11.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "SHContactRequest.h"

@implementation SHContactRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"senderUserID": @"request_id",
             @"targetUserID": @"answer_id",
             @"status": @"status",
             @"requestID": @"id"
             };
}

@end
