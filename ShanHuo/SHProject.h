//
//  SHProject.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "SHUser.h"
#import "SHFile.h"

@interface SHProject : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *content;
@property (nonatomic, copy, readonly) NSNumber *projectID;

@property (nonatomic, strong, readonly) SHUser *user;

@property (nonatomic, copy, readonly) NSArray *files;

@end
