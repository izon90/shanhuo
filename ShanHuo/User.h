//
//  User.h
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NotNull(obj) (obj && (![obj isEqual:[NSNull null]]) && (![obj isEqual:@"<null>"]) )

extern NSString * const kAvatarImageURL;
extern NSString * const kUserName;
extern NSString * const kUserLocation;
extern NSString * const kRating;

@interface User : NSObject

@property (readonly) NSURL *avatarImageURL;
@property (readonly) NSString *userName;
@property (readonly) NSString *userLocation;
@property (readonly) NSNumber *rating;

- (instancetype)initWithUserInfo:(NSDictionary *)info;

@end
