//
//  GKApplicationContext.m
//  GKUserKitExample
//
//  Created by 小悟空 on 3/7/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKUserApplicationContext.h"

#import "GKUserBackendMock.h"
#import "GKUserServiceImpl.h"
#import "GKUserAuthenticationController.h"
#import "GKRegistrationController.h"
#import "GKUserRepositoryImpl.h"

@implementation GKUserApplicationContext

- (void)configure {
    [self bindClass:[GKVerificationCodeBackendImpl class]
         toProtocol:@protocol(GKVerificationCodeBackend)];
    
    [self bindClass:[GKUserRepositoryImpl class]
         toProtocol:@protocol(GKUserRepository)];
    [self bindClass:[GKUserBackendImpl class]
         toProtocol:@protocol(GKUserBackend)];
    [self bindClass:[GKUserServiceImpl class]
         toProtocol:@protocol(GKUserService)];
}

@end

@implementation GKUserApplicationContextDemonstration

- (void)configure
{
    [self bindClass:[GKUserBackendMock class]
         toProtocol:@protocol(GKUserBackend)];
    [self bindClass:[GKUserServiceImpl class]
         toProtocol:@protocol(GKUserService)];
}
@end