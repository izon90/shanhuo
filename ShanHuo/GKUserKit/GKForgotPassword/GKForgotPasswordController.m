//
//  GKForgotPasswordController.m
//  GKUserKitExample
//
//  Created by MASGG on 15-2-22.
//  Copyright (c) 2015年 GKCommerce. All rights reserved.
//

#import "GKForgotPasswordController.h"
#import "GKVerificationTableViewCell.h"
#import "GKTableViewCell.h"
#import "GKVerificationTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

#define kVerificationTime 10;
@interface GKForgotPasswordController (){
    BOOL _isValided;
    int _verificationTime;
    NSTimer *_timer;
 
}
@property (strong, nonatomic) GKVerificationTableViewCell *verificationCell;
@end

@implementation GKForgotPasswordController

- (id)init
{
    self = [self initWithNibName:@"GKForgotPasswordController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    for (NSString *identifier in @[@"GKVerificationTableViewCell"]) {
        [self.tableView registerNib:[UINib nibWithNibName:identifier bundle:nil]
             forCellReuseIdentifier:identifier];
    }
    
    _verificationTime=kVerificationTime;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isValided) {
        return 4;
    }else{
        return 2;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    NSString *identifier = @"GKRegistrationTableViewCell";
    GKTableViewCell *cell = GKTableViewCell.new;

    switch (indexPath.row) {
        case 0: {
            static NSString *identifier = @"GKVerificationTableViewCell";
            GKVerificationTableViewCell *verification;
            verification =
                [self.tableView dequeueReusableCellWithIdentifier:identifier];
            verification.titleLabel.text = @"手机号";
            verification.inputTextField.placeholder = @"请输入手机号或Email地址";
            return verification;
            // cell = (GKTableViewCell *)verification;
            break;
        }
        case 1:{
            cell.titleLabel.text = @"验证码";
            cell.inputTextField.placeholder = @"请输入接收到验证码";
            break;
        }
        case 2: {
            cell.titleLabel.text=@"新密码";
            cell.inputTextField.placeholder=@"请输入新密码";
            break;
        }
        case 3:{
            cell.titleLabel.text=@"确认密码";
            cell.inputTextField.placeholder=@"请确认密码";
            break;
        }
        default:
            break;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0f;
}

@end
