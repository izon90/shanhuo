//
//  ViewController.m
//  GKUserKitExample
//
//  Created by 宇 陈 on 15/2/21.
//  Copyright (c) 2015年 GKCommerce. All rights reserved.
//

#import "GKRegistrationController.h"
#import "GKRegistrationTableViewCell.h"
#import "GKUserAccessToken.h"
#import "GKTableViewCell.h"
#import <Objection/Objection.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <Masonry/Masonry.h>

@interface GKRegistrationController ()
@end

@implementation GKRegistrationController
objection_requires_sel(@selector(service))

- (id)init
{
    self = [self initWithNibName:@"GKRegistrationController" bundle:nil];
    if (self) {
        self.service = [[JSObjection defaultInjector]
                        getObject:@protocol(GKUserService)];
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    for (NSString* identifier in @[@"GKRegistrationTableViewCell"]) {
        [self.tableView registerNib:[UINib nibWithNibName:identifier bundle:nil]
             forCellReuseIdentifier:identifier];
    }
}


- (void)setup
{
//    self.service = [[GKUserContainerMock alloc] userService];
    self.registration = [[GKUserRegistration alloc] init];
    self.title = @"用户注册";
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 3;
            break;
        case 1:
            return 1;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0f;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self configureBasicCell:tableView cellForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)configureBasicCell:(UITableView *)tableView
                  cellForRowAtIndexPath:(NSIndexPath *)indexpath {
    
    if (indexpath.section == 0) {
        GKTableViewCell *cell = GKTableViewCell.new;
        
        switch (indexpath.row) {
            case 0:
            {
                cell.titleLabel.text = @"邮箱";
                cell.inputTextField.placeholder = @"Email";
                RAC(self.registration, email) =
                    cell.inputTextField.rac_textSignal;
            }
                break;
            case 1:
            {
                cell.titleLabel.text = @"昵称";
                cell.inputTextField.placeholder = @"昵称";

                RAC(self.registration, username) =
                    cell.inputTextField.rac_textSignal;
            }
                break;
            case 2:
            {
                cell.titleLabel.text = @"密码";
                cell.inputTextField.placeholder = @"密码";
                cell.inputTextField.secureTextEntry = YES;

                RAC(self.registration, password) =
                    cell.inputTextField.rac_textSignal;
            }
                break;
            default:
                break;
        }
        
        return cell;
    } else {
        UITableViewCell *cell;
        cell = [[UITableViewCell alloc]
                        initWithStyle:UITableViewCellStyleDefault
                        reuseIdentifier:@"GKRegisterTableViewCell"];
        
        cell.textLabel.textColor = self.view.tintColor;
        cell.textLabel.text = @"注册";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.frame = cell.contentView.frame;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }

    return nil;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [self signup:nil];
    }
}



- (void(^)(GKUser *))didSignupUserSuccess
{
    return ^(GKUser *user) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.navigationController popViewControllerAnimated:YES];

        if (self.signupDidSucceed)
            self.signupDidSucceed(self, [[GKUser alloc] init]);
    };
}

- (void(^)(NSError *))didSignupUserFailure
{
    return ^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [[[UIAlertView alloc] initWithTitle:@"提示"
                                    message:error.localizedDescription
                                   delegate:nil
                          cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]
         show];

        if (self.signupDidFail)
            self.signupDidFail(error);
    };
}

- (IBAction)signup:(id)sender
{
    NSError *error = [self.registration valid];
    if (nil == error) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [[self.service signup:self.registration]
         subscribeNext:[self didSignupUserSuccess] error:[self didSignupUserFailure]];
        return;
    }
    
    [[[UIAlertView alloc] initWithTitle:@"提示"
                                message:error.localizedDescription delegate:nil
                      cancelButtonTitle:@"确定" otherButtonTitles:nil, nil]
     show];
    if (self.signupDidFail)
        self.signupDidFail(error);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
