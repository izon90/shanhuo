//
//  GKDefines.h
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#ifndef GKUserKitExample_GKDefines_h
#define GKUserKitExample_GKDefines_h

#import <CocoaLumberjack/CocoaLumberjack.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <AFNetworking/AFNetworking.h>

#import "App.h"
#import "GKUser.h"
#import "GKUserAccessToken.h"
#import "GKUserAuthentication.h"
#import "GKUserRegistration.h"
#import "GKTextField.h"
#import "GKUserService.h"

#define GKColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#endif
