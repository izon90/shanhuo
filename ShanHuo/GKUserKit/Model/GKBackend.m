//
//  GKBackend.m
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKBackend.h"

@implementation GKBackend

- (id)init {
    
    self = [super init];
    if (self) {
        self.manager = [AFHTTPRequestOperationManager manager];
        self.config = GKConfig.config;
    }
    return self;
}

- (NSString *)URLWithResource:(NSString *)resource {
    return [NSString stringWithFormat:@"%@%@", self.config.host, resource];
}

- (NSError *)errorWithResponse:(NSString *)response {
    NSData *responseData = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *errorHash =
    [NSJSONSerialization JSONObjectWithData:responseData options:0
                                      error:&error];
    if (error != nil)
        return error;
    
    NSArray *errors = errorHash[@"errors"];
    error = [NSError errorWithDomain:@"" code:1 userInfo:@{NSLocalizedDescriptionKey: ((NSArray *)errors.firstObject[@"messages"]).firstObject}];
    
    return error;
}

@end
