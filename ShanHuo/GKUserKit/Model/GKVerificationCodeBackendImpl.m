//
//  GKVerificationCodeBackendImpl.m
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKVerificationCodeBackendImpl.h"

@implementation GKVerificationCodeBackendImpl

- (void)requestVerificationCode:(NSString *)receiver
                    forResource:(NSString *)resource
                     completion:(void (^)(NSError *error))completion
{
    NSString *url = [self URLWithResource:@"verificationcode"];
    NSLog(@"%@", url);
    [self.manager
     POST:url
     parameters:@{ @"mobile": receiver, @"resource": resource }
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         if (completion)
             completion(nil);
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         DDLogError(@"requestVerificationCode:forResource:completion");
//         DDLogError(@"%@", error.localizedDescription);
//         DDLogError(@"%@", operation.responseString);
         if (completion)
             completion(error);
     }];
}
@end
