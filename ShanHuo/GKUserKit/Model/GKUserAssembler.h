//
//  GKUserAssembler.h
//  GKUserKitExample
//
//  Created by 小悟空 on 6/19/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKUser.h"

@interface GKUserAssembler : NSObject

- (GKUserAccessToken *)accessTokenWithAuthenticate:(NSDictionary *)authenticate;
- (GKUser *)user:(NSDictionary *)anUser;
@end
