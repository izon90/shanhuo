//
//  GKVerificationCodeBackendImpl.h
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKBackend.h"
#import "GKVerificationCodeBackend.h"

@interface GKVerificationCodeBackendImpl : GKBackend <GKVerificationCodeBackend>

@end
