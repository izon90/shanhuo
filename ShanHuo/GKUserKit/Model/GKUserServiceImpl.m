//
//  GKUserServiceImpl.m
//  GKUserKitExample
//
//  Created by SeanChense on 15/2/23.
//  Copyright (c) 2015年 GKCommerce. All rights reserved.
//

#import "GKUserServiceImpl.h"
#import "GKUser.h"
#import "GKUserRepository.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <Objection/Objection.h>
//static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@interface GKUserServiceImpl()
@end

@implementation GKUserServiceImpl
objection_requires_sel(@selector(backend), @selector(repository))

- (void)setUser:(GKUser *)user {
    [self.repository storage:user];
    [[App shared] setCurrentUser:user];
}

- (RACSignal *)signup:(GKUserRegistration *)registration
{
  return
  [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
    [[self.backend signup:registration]
     subscribeNext:^(GKUserAccessToken *accessToken) {
         GKUser *user = GKUser.new;
         user.accessToken = accessToken;
         
         DDLogVerbose(@"用户注册成功 %@", registration.username);
         DDLogVerbose(@"Access token %@", accessToken.accessToken);
         
         [[self.backend requestUser:accessToken] subscribeNext:^(GKUser *user) {
             [self setUser:user];
             [subscriber sendNext:user];
             [subscriber sendCompleted];
         } error:^(NSError *error) {
             [subscriber sendError:error];
         }];

    } error:^(NSError *error) {
        [subscriber sendError:error];
    }];
    
    return (RACDisposable *)nil;
  }];
}

- (BOOL)authorized:(GKUser *)user {
    GKUser *restored = [self.repository restore];
    return [restored authorized];
}

- (void)signup:(GKUserRegistration *)registration
    completion:(void(^)(GKUser *user, NSError *error))completion {
    
    [[self.backend signup:registration] subscribeNext:^(id x) {
        [self.repository create:x];
        if (completion)
            completion(x, nil);
    } error:^(NSError *error) {
        if (completion)
            completion(nil, error);
    }];
}

- (GKUser *)restore
{
    GKUser *user = [self.repository restore];
    if (user) {
        [[App shared] setCurrentUser:user];
        DDLogVerbose(@"从磁盘用户恢复 %@ %d %@", user.username, (int)user.userID,
                     user.accessToken.accessToken);
    }
    
    return user;
}

- (RACSignal *)authenticate:(GKUserAuthentication *)model
{
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[self.backend requestAuthenticate:model]
         subscribeNext:[self didAuthenticateUserSuccess:subscriber authenticationModel:model]
         error:[self didAuthenticateUserFailure:subscriber]];
        
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

- (RACSignal *)authenticateWithMobile:(GKUserAuthentication *)model
{
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[self.backend requestAuthenticateWithMobile:model]
         subscribeNext:[self didAuthenticateUserSuccess:subscriber
                                    authenticationModel:model]
         error:[self didAuthenticateMobileFailure:subscriber]];
        
        // Block写在方法内
        // 快速切换使用didAuthenticateUserFailure还是方法内的Block
        //
        //         error:^(NSError *error) {
        //             NSDictionary *userInfo;
        //             userInfo = @{ NSLocalizedDescriptionKey: @"错误的短信验证码" };
        //             error = [NSError errorWithDomain:@"User" code:2 userInfo:userInfo];
        //             [subscriber sendNext:error];
        //         }];
        
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

- (void(^)(NSError *))didAuthenticateMobileFailure:(id<RACSubscriber>)subscriber
{
    return ^(NSError *error) {
        NSDictionary *userInfo;
        userInfo = @{ NSLocalizedDescriptionKey: @"错误的短信验证码" };
        error = [NSError errorWithDomain:@"User" code:2 userInfo:userInfo];
        [subscriber sendError:error];
    };
}

- (void(^)(GKUserAccessToken *))
didAuthenticateUserSuccess:(id<RACSubscriber>)subscriber
authenticationModel:(GKUserAuthentication *)anAuthenticationModel
{
    return ^(GKUserAccessToken *accessToken) {
        
        [[self.backend requestUser:accessToken] subscribeNext:^(GKUser *user) {
            //            user.username = anAuthenticationModel.username;
            //            user.mobilePhoneNumber = user.username;
            user.accessToken = accessToken;
            
            [self setUser:user];
            DDLogVerbose(@"登录成功 %@", user.username);
            [subscriber sendNext:user];
            [subscriber sendCompleted];
        } error:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        
    };
}

- (void(^)(NSError *))didAuthenticateUserFailure:(id<RACSubscriber>)subscriber
{
    return ^(NSError *error) {
        DDLogVerbose(@"登录失败");
        [subscriber sendError:error];
    };
}

- (RACSignal *)logout
{
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        GKUser *user = [[App shared] currentUser];
        [self.repository storage:nil];
        [[App shared] setCurrentUser:nil];
        
        [subscriber sendNext:user];
        [subscriber sendCompleted];
        
        return nil;
    }];
}

- (RACSignal *)changeName:(NSString *)name withUser:(GKUser *)anUser
{
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[self.backend requestChangeName:name withUser:anUser]
         subscribeNext:^(id x) {
             GKUser *user = [[App shared] currentUser];
             user.name = name;
             [self.repository storage:user];
             
             [subscriber sendNext:user];
             [subscriber sendCompleted];
         }
         error:^(NSError *error) {
             DDLogVerbose(@"%@", error.localizedDescription);
             [subscriber sendError:error];
         }];
        return nil;
    }];
}

- (void)updateUserInfomation
{
    GKUser *user = [[App shared] currentUser];
    if ([user authorized]) {
        [[self.backend requestUser:user.accessToken]
         subscribeNext:^(GKUser *newUser) {
             [user merge:newUser];
             [self.repository storage:user];
         }
         error:^(NSError *error) {
             DDLogError([error localizedDescription]);
         }];
    }
}

- (void)storage:(GKUser *)user
{
    [self.repository storage:user];
}
@end
