//
//  GKBackend.h
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "GKConfig.h"

@interface GKBackend : NSObject

@property (strong, nonatomic) GKConfig *config;
@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

- (NSString *)URLWithResource:(NSString *)resource;
- (NSError *)errorWithResponse:(NSString *)response;
@end
