//
//  GKVerificationCodeBackend.h
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKDefines.h"

@protocol GKVerificationCodeBackend <NSObject>

- (void)requestVerificationCode:(NSString *)receiver
                    forResource:(NSString *)resource
                     completion:(void (^)(NSError *error))completion;
@end
