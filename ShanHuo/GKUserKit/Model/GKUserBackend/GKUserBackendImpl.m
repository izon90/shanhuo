//
//  GKUserBackendImpl.m
//  GKUserKitExample
//
//  Created by SeanChense on 15/2/23.
//  Copyright (c) 2015年 GKCommerce. All rights reserved.
//

#import "GKUserBackendImpl.h"
#import "GKUserAccessToken.h"

@implementation GKUserBackendImpl

- (id)init
{
    self = [super init];
    if (self) {
        self.assembler = [[GKUserAssembler alloc] init];
    }
    return self;
}

- (RACSignal *)signup:(GKUserRegistration *)registration {
    @weakify(self)
    NSDictionary *parameters =
    @{
        @"username": registration.mobile,
        @"password": registration.password,
        @"cellphone": registration.mobile,
        @"verificationcode": registration.verificationCode
    };
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self.manager
         POST:[self URLWithResource:@"user"]
         parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             GKUserAccessToken *accessToken;
             accessToken = [self.assembler
                            accessTokenWithAuthenticate:responseObject];
             
            [subscriber sendNext: accessToken];
            [subscriber sendCompleted];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSError *ApiError;
             ApiError = [self errorWithResponse:operation.responseString];
            [subscriber sendError:ApiError];
            [subscriber sendCompleted];
        }];
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

- (RACSignal *)requestAuthenticate:(GKUserAuthentication *)user
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"grant_type"]       = @"password";
    parameters[@"username"]         = user.username;
    parameters[@"password"]         = user.password;
    parameters[@"client_id"]        = @"swagger";
    parameters[@"client_secret"]    = @"swagger";
    
    // TODO: 格式化
    DDLogVerbose(@"request authenticate %@", self.config.OAuthAccessTokenURL);
    @weakify(self)
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self.manager
         POST:self.config.OAuthAccessTokenURL
         parameters:parameters
         success:^(AFHTTPRequestOperation *operation,
                   id responseObject) {
             DDLogVerbose(@"did request authenticate success.");
             [subscriber sendNext:
              [self.assembler accessTokenWithAuthenticate:responseObject]];
         } failure:^(AFHTTPRequestOperation *operation,
                     NSError *error) {
             DDLogError(@"%@", error.localizedDescription);
             [subscriber sendError:error];
             [subscriber sendCompleted];
         }];
        
        return [RACDisposable disposableWithBlock:^{
            
        }];
    }];
}

- (RACSignal *)requestAuthenticateWithMobile:(GKUserAuthentication *)user
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"grant_type"]       = @"mobile";
    parameters[@"username"]         = user.username;
    parameters[@"password"]         = user.password;
    parameters[@"client_id"]        = @"swagger";
    parameters[@"client_secret"]    = @"swagger";
    DDLogVerbose(@"发起认证请求");
    DDLogVerbose(@"%@", self.config.OAuthAccessTokenURL);
    // TODO: 格式化
    @weakify(self)
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self.manager
         POST:self.config.OAuthAccessTokenURL
         parameters:parameters
         success:^(AFHTTPRequestOperation *operation,
                   id responseObject) {
             DDLogVerbose(@"did request authenticate success.");
             GKUserAccessToken *accessToken;
             accessToken = [self.assembler accessTokenWithAuthenticate:responseObject];
             [subscriber sendNext:accessToken];
             [subscriber sendCompleted];
         } failure:^(AFHTTPRequestOperation *operation,
                     NSError *error) {
             DDLogError(@"%@", error.localizedDescription);
             [subscriber sendError:error];
         }];
        
        return [RACDisposable disposableWithBlock:^{
            
        }];
    }];
}

- (RACSignal *)requestUser:(GKUserAccessToken *)accessToken
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"access_token"] = accessToken.accessToken;
    @weakify(self)
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        
        NSString *requestURL = [self URLWithResource:@"api/user"];
        DDLogVerbose(@"%@", requestURL);
        [self.manager
         GET:requestURL
         parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             GKUser *user = [self.assembler user:responseObject];
             DDLogVerbose(@"请求用户信息 %d", (int)user.userID);
             [subscriber sendNext:user];
             [subscriber sendCompleted];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             DDLogError(@"%@", error.localizedDescription);
             DDLogError(@"%@", operation.responseString);
             [subscriber sendError:error];
         }];
        
        return [RACDisposable disposableWithBlock:^{ }];
    }];
}

- (RACSignal *)requestChangeName:(NSString *)name withUser:(GKUser *)anUser
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    //    parameters[@"access_token"] = anUser.accessToken.accessToken;
    parameters[@"name"] = name;
    
    [self.manager.requestSerializer
     setValue:[NSString stringWithFormat:@"Bearer %@",
               anUser.accessToken.accessToken]
     forHTTPHeaderField:@"Authorization"];
    
    @weakify(self)
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self.manager
         POST:[self URLWithResource:@"user/name"]
         parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             [subscriber sendNext:@1];
             [subscriber sendCompleted];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             DDLogVerbose(@"%@", operation.responseString);
             [subscriber sendError:error];
         }];
        
        return [RACDisposable disposableWithBlock:^{ }];
    }];
}

- (RACSignal *)requestUploadPhoto:(UIImage *)photo withUser:(GKUser *)anUser
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [self.manager.requestSerializer
     setValue:[NSString stringWithFormat:@"Bearer %@",
               anUser.accessToken.accessToken]
     forHTTPHeaderField:@"Authorization"];
    
    @weakify(self)
    return
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        NSData *photoRaw = UIImageJPEGRepresentation(photo, 1.0f);
        AFHTTPRequestOperation *operation =
        [self.manager
         POST:[self URLWithResource:@"user/avatar"]
         parameters:parameters
         constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
             [formData appendPartWithFileData:photoRaw name:@"avatar"
                                     fileName:@"avatar.jpg"
                                     mimeType:@"image/jpeg"];
         }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             [subscriber sendNext:@1];
             [subscriber sendCompleted];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [subscriber sendError:error];
         }];
        [operation start];
        
        return [RACDisposable disposableWithBlock:^{ }];
    }];
}
@end
