//
//  GKUserBackendImpl.h
//  GKUserKitExample
//
//  Created by SeanChense on 15/2/23.
//  Copyright (c) 2015年 GKCommerce. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKUserBackend.h"
#import "GKBackend.h"

#import "GKUserAssembler.h"
#import "GKDefines.h"

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@interface GKUserBackendImpl : GKBackend <GKUserBackend>

@property (strong, nonatomic) GKUserAssembler *assembler;
@end
