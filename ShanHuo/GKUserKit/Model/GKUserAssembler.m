//
//  GKUserAssembler.m
//  GKUserKitExample
//
//  Created by 小悟空 on 6/19/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKUserAssembler.h"
#import "GKUser.h"

@implementation GKUserAssembler

- (GKUser *)user:(NSDictionary *)anUser;
{
    GKUser *user = [[GKUser alloc] init];
    user.email = [self safe:[anUser objectForKey:@"email"]];
    user.username = [anUser objectForKey:@"username"];
    user.userID = [[anUser objectForKey:@"id"] integerValue];
    user.name = [self safe:[anUser objectForKey:@"name"]];
    user.mobilePhoneNumber = [anUser objectForKey:@"mobilePhoneNumber"];
    
    return user;
}

- (NSString *)safe:(id)x
{
    if ([x isKindOfClass:[NSNull class]])
        return @"";
    return x;
}

- (GKUserAccessToken *)accessTokenWithAuthenticate:(NSDictionary *)authenticate
{
    NSString *tokenType;
    GKUserAccessToken *accessToken;
    tokenType = [[authenticate objectForKey:@"token_type"] lowercaseString];
    accessToken = [[GKUserAccessToken alloc] init];
    if ([@"bearer" isEqualToString:tokenType])
        accessToken.type = GKBearer;
    else if ([@"bearer" isEqualToString:tokenType])
        accessToken.type = GKMAC;
    accessToken.accessToken = [authenticate objectForKey:@"access_token"];
    accessToken.expires = [[authenticate objectForKey:@"expires_in"]
                           integerValue];
    return accessToken;
}
@end
