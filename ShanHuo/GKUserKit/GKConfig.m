//
//  GKConfig.m
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKConfig.h"

@implementation GKConfig

+ (instancetype)config
{
    static GKConfig *_shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
    });
    return _shared;
}

- (NSURL *)URLwithPath:(NSString *)path {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.host, path]];
}

- (NSString *)URLStringWithPath:(NSString *)path {
    return [NSString stringWithFormat:@"%@%@", self.host, path];
}

@end
