//
//  GKTextField.m
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKTextField.h"

@implementation GKTextField

{
    CALayer *topLayer;
    CALayer *rightLayer;
    CALayer *bottomLayer;
    CALayer *leftLayer;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    topLayer = [CALayer layer];
    rightLayer = [CALayer layer];
    bottomLayer = [CALayer layer];
    leftLayer = [CALayer layer];
    
    [self.layer addSublayer:topLayer];
    [self.layer addSublayer:rightLayer];
    [self.layer addSublayer:bottomLayer];
    [self.layer addSublayer:leftLayer];
    
    self.leftView =
    [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.height,
                                             self.frame.size.height)];
    
    self.leftViewMode = UITextFieldViewModeAlways;
    self.invalid = false;
    self.valid = true;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    if (_borderColor != borderColor) {
        _borderColor = borderColor;
        self.layer.borderColor = borderColor.CGColor;
    }
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    if (_borderWidth != borderWidth) {
        _borderWidth = borderWidth;
        self.layer.borderWidth = borderWidth;
    }
}

- (void)setCornerRadius:(CGFloat)cornerRadius
{
    if (_cornerRadius != cornerRadius) {
        _cornerRadius = cornerRadius;
        self.layer.cornerRadius = cornerRadius;
    }
}

- (void)setTopBorderColor:(UIColor *)topBorderColor
{
    if (_topBorderColor != topBorderColor) {
        _topBorderColor = topBorderColor;
        
        topLayer.borderColor = topBorderColor.CGColor;
    }
}

- (void)setRightBorderColor:(UIColor *)rightBorderColor
{
    if (_rightBorderColor != rightBorderColor) {
        _rightBorderColor = rightBorderColor;
        
        rightLayer.borderColor = rightBorderColor.CGColor;
    }
}

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor
{
    if (_bottomBorderColor != bottomBorderColor) {
        _bottomBorderColor = bottomBorderColor;
        
        bottomLayer.borderColor = bottomBorderColor.CGColor;
    }
}

- (void)setLeftBorderColor:(UIColor *)leftBorderColor
{
    if (_leftBorderColor != leftBorderColor) {
        _leftBorderColor = leftBorderColor;
        
        leftLayer.borderColor = leftBorderColor.CGColor;
    }
}

- (void)setTopBorderWidth:(CGFloat)topBorderWidth
{
    if (_topBorderWidth != topBorderWidth) {
        _topBorderWidth = topBorderWidth;
        
        topLayer.borderWidth = topBorderWidth;
    }
}

- (void)setRightBorderWidth:(CGFloat)rightBorderWidth
{
    if (_rightBorderWidth != rightBorderWidth) {
        _rightBorderWidth = rightBorderWidth;
        
        rightLayer.borderWidth = rightBorderWidth;
    }
}

- (void)setBottomBorderWidth:(CGFloat)bottomBorderWidth
{
    if (_bottomBorderWidth != bottomBorderWidth) {
        _bottomBorderWidth = bottomBorderWidth;
        
        rightLayer.borderWidth = bottomBorderWidth;
    }
}

- (void)setLeftBorderWidth:(CGFloat)leftBorderWidth
{
    if (_leftBorderWidth != leftBorderWidth) {
        _leftBorderWidth = leftBorderWidth;
        
        leftLayer.borderWidth = leftBorderWidth;
    }
}

- (void)removeSubviewsFromView:(UIView *)view {
    for (UIView *child in [view subviews])
        [child removeFromSuperview];
}

- (void)setLeftImage:(UIImage *)leftImage {
    if (_leftImage != leftImage) {
        _leftImage = leftImage;
        
        // [self removeSubviewsFromView:self.leftView];
        self.leftImageView = [[UIImageView alloc] initWithImage:leftImage];
        [self.leftView addSubview:self.leftImageView];
        
        self.leftView.backgroundColor = [UIColor clearColor];
    }
}

- (void)setLeftInvalidImage:(UIImage *)leftInvalidImage {
    if (_leftInvalidImage != leftInvalidImage) {
        _leftInvalidImage = leftInvalidImage;
        
        self.leftInvalidImageView =
            [[UIImageView alloc] initWithImage:leftInvalidImage];
        self.leftViewMode = UITextFieldViewModeAlways;
        self.leftView.backgroundColor = [UIColor clearColor];
    }
}

- (void)setInvalid:(BOOL)invalid {
    if (_invalid != invalid) {
        _invalid = invalid;
        
        [self removeSubviewsFromView:self.leftView];
        if (invalid) {
            [self.leftView addSubview:self.leftInvalidImageView];
        } else {
            [self.leftView addSubview:self.leftImageView];
        }
    }
}

- (void)setValid:(BOOL)valid {
    if (_valid != valid) {
        _valid = valid;
        
        self.invalid = !valid;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
