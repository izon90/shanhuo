//
//  GKVerificationButton.m
//  GKUserKitExample
//
//  Created by 小悟空 on 5/31/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import "GKVerificationButton.h"
#import <Masonry/Masonry.h>

@implementation GKVerificationButton

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.layer.cornerRadius = 5.0f;
    self.layer.borderColor = self.tintColor.CGColor;
    self.layer.borderWidth = 1.0f;
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.textColor = self.tintColor;
    self.textLabel.font = [UIFont systemFontOfSize:14.0];
    [self addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
//    [self mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@100);
//        make.height.equalTo(@30);
//    }];
    
    self.textLabel.text = @"请求验证码";
}

- (void)updateConstraints
{
    [super updateConstraints];
    

}
@end
