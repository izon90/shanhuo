//
//  GKConfig.h
//  GKUserKitExample
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GKCommerce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GKConfig : NSObject

@property (strong, nonatomic) NSString *host;
@property (strong, nonatomic) NSString *OAuthAccessTokenURL;

+ (instancetype)config;

- (NSURL *)URLwithPath:(NSString *)path;
- (NSString *)URLStringWithPath:(NSString *)path;

@end
