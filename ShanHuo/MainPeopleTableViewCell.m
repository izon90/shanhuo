//
//  MainPeopleTableViewCell.m
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "MainPeopleTableViewCell.h"
#import "SHTag.h"
#import "SHTagView.h"

@implementation MainPeopleTableViewCell

- (void)setUser:(SHUser *)user {
    if (_user != user) {
        _user = user;
    }
    self.usernameLabel.text = _user.name ? _user.name : _user.username;
}

- (void)setTags:(NSArray *)tags {
    if (_tags != tags) {
        _tags = tags;
    }
    self.tagView.orientation = CSLinearLayoutViewOrientationHorizontal;
    
    CGFloat totalWidth = 0;
    
    for (int i = 0; i < [_tags count]; i++) {
        SHTag *tag = _tags[i];
        
        NSString *tagName = tag.fullname;
        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tagName];
        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} range:NSMakeRange(0, tagName.length)];
        CGRect requirementRect = [attribtedTag boundingRectWithSize:CGSizeMake(self.tagView.frame.size.width, 16) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        if (totalWidth + requirementRect.size.width <= 200) {
            [self.tagView addItem:[self layoutItemWithTag:tag]];
            totalWidth += requirementRect.size.width + 8;
        } else {
            break;
        }
    }
}
- (void)awakeFromNib {
    // Initialization code
    [self layoutIfNeeded];
    
    
}

- (CSLinearLayoutItem *)layoutItemWithTag:(SHTag *)tag {
    SHTagView *tagView = [[SHTagView alloc] initWithTag:tag];
    tagView.borderColor = SHColor.tintColor;
    tagView.borderWidth = 1;
    CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:tagView];
    item.padding = CSLinearLayoutMakePadding(0.0, 0.0, 0.0, 5.0);
    item.fillMode = CSLinearLayoutItemFillModeNormal;
    item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    return item;
}

//- (CSLinearLayoutItem *)requirementLayoutItemWithTag:(SHTag *)tag {
//    SHTagView *tagView = [[SHTagView alloc] initWithTag:tag];
//    tagView.backgroundColor = SHColor.tintColor;
//    tagView.cornerRadius = 3.0f;
//    tagView.tagLabel.textColor = UIColor.whiteColor;
//    CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:tagView];
//    item.padding = CSLinearLayoutMakePadding(0.0, 0.0, 0.0, 5.0);
//    item.fillMode = CSLinearLayoutItemFillModeNormal;
//    item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
//    
//    return item;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
