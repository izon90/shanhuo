//
//  SHHelper.h
//  ShanHuo
//
//  Created by heyong on 15/7/30.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHHelper : NSObject

+ (instancetype)sharedHelper;

- (void)archiverObject:(id)object toUserDefaultsWithName:(NSString *)name;
- (id)unarchiveObjectWithUserDefaultsName:(NSString *)name;

- (void)showHudWithMessage:(NSString *)message fromView:(UIView *)view;

@end
