//
//  SHUser.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "MTLModel.h"
#import "GKUser.h"

@interface SHUser : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *userID;
@property (nonatomic, copy, readonly) NSString *username;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSNumber *rank;
@property (nonatomic, copy, readonly) NSArray *tags;
@property (nonatomic, copy, readonly) NSNumber *gender;
@property (nonatomic, copy, readonly) NSNumber *age;
@property (nonatomic, copy, readonly) NSNumber *allowContacts;
@property (nonatomic, copy, readonly) NSNumber *cityID;
@property (nonatomic, copy, readonly) NSNumber *districtID;
@property (nonatomic, copy, readonly) NSNumber *provinceID;
@property (nonatomic, copy, readonly) NSDate *vipExpireDate;


- (instancetype)initWithGKUser:(GKUser *)user;

@end
