//
//  TagCell.m
//  FreelanceProject
//
//  Created by heyong on 7/20/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "TagCell.h"

@implementation TagCell

- (void)setTags:(NSArray *)tags {
    if (_tags != tags) {
        _tags = tags;
    }
}

- (void)setTagsViewHeight:(NSNumber *)tagsViewHeight {
    if (_tagsViewHeight != tagsViewHeight) {
        _tagsViewHeight = tagsViewHeight;
    }
}

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
