//
//  SHTextField.m
//  ShanHuo
//
//  Created by heyong on 8/1/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHTextField.h"

@implementation SHTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(10, 0, bounds.size.width-20, bounds.size.height);
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return CGRectMake(10, 0, bounds.size.width-20, bounds.size.height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectMake(10, 0, bounds.size.width-20, bounds.size.height);
}

@end
