//
//  CreateProjectHeaderView.m
//  ShanHuo
//
//  Created by heyong on 15/7/31.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "CreateProjectHeaderView.h"

@implementation CreateProjectHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentTextView.textContainerInset = UIEdgeInsetsMake(10, 8, 10, 8);
}

@end
