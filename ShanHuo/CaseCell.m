//
//  CaseCell.m
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "CaseCell.h"

@implementation CaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.contentLabel];
        [self.contentView addSubview:self.imageViews];
    }
    return self;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 12, self.contentView.frame.size.width - 24, 20)];
        _contentLabel.textColor = [UIColor blackColor];
        _contentLabel.font = [UIFont systemFontOfSize:15];
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}

- (UIScrollView *)imageViews {
    if (!_imageViews) {
        _imageViews = [[UIScrollView alloc] initWithFrame:CGRectMake(12, self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 12, self.contentView.frame.size.width - 24, self.contentView.frame.size.width/3)];
        _imageViews.contentSize = CGSizeMake(self.contentView.frame.size.width - 24, self.contentView.frame.size.width/3);
    }
    return _imageViews;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect rect = [self.contentLabel.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.contentView.bounds)-24, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15]} context:nil];
    
    CGRect contentLabelFrame = self.contentLabel.frame;
    contentLabelFrame.size.height = rect.size.height;
    self.contentLabel.frame = contentLabelFrame;
    
    CGRect imageViewsFrame = self.imageViews.frame;
    if ([self.contentLabel.text length] > 0) {
        imageViewsFrame.origin.y = self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 12;
        self.imageViews.frame = imageViewsFrame;
    } else {
        imageViewsFrame.origin.y = 12;
        self.imageViews.frame = imageViewsFrame;
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
