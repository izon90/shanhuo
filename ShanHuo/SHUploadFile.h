//
//  SHUploadFile.h
//  ShanHuo
//
//  Created by 小悟空 on 7/16/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHBaseFile.h"

@interface SHUploadFile : SHBaseFile

@property (assign, nonatomic) NSInteger resourceID;
@property (strong, nonatomic) NSString *resourceType;
@property (strong, nonatomic) NSData *data;
@property (strong, nonatomic) NSData *previewData;
@end
