//
//  SHRequest.h
//  ShanHuo
//
//  Created by heyong on 15/8/6.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SHBriefRequest : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *requestID;
@property (nonatomic, copy, readonly) NSNumber *owernID;
@property (nonatomic, copy, readonly) NSNumber *briefID;
@property (nonatomic, copy, readonly) NSNumber *userID;
@property (nonatomic, copy, readonly) NSNumber *status;

@end
