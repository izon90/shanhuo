//
//  CreateBriefViewController.m
//  ShanHuo
//
//  Created by heyong on 8/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "CreateBriefViewController.h"
#import <SZTextView/SZTextView.h>

@interface CreateBriefViewController ()

@property (weak, nonatomic) IBOutlet SZTextView *contentTextView;
@property (weak, nonatomic) IBOutlet UILabel *deadlineLabel;
@property (weak, nonatomic) IBOutlet UITextField *budgetTextField;
@property (weak, nonatomic) IBOutlet UIView *tagView;
@property (weak, nonatomic) IBOutlet UIView *fileView;

@end

@implementation CreateBriefViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
