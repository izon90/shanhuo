//
//  IntroViewController.m
//  ShanHuo
//
//  Created by heyong on 15/8/6.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "IntroViewController.h"
#import <EAIntroView/EAIntroView.h>

@interface IntroViewController () <EAIntroDelegate>
//<UIScrollViewDelegate>

//@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableArray *intros = [NSMutableArray arrayWithCapacity:5];
    
    for (int i = 1; i < 6; i++) {
        EAIntroPage *page = [[EAIntroPage alloc] init];
        page.bgImage = [UIImage imageNamed:[NSString stringWithFormat:@"intro%@.jpg", @(i)]];
        [intros addObject:page];
    }
    
    EAIntroView *introView = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:intros];
    introView.skipButton = nil;
    introView.delegate = self;
    [introView showInView:self.view];
    
//    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
//    [self.view addSubview:self.scrollView];
//    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width * 5, self.view.frame.size.height);
//    self.scrollView.delegate = self;
//    
//    for (int i = 0; i < 5; i++) {
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        imageView.contentMode = UIViewContentModeScaleAspectFill;
//        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"intro%@.jpg", @(i-1)]];
//        [self.scrollView addSubview:imageView];
//    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    
//}

- (void)introDidFinish:(EAIntroView *)introView {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IntroOut" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
