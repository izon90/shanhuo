//
//  MyInfoViewController.m
//  ShanHuo
//
//  Created by heyong on 7/26/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "MyInfoViewController.h"
#import "SHNetworkEngine.h"
#import "SHUser.h"
#import "SHTag.h"
#import "UIImageView+WebCache.h"
#import "UserInfomationController.h"
#import "SHHelper.h"
#import "ProjectViewController.h"
#import "BriefViewController.h"
#import "TagTableViewController.h"
#import "GKUserServiceImpl.h"

@interface MyInfoViewController () <UserInfomationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) SHUser *user;

@property (strong, nonatomic) UIView *tagsView;
@property (assign, nonatomic) CGFloat tagsViewHeight;
@property (weak, nonatomic) IBOutlet TagCell *tagCell;

@end

@implementation MyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
}

- (void)loadData {
    GKUser *currentUser = [App shared].currentUser;
    NSNumber *userID = @(currentUser.userID);
    
    __weak typeof(self) wself = self;
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    
    [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:userID block:^(SHUser *user, NSError *error) {
        wself.user = user;
        [[SHHelper sharedHelper] archiverObject:wself.user toUserDefaultsWithName:@"SHUser"];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *tags = wself.user.tags;
            if (tags && [tags count] > 0) {
                [wself.tagCell setTags:tags];
                [self addTagsView:wself.tagCell.contentView];
            }
            wself.nameLabel.text = wself.user.name ? wself.user.name : wself.user.username;
            [self.tableView reloadData];
            [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        });
    }];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, userID]];
    [self.avatarImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
}

// 添加tag label
- (void)addTagsView:(UIView *)tagsView {
    // 整个tag view的padding
    UIEdgeInsets tagsViewPadding = UIEdgeInsetsMake(40, 10, 10, 10);
    
    // 记录上一个添加到cell contentView的label，用于计算下一个label的frame
    CGRect previousTagLabelFrame = CGRectZero;
    
    // 是否是第一个添加的tag label
    BOOL previousTagLabelExist = NO;
    
    // 获取测试tags
    //    NSArray *tags = self.dataSource[1];
    NSArray *tags = self.user.tags;
    
    for (int i = 0; i < [tags count]; i++) {
        SHTag *tagObject = tags[i];
        NSString *tag = tagObject.fullname;
        
        // 计算除tag文字的宽度，高度固定为20
        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag];
        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, tag.length)];
        CGRect rect = [attribtedTag boundingRectWithSize:CGSizeMake(tagsView.frame.size.width - 10, 20) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        CGFloat labelWidth = rect.size.width+10;
        
        // 初始化tag label
        UILabel *label = [[UILabel alloc] init];
        
        // 如果前一个的label已经加入contentView
        if (previousTagLabelExist) {
            // 获取已经添加的label的总宽度
            CGFloat existedWidth = previousTagLabelFrame.origin.x + previousTagLabelFrame.size.width + labelWidth + 5;
            // 如果总宽度大于cell contentView的宽度，那么就把下一个label放在下一行
            if (existedWidth > tagsView.frame.size.width) {
                // 算出下一行label的frame
                label.frame = CGRectMake(tagsViewPadding.left, previousTagLabelFrame.origin.y + previousTagLabelFrame.size.height + 5, labelWidth, 20);
            } else {
                // 如果没有超出contentView的剩余宽度空间，那么就继续在当前行添加label
                label.frame = CGRectMake(tagsViewPadding.left + previousTagLabelFrame.origin.x + previousTagLabelFrame.size.width + 5, previousTagLabelFrame.origin.y, labelWidth, 20);
            }
        } else {
            // 第一个label的宽度
            label.frame = CGRectMake(tagsViewPadding.left, tagsViewPadding.top, labelWidth, 20);
        }
        label.font = [UIFont systemFontOfSize:14];
        label.text = tag;
        // 居中显示
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        label.textColor = [UIColor whiteColor];
        [tagsView addSubview:label];
        
        // 记录下最近的label的frame，用于计算下一个label的frame
        previousTagLabelFrame = label.frame;
        previousTagLabelExist = YES;
    }
    // 计算出tags添加完以后，总高度，用于heightForRowAtIndexPath
    self.tagsViewHeight = previousTagLabelFrame.size.height + previousTagLabelFrame.origin.y;
    // table view cell高度改变了，reload当前section
    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && indexPath.section == 0) {
        UserInfomationController *userInfoController = (UserInfomationController *)[self.storyboard instantiateViewControllerWithIdentifier:@"UserInfomationController"];
        userInfoController.user = self.user;
        userInfoController.delegate = self;
        [self.navigationController pushViewController:userInfoController animated:YES];
    } if (indexPath.section == 2 && indexPath.row == 1) {
        ProjectViewController *projectViewController = (ProjectViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProjectViewController"];
        projectViewController.user = self.user;
        [self.navigationController pushViewController:projectViewController animated:YES];
    } if (indexPath.section == 2 && indexPath.row == 0) {
        BriefViewController *briefViewControler = (BriefViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"BriefViewController"];
        briefViewControler.user = self.user;
        [self.navigationController pushViewController:briefViewControler animated:YES];
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        __weak typeof(self) wself = self;
        TagTableViewController *controller;
        controller = [[TagTableViewController alloc] initWithParent:nil selection:^(Tag *tag, NSError *error) {
            [[SHNetworkEngine sharedEngine] attachTag:tag toUser:[[App shared] currentUser] completion:^(Tag *tag, NSError *error) {
                [wself loadData];
            }];
        }];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.section == 4 && indexPath.row == 0) {
        GKUserServiceImpl *service = [[GKUserServiceImpl alloc] init];
        [[service logout] subscribeNext:^(id x) {
            
            UINavigationController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"UserLoginNavigationController"];
            [self.navigationController presentViewController:controller animated:YES completion:^{
                
            }];
            [self loadData];
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 50;
    if (indexPath.section == 0) {
        height = 100;
    } else if (indexPath.section == 1) {
        CGFloat tagsViewHeight = self.tagsViewHeight;
        return MAX(tagsViewHeight+10, 64);
    }
    return height;
}

- (void)userInformationControllerDidChangeUser:(UserInfomationController *)controller {
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
