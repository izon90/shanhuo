//
//  SHProject.m
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHProject.h"

@implementation SHProject

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title": @"title",
             @"content": @"content",
             @"projectID": @"id",
             @"user": @"user",
             @"files": @"files"
             };
}

+ (NSValueTransformer *)userJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[SHUser class]];
}

+ (NSValueTransformer *)filesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[SHFile class]];
}

@end
