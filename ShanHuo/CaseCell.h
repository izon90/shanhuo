//
//  CaseCell.h
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CaseCell : UITableViewCell

@property (strong, nonatomic) UILabel *contentLabel;
@property (strong, nonatomic) UIScrollView *imageViews;

@end
