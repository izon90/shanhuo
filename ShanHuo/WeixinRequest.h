//
//  WeixinRequest.h
//  ShanHuo
//
//  Created by heyong on 15/8/7.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeixinRequest : NSObject

+ (instancetype)shareRequest;

- (void)checkoutWithPrepayId:(NSString *)prepayId;
- (BOOL)isWeixinInstalled;
- (void)sendPay_demo;

@end
