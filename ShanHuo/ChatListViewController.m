//
//  ChatListViewController.m
//  ShanHuo
//
//  Created by heyong on 7/24/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "ChatListViewController.h"
#import "SHConversationViewController.h"
#import "SHChatListCell.h"
#import "SHUserInfo.h"
#import "UIImageView+WebCache.h"
#import "SHNetworkEngine.h"
#import "GKConfig.h"
#import "ProfileViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "SHChatRequestCell.h"
#import "SHConversationCell.h"
#import "SHSystemCell.h"
#import "GKConfig.h"

@interface ChatListViewController ()

@end

@implementation ChatListViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE), @(ConversationType_SYSTEM)]];
        [self setCollectionConversationType:@[@(ConversationType_GROUP)]];
    }
    return self;
}

- (void)pushConversationViewControllerWithModel:(RCConversationModel *)model targetUsername:(NSString *)username {
    SHConversationViewController *conversationVC = [[SHConversationViewController alloc]init];
    conversationVC.conversationType = ConversationType_PRIVATE;
    conversationVC.targetId = model.targetId;
    conversationVC.userName = username;
    conversationVC.title = username;
    [self.navigationController pushViewController:conversationVC animated:YES];
}

-(void)onSelectedTableRow:(RCConversationModelType)conversationModelType conversationModel:(RCConversationModel *)model atIndexPath:(NSIndexPath *)indexPath {
    [self.conversationListTableView deselectRowAtIndexPath:indexPath animated:YES];
    if (model.conversationType == ConversationType_PRIVATE) {
        [self pushConversationViewControllerWithModel:model targetUsername:@""];
    } else if (model.conversationType == ConversationType_SYSTEM) {
        if ([model.lastestMessage isMemberOfClass:[RCContactNotificationMessage class]]) {
            RCContactNotificationMessage *message = (RCContactNotificationMessage *)model.lastestMessage;
            NSData *jsonData = [message.extra dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error = nil;
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            if (!error) {
                NSString *statusStr = dict[@"status"];
                NSNumber *status = @([statusStr intValue]);
                if ([status intValue] == 1) {
                    NSString *userName = [dict valueForKeyPath:@"answer.username"];
                    [self pushConversationViewControllerWithModel:model targetUsername:userName];
                }
            }
        }
    }
}

- (NSMutableArray *)willReloadTableData:(NSMutableArray *)dataSource {
    for (int i = 0; i < dataSource.count; i++) {
        RCConversationModel *model = dataSource[i];
        if (model.conversationType == ConversationType_SYSTEM) {
            if ([model.lastestMessage isMemberOfClass:[RCContactNotificationMessage class]]) {
                RCContactNotificationMessage *message = (RCContactNotificationMessage *)model.lastestMessage;
                NSString *targetUserId = message.targetUserId;
                [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([targetUserId intValue]) block:^(SHUser *user, NSError *error) {
                    if (user) {
                        NSString *targetUsername = user.name ? user.name : user.username;
                        SHUserInfo *userInfo = [[SHUserInfo alloc] init];
                        userInfo.userId = targetUserId;
                        userInfo.name = targetUsername;
                        userInfo.portraitUri = [NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID];
                        
                        model.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
                        model.extend = userInfo;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self refreshConversationTableViewWithConversationModel:model];
                            [self resetConversationListBackgroundViewIfNeeded];
                            [self notifyUpdateUnreadMessageCount];
                        });
                    }
                }];
            }
        }
        else if (model.conversationType == ConversationType_PRIVATE) {
            if ([model.lastestMessage isMemberOfClass:[RCTextMessage class]]) {
                NSString *targetId = model.targetId;
                [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([targetId intValue]) block:^(SHUser *user, NSError *error) {
                    if (user) {
                        NSString *targetUsername = user.name ? user.name : user.username;
                        SHUserInfo *userInfo = [[SHUserInfo alloc] init];
                        userInfo.userId = targetId;
                        userInfo.name = targetUsername;
                        userInfo.portraitUri = [NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID];

                        model.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
                        model.extend = userInfo;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self refreshConversationTableViewWithConversationModel:model];
                            [self resetConversationListBackgroundViewIfNeeded];
                            [self notifyUpdateUnreadMessageCount];
                        });
                    }
                }];
            }
        }
    }
    return dataSource;
}

//自定义cell
- (RCConversationBaseCell *)rcConversationListTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RCConversationModel *model = self.conversationListDataSource[indexPath.row];
    
    if (model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION) {
        if (model.conversationType == ConversationType_PRIVATE) {
            SHConversationCell *cell = (SHConversationCell *)[[[NSBundle mainBundle] loadNibNamed:@"SHConversationCell" owner:self options:nil] firstObject];
            
            if ([model.lastestMessage isKindOfClass:[RCTextMessage class]]) {
                RCTextMessage *message = (RCTextMessage *)model.lastestMessage;
                cell.messageLabel.text = message.content;
            }
            
            SHUserInfo *userInfo = model.extend;
            cell.usernameLabel.text = userInfo.name;
            [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
            
            cell.favoriteBlock = ^(SHConversationCell *cell, BOOL favorite) {
                if (favorite) {
                    //                cell.favoriteImageView.image = [UIImage imageNamed:@""];
                }
            };
            return cell;
        } else if (model.conversationType == ConversationType_SYSTEM && [model.lastestMessage isMemberOfClass:[RCContactNotificationMessage class]]) {
            RCContactNotificationMessage *message = (RCContactNotificationMessage *)model.lastestMessage;
            //BriefSupplier
            //BriefSupplierAgree
            //BeiefSupplierReject
            if ([message.operation isEqualToString:@"BriefSupplier"] || [message.operation isEqualToString:@"UserContactRequest"]) {
                SHChatRequestCell *cell = (SHChatRequestCell *)[[[NSBundle mainBundle] loadNibNamed:@"SHChatRequestCell" owner:self options:nil] firstObject];
                cell.messageLabel.text = message.message;
                
                SHUserInfo *userInfo = model.extend;
                cell.usernameLabel.text = userInfo.name;
                [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
                
                if (message.extra) {
                    NSLog(@"%@", message.extra);
                    NSData *jsonData = [message.extra dataUsingEncoding:NSUTF8StringEncoding];
                    NSError *error = nil;
                    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                    if (!error) {
                        NSString *statusStr = [dict valueForKeyPath:@"brief.status"];
                        NSNumber *status = @([statusStr intValue]);
                        
                        if ([status intValue] == 0) {
                            
                        } else {
                            cell.buttonView.hidden = YES;
                        }
                        
                        NSString *briefIDStr = [dict valueForKeyPath:@"brief.id"];
                        NSNumber *briefID = @([briefIDStr intValue]);
                        
                        cell.agreeBlock = ^(SHChatRequestCell *cell, UIView *agreeView) {
                            if ([message.operation isEqualToString:@"BriefSupplier"]) {
                                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                [[SHNetworkEngine sharedEngine] approveOrDenyRequestToJoinBrief:briefID status:@(1) completion:^(SHBriefRequest *request, NSError *error) {
                                    if (request) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            cell.buttonView.hidden = YES;
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            [[SHHelper sharedHelper] showHudWithMessage:@"已经同意" fromView:self.navigationController.view];
                                        });
                                    } else {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            cell.buttonView.hidden = YES;
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            [[SHHelper sharedHelper] showHudWithMessage:@"网络错误" fromView:self.navigationController.view];
                                        });
                                    }
                                }];
                            }
                        };
                        
                        cell.refuseBlock = ^(SHChatRequestCell *cell, UIView *refuseView) {
                            if ([message.operation isEqualToString:@"BriefSupplier"]) {
                                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                [[SHNetworkEngine sharedEngine] approveOrDenyRequestToJoinBrief:briefID status:@(2) completion:^(SHBriefRequest *request, NSError *error) {
                                    if (request) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            cell.buttonView.hidden = YES;
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            [[SHHelper sharedHelper] showHudWithMessage:@"已经拒绝" fromView:self.navigationController.view];
                                        });
                                    } else {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            cell.buttonView.hidden = YES;
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            [[SHHelper sharedHelper] showHudWithMessage:@"网络错误" fromView:self.navigationController.view];
                                        });
                                    }
                                }];
                            }
                        };
                    }
                }
                if ([message.operation isEqualToString:@"UserContactRequest"]) {
                    cell.agreeBlock = ^(SHChatRequestCell *cell, UIView *agreeView) {
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        [[SHNetworkEngine sharedEngine] userContactRequestWithSenderUserId:@([userInfo.userId intValue]) targetUserId:@([App shared].currentUser.userID) status:@(1) completion:^(SHContactRequest *request, NSError *error) {
                            if (request) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    cell.buttonView.hidden = YES;
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    [[SHHelper sharedHelper] showHudWithMessage:@"已经同意" fromView:self.navigationController.view];
                                });
                            } else {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    cell.buttonView.hidden = YES;
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    [[SHHelper sharedHelper] showHudWithMessage:@"网络错误" fromView:self.navigationController.view];
                                });
                            }
                        }];
                    };
                    
                    cell.refuseBlock = ^(SHChatRequestCell *cell, UIView *refuseView) {
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        [[SHNetworkEngine sharedEngine] userContactRequestWithSenderUserId:@([userInfo.userId intValue]) targetUserId:@([App shared].currentUser.userID) status:@(2) completion:^(SHContactRequest *request, NSError *error) {
                            if (request) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    cell.buttonView.hidden = YES;
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    [[SHHelper sharedHelper] showHudWithMessage:@"已经拒绝" fromView:self.navigationController.view];
                                });
                            } else {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    cell.buttonView.hidden = YES;
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    [[SHHelper sharedHelper] showHudWithMessage:@"网络错误" fromView:self.navigationController.view];
                                });
                            }
                        }];
                    };
                }
                return cell;
            } else {
                SHSystemCell *cell = (SHSystemCell *)[[[NSBundle mainBundle] loadNibNamed:@"SHSystemCell" owner:self options:nil] firstObject];
                cell.systemMessageLabel.text = message.message;
                return cell;
            }
        } else {
            return [[RCConversationBaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
        }
    } else {
        return [[RCConversationBaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    }
}

- (void)rcConversationListTableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    RCConversationModel *model = self.conversationListDataSource[indexPath.row];
    [[RCIMClient sharedRCIMClient] removeConversation:ConversationType_SYSTEM targetId:model.targetId];
    [self.conversationListDataSource removeObjectAtIndex:indexPath.row];
    [self.conversationListTableView reloadData];
}

- (void)didReceiveMessageNotification:(NSNotification *)notification
{
    __weak typeof(&*self) blockSelf_ = self;
    RCMessage *message = notification.object;
    
    if ([message.content isMemberOfClass:[RCContactNotificationMessage class]] && message.conversationType == ConversationType_SYSTEM) {
        RCContactNotificationMessage *_contactNotificationMsg = (RCContactNotificationMessage *)message.content;
        NSString *targetUserId = _contactNotificationMsg.targetUserId;
        [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([targetUserId intValue]) block:^(SHUser *user, NSError *error) {
            if (user) {
                NSString *targetUsername = user.name ? user.name : user.username;
                SHUserInfo *userInfo = [[SHUserInfo alloc] init];
                userInfo.userId = targetUserId;
                userInfo.name = targetUsername;
                userInfo.portraitUri = [NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID];
                
                RCConversationModel *model = [RCConversationModel new];
                model.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
                model.extend = userInfo;
                model.senderUserId = message.senderUserId;
                model.lastestMessage = _contactNotificationMsg;
                model.targetId = message.targetId;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refreshConversationTableViewWithConversationModel:model];
                    [self resetConversationListBackgroundViewIfNeeded];
                    [self notifyUpdateUnreadMessageCount];
                    
                    NSNumber *left = [notification.userInfo objectForKey:@"left"];
                    if (0 == left.integerValue) {
                        [super refreshConversationTableViewIfNeeded];
                    }
                });
            }
        }];
    }
    else if (message.conversationType == ConversationType_PRIVATE) {
        if ([message.content isMemberOfClass:[RCMessageContent class]]) {
            NSString *targetId = message.targetId;
            [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([targetId intValue]) block:^(SHUser *user, NSError *error) {
                if (user) {
                    NSString *targetUsername = user.name ? user.name : user.username;
                    SHUserInfo *userInfo = [[SHUserInfo alloc] init];
                    userInfo.userId = targetId;
                    userInfo.name = targetUsername;
                    userInfo.portraitUri = [NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID];
                    
                    RCConversationModel *model = [RCConversationModel new];
                    model.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
                    model.extend = userInfo;
                    model.senderUserId = message.senderUserId;
                    model.targetId = message.targetId;
                    model.lastestMessage = message.content;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self refreshConversationTableViewWithConversationModel:model];
                        [self resetConversationListBackgroundViewIfNeeded];
                        [self notifyUpdateUnreadMessageCount];
                        
                        NSNumber *left = [notification.userInfo objectForKey:@"left"];
                        if (0 == left.integerValue) {
                            [super refreshConversationTableViewIfNeeded];
                        }
                    });
                }
            }];
        }
    } else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [super didReceiveMessageNotification:notification];
            [blockSelf_ resetConversationListBackgroundViewIfNeeded];
        });
    }
}

- (CGFloat)rcConversationListTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 44;
    RCConversationModel *model = self.conversationListDataSource[indexPath.row];
    if (model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION) {
        height = 90;
    } else if (model.conversationType == ConversationType_PRIVATE) {
        height = 50;
    } else if (model.conversationType == ConversationType_SYSTEM) {
        return 80;
    }
    return height;
}

- (void)didTapCellPortrait:(RCConversationModel *)model {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([model.senderUserId intValue]) block:^(SHUser *user, NSError *error) {
        if (user) {
            ProfileViewController *profile = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            profile.user = user;
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.navigationController pushViewController:profile animated:YES];
            });
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"快聊";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
