//
//  SHColor.m
//  ShanHuo
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHColor.h"

@implementation SHColor

+ (UIColor *)disabledColor
{
    return GKColorFromRGB(0xaaaaaa);
}

+ (UIColor *)tintColor {
    return GKColorFromRGB(0x3cc8b0);
}
@end
