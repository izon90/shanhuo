//
//  SHTag.h
//  ShanHuo
//
//  Created by 小悟空 on 6/24/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "GKView.h"
#import "SHTag.h"

@interface SHTagView : GKView

@property (strong, nonatomic) UILabel *tagLabel;
@property (strong, nonatomic) SHTag *tag;
- (id)initWithTag:(SHTag *)tag;
@end
