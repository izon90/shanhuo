//
//  SHTag.m
//  ShanHuo
//
//  Created by 小悟空 on 6/24/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHTagView.h"

@implementation SHTagView
- (id)initWithTag:(SHTag *)tag {
    NSDictionary *attributes =
    @{NSFontAttributeName: [UIFont systemFontOfSize:12.0]};
    CGSize size = [tag.fullname sizeWithAttributes:attributes];
    self = [self initWithFrame:CGRectMake(0, 0, size.width + 8, 16)];
    if (self) {
        self.tag = tag;
        
        CGRect labelFrame =
        CGRectMake(8.0, 0.0, self.frame.size.width + 16, self.frame.size.height);
        self.tagLabel = [[UILabel alloc] initWithFrame:labelFrame];
        self.tagLabel.text = tag.fullname;
        self.tagLabel.font = [UIFont systemFontOfSize:10.0];
        [self addSubview:self.tagLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
