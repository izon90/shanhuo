//
//  SHFileBackend.h
//  ShanHuo
//
//  Created by 小悟空 on 7/8/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WKFile.h"
#import "SHUploadFile.h"

@interface SHFileBackend : GKBackend

- (void)requestUploadFile:(SHUploadFile *)file
                     user:(GKUser *)anUser
uploadProgressBlock:(void (^)(NSUInteger bytesWritten,
                              long long totalBytesWritten,
                              long long totalBytesExpectedToWrite))uploadProgressBlock
completion:(void (^)(NSArray *files, NSError *error))completion;
@end
