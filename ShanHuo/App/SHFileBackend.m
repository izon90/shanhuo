//
//  SHFileBackend.m
//  ShanHuo
//
//  Created by 小悟空 on 7/8/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHFileBackend.h"

@implementation SHFileBackend

- (void)requestUploadFile:(SHUploadFile *)file
                    user:(GKUser *)anUser
      uploadProgressBlock:(void (^)(NSUInteger bytesWritten,
                                    long long totalBytesWritten,
                                    long long totalBytesExpectedToWrite))uploadProgressBlock
completion:(void (^)(NSArray *files, NSError *error))completion {
    
    NSString *resourceID =
    [NSString stringWithFormat:@"%d", (int)file.resourceID];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"access_token"] = anUser.accessToken.accessToken;
    parameters[@"resource_id"] = resourceID;
    parameters[@"resource_type"] = @"Brief";
    
    AFHTTPRequestOperation *operation =
    [self.manager
     POST:[self URLWithResource:@"api/file"]
     parameters:nil
     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
         NSError *error;
         
         if (nil == file.data) {
             [formData appendPartWithFileURL:[NSURL URLWithString:file.path] name:@"files[]"
                                    fileName:file.name mimeType:@"video/quicktime"
                                       error:&error];
             
             
         } else {
             NSString *filename = file.name;
             [formData appendPartWithFileData:file.data name:@"files[]"
                                     fileName:filename mimeType:@"image/jpeg"];
         }
         
         [formData appendPartWithFileData:file.previewData name:@"previews[]"
                                 fileName:file.name mimeType:@"image/jpeg"];
         
         [formData appendPartWithFormData:[anUser.accessToken.accessToken
                                           dataUsingEncoding:NSUTF8StringEncoding]
                                     name:@"access_token"];
         
         [formData appendPartWithFormData:[resourceID dataUsingEncoding:NSUTF8StringEncoding]
                                     name:@"resource_id"];
         
         [formData appendPartWithFormData:[@"Brief" dataUsingEncoding:NSUTF8StringEncoding]
                                     name:@"resource_type"];
     }
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"上传成功");
         if (completion)
             completion([self filesWithJSON:responseObject], nil);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"上传失败");
         NSLog(@"%@", error.localizedDescription);
         NSLog(@"%@", operation.responseString);
         if (completion)
             completion(nil, error);
     }];
    
    [operation
     setUploadProgressBlock:^(NSUInteger bytesWritten,
                              long long totalBytesWritten,
                              long long totalBytesExpectedToWrite)
    {
//        DDLogVerbose(@"%lld / %lld", totalBytesWritten,
//                     totalBytesExpectedToWrite);
        uploadProgressBlock(bytesWritten, totalBytesWritten,
                            totalBytesExpectedToWrite);
    }];
    
    [operation start];
}

- (NSArray *)filesWithJSON:(NSArray *)JSON {
    NSMutableArray *files = [[NSMutableArray alloc] init];
    for (NSDictionary *item in JSON) {
        WKFile *file = WKFile.new;
        file.path = item[@"path"];
        file.name = item[@"name"];
        file.fileID = item[@"id"];
        file.size = [item[@"size"] integerValue];
        file.type = item[@"type"];
        
        [files addObject:file];
    }
    return files;
}
@end
