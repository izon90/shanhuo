//
//  UserCell.h
//  FreelanceProject
//
//  Created by heyong on 7/20/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHUser.h"
#import "HCSStarRatingView.h"

@interface UserCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLocationLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;

@property (strong, nonatomic) SHUser *user;

@end
