//
//  SHTag.m
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHTag.h"

@implementation SHTag

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"tagID": @"id",
             @"fullname": @"fullname"
             };
}

@end
