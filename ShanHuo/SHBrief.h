//
//  SHBrief.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "SHUser.h"

@interface SHBrief : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *briefID;
@property (nonatomic, copy, readonly) NSString *content;
@property (nonatomic, copy, readonly) NSNumber *numberOfFiles;
@property (nonatomic, copy, readonly) NSNumber *paid;
@property (nonatomic, copy, readonly) NSNumber *price;
@property (nonatomic, copy, readonly) NSDate *createdAt;
@property (nonatomic, copy, readonly) NSDate *updatedAt;
@property (nonatomic, copy, readonly) NSDate *startAt;
@property (nonatomic, strong, readonly) SHUser *user;
@property (nonatomic, copy, readonly) NSArray *tags;

+ (NSDateFormatter *)dateFormatter;

@end
