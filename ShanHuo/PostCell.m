//
//  PostCell.m
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "PostCell.h"

@implementation PostCell

- (void)setBrief:(SHBrief *)brief {
    if (_brief != brief) {
        _brief = brief;
    }
    self.contentLabel.text = _brief.content;
    self.deadlineLabel.text = [[SHBrief dateFormatter] stringFromDate:_brief.startAt];
    self.budgetLabel.text = [NSString stringWithFormat:@"¥ %@", _brief.price];
}

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
