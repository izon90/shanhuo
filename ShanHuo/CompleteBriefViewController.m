//
//  CompleteBriefViewController.m
//  ShanHuo
//
//  Created by heyong on 15/8/14.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "CompleteBriefViewController.h"
#import "NormalPersonCell.h"
#import "UIImageView+WebCache.h"
#import "SHNetworkEngine.h"

@interface CompleteBriefViewController ()

@property (strong, nonatomic) NSMutableArray *users;
@property (copy, nonatomic) NSNumber *currentPage;

@end

@implementation CompleteBriefViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"完成需求";
    
    __weak typeof(self) wself = self;

    self.currentPage = @(1);

    [self.tableView registerNib:[UINib nibWithNibName:@"NormalPersonCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getAllUsersWithPage:self.currentPage block:^(NSArray *objects, NSError *error) {
        if (objects) {
            if ([self.currentPage integerValue] == 1) {
                wself.users = [objects mutableCopy];
            } else {
                [wself.users addObjectsFromArray:objects];
            }
        } else {
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"Cell";
    NormalPersonCell *cell = (NormalPersonCell *)[tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    SHUser *user = self.users[indexPath.row];
    NSURL *avatarURl = [NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID]];
    [cell.avatarImageView sd_setImageWithURL:avatarURl placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
    cell.usernameLabel.text = user.name ? user.name : user.username;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

@end
