//
//  SHConversationCell.m
//  ShanHuo
//
//  Created by heyong on 15/8/5.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "SHConversationCell.h"

@implementation SHConversationCell

- (void)awakeFromNib {
    self.isFavorite = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(favoriteGesture:)];
    self.favoriteImageView.userInteractionEnabled = YES;
    [self.favoriteImageView addGestureRecognizer:tap];
}

- (void)favoriteGesture:(UITapGestureRecognizer *)sender {
    if (self.favoriteBlock) {
        self.favoriteBlock(self, !self.isFavorite);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
