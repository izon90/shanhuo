//
//  MediaMovie.h
//  ShanHuo
//
//  Created by heyong on 8/1/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaMovie : NSObject

@property (copy, nonatomic) NSURL *mediaURL;
@property (strong, nonatomic) UIImage *previewImage;
@property (copy, nonatomic) NSString *filename;
@property (strong, nonatomic) NSData *thumbnailData;
@end
