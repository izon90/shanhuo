//
//  UIAlertView+SH.m
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "UIAlertView+SH.h"

@implementation UIAlertView (SH)

+ (UIAlertView *)errorMessage:(NSString *)message {
    UIAlertView *alertView;
    alertView =
    [[UIAlertView alloc] initWithTitle:@"错误提示" message:message
                              delegate:self cancelButtonTitle:@"确定"
                     otherButtonTitles:nil];
    [alertView show];
    return alertView;
}
@end
