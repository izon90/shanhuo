//
//  UIAlertView+SH.h
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (SH)

+ (UIAlertView *)errorMessage:(NSString *)message;
@end
