//
//  UserLoginViewController.m
//  ShanHuo
//
//  Created by 小悟空 on 6/14/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHUserLoginViewController.h"
#import <RongIMKit/RongIMKit.h>
#import "SHNetworkEngine.h"

@interface SHUserLoginViewController ()

@end

@implementation SHUserLoginViewController
objection_requires_sel(@selector(service))

- (IBAction)forgetPasswordAction:(id)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewModel = [[SHUserLoginViewModel alloc] init];
    [[JSObjection defaultInjector] injectDependencies:self];
    
    @weakify(self)
    RAC(self.viewModel.authentication, username) =
        self.mobileTextField.rac_textSignal;
    RAC(self.viewModel.authentication, password) =
        self.passwordTextField.rac_textSignal;
    [self.viewModel.mobileValid subscribeNext:^(NSNumber *valid) {
        @strongify(self)
        self.mobileTextField.valid = valid.boolValue;
    }];
    [self.viewModel.passwordValid subscribeNext:^(NSNumber *valid) {
        @strongify(self)
        self.passwordTextField.valid = valid.boolValue;
    }];
    
    // Fake
//    NSString *fakeMobile   = @"18621068396",
//             *fakePassword = @"123456";
//    self.mobileTextField.text = self.viewModel.authentication.username =
//        fakeMobile;
//    self.passwordTextField.text = self.viewModel.authentication.password =
//        fakePassword;
    
    self.authenticateButton.rac_command = self.viewModel.loginCommand;
    [self.viewModel.loginValid subscribeNext:^(NSNumber *valid) {
        if (valid.boolValue) {
            [self.authenticateButton setBackgroundColor:[SHColor tintColor]];
        } else {
            [self.authenticateButton setBackgroundColor:[SHColor disabledColor]];
        }
    }];
    
    [[self.viewModel.loginCommand.executionSignals
      flattenMap:^RACStream *(RACSignal *signals) {
          return [[signals materialize] filter:^BOOL(RACEvent *event) {
              return event.eventType == RACEventTypeNext;
          }];
      }] subscribeNext:^(RACEvent *event) {
          @strongify(self)
          [self setCurrentUserInfo];
          [self getTokenAndConnectToRongCloud];
          [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLogin" object:nil];
          [self.navigationController dismissViewControllerAnimated:YES
                                                        completion:nil];
      }];
    [self.viewModel.loginCommand.errors subscribeNext:^(NSError *error) {
        [UIAlertView errorMessage:@"登录失败，用户名或者密码不正确。"];
    }];
}

- (void)setCurrentUserInfo {
    [[SHNetworkEngine sharedEngine] getUserInfoWithUserID:@([App shared].currentUser.userID) block:^(SHUser *user, NSError *error) {
        RCUserInfo *userInfo = [[RCUserInfo alloc] init];
        userInfo.userId = [NSString stringWithFormat:@"%@", user.userID];
        userInfo.name = user.name ? user.name : user.username;
        userInfo.portraitUri = [NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, user.userID];
        [RCIM sharedRCIM].currentUserInfo = userInfo;
        [[RCIM sharedRCIM]refreshUserInfoCache:userInfo withUserId:userInfo.userId];
    }];
}

- (void)getTokenAndConnectToRongCloud {
    NSString *accessToken = [App shared].currentUser.accessToken.accessToken;
    NSLog(@"access token: %@", accessToken);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPAdditionalHeaders = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", accessToken]};
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSURL *url = [NSURL URLWithString:[[GKConfig config] URLStringWithPath:@"api/user/rongyun/token"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    __weak typeof (self) wself = self;
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (((NSHTTPURLResponse *)response).statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSLog(@"%@", json);
            NSString *token = json[@"token"];
            [wself connectRongCloudWithToken:token];
        }
    }];
    [task resume];
}

- (void)connectRongCloudWithToken:(NSString *)token {
    [[RCIM sharedRCIM] connectWithToken:token success:^(NSString *userId) {
        NSLog(@"success connect to cloud with userId: %@", userId);
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"RongCloudToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } error:^(RCConnectErrorCode status) {
        NSLog(@"error: %@", @(status));
    } tokenIncorrect:^{
        NSLog(@"token incorrect");
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
