//
//  SHUserSignupViewModel.m
//  ShanHuo
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHUserSignupViewModel.h"

@implementation SHUserSignupViewModel
objection_requires_sel(@selector(service))
- (id)init {
    self = [super init];
    if (self) {
        self.registration = GKUserRegistration.new;
        [[JSObjection defaultInjector] injectDependencies:self];
    }
    return self;
}

- (RACSignal *)mobileValid {
    if (nil != _mobileValid)
        return _mobileValid;
    
    _mobileValid =
    [RACObserve(self, registration.mobile) map:^id(NSString *mobile) {
        return mobile.length == 11 ? @1 : @0;
    }];
    
    return _mobileValid;
}

- (RACSignal *)passwordValid {
    if (nil != _passwordValid)
        return _passwordValid;
    
    _passwordValid =
    [RACObserve(self, registration.password) map:^id(NSString *password) {
        return password.length > 4 ? @1 : @0;
    }];
    
    return _passwordValid;
}

- (RACSignal *)verificationCodeValid {
    if (nil != _verificationCodeValid)
        return _verificationCodeValid;
    
    _verificationCodeValid =
    [RACObserve(self, registration.verificationCode)
     map:^id(NSString *verificationCode) {
        return verificationCode.length == 6 ? @1 : @0;
    }];
    
    return _verificationCodeValid;
}

- (RACSignal *)registerValid {
    if (nil != _registerValid) {
        return _registerValid;
    }
    _registerValid =
    [RACSignal
     combineLatest:@[self.mobileValid, self.passwordValid,
                     self.verificationCodeValid]
     reduce:^id(NSNumber *mobileValid, NSNumber *passwordValid,
                NSNumber *verificationCodeValid) {
         return mobileValid.boolValue && passwordValid.boolValue &&
                verificationCodeValid.boolValue ? @1 : @0;
    }];
    return _registerValid;
}

- (RACCommand *)registerCommand {
    if (nil != _registerCommand) {
        return _registerCommand;
    }
    
    _registerCommand =
    [[RACCommand alloc]
     initWithEnabled:self.registerValid
     signalBlock:^RACSignal *(id input) {
         return [self.service signup:self.registration];
    }];
    
    return _registerCommand;
}
@end
