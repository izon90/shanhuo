//
//  SHUserLoginViewModel.m
//  ShanHuo
//
//  Created by 小悟空 on 6/20/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHUserLoginViewModel.h"

@implementation SHUserLoginViewModel
objection_requires_sel(@selector(service))

- (id)init {
    self = [super init];
    if (self) {
        self.authentication = [[GKUserAuthentication alloc] init];
        self.mobileValidation = MobileValidation.new;
        self.passwordValidation = PasswordValidation.new;
        [[JSObjection defaultInjector] injectDependencies:self];
    }
    return self;
}

- (RACSignal *)mobileValid {
    if (_mobileValid != nil)
        return _mobileValid;
    
    _mobileValid =
    [RACObserve(self.authentication, username) map:^id(id value) {
        return [NSNumber numberWithBool:[self.mobileValidation valid:value]];
    }];
    
    return _mobileValid;
}

- (RACSignal *)passwordValid {
    if (_passwordValid != nil)
        return _passwordValid;
    
    _passwordValid =
    [RACObserve(self.authentication, password) map:^id(id value) {
        return [NSNumber numberWithBool:[self.passwordValidation valid:value]];
    }];
    
    return _passwordValid;
}

- (RACSignal *)loginValid {
    if (_loginValid != nil)
        return _loginValid;
    
    _loginValid =
    [RACSignal
     combineLatest:@[self.mobileValid, self.passwordValid]
     reduce:^id (NSNumber *mobileValid, NSNumber *passwordValid){
         return mobileValid.boolValue && passwordValid.boolValue ? @1 : @0;
     }];
    
    return _loginValid;
}

- (RACCommand *)loginCommand {
    if (_loginCommand != nil)
        return _loginCommand;
    
    _loginCommand =
    [[RACCommand alloc]
     initWithEnabled:self.loginValid
     signalBlock:^RACSignal *(id input) {
         return [self.service authenticate:self.authentication];
     }];
    
    return _loginCommand;
}
@end
