//
//  UserSignupController.m
//  ShanHuo
//
//  Created by 小悟空 on 6/15/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHUserSignupController.h"

@interface SHUserSignupController ()

@end

@implementation SHUserSignupController
objection_requires_sel(@selector(userService), @selector(verificationCodeBackend))

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[JSObjection defaultInjector] injectDependencies:self];
        self.viewModel = SHUserSignupViewModel.new;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.calculagraph = 60;
    @weakify(self);
    RAC(self.viewModel.registration, mobile) =
        [self.mobileTextField rac_textSignal];
    RAC(self.viewModel.registration, password) =
        [self.passwordTextField rac_textSignal];
    RAC(self.viewModel.registration, verificationCode) =
        [self.verificationCodeTextField rac_textSignal];
    
    [self.viewModel.mobileValid subscribeNext:^(NSNumber *valid) {
        @strongify(self);
        self.mobileTextField.valid = valid.boolValue;
        self.sendVerificationCodeLabel.enabled = valid.boolValue;
    }];
    
    RAC(self.verificationCodeTextField, valid) =
        self.viewModel.verificationCodeValid;
    RAC(self.passwordTextField, valid) = self.viewModel.passwordValid;
    
//    self.mobileTextField.text = @"18621068396";
//    self.passwordTextField.text = @"123456";
//    self.verificationCodeTextField.text = @"000000";
    
    self.viewModel.registration.username = self.mobileTextField.text;
    self.viewModel.registration.mobile = self.mobileTextField.text;
    self.viewModel.registration.password = self.passwordTextField.text;
    self.viewModel.registration.verificationCode = self.verificationCodeTextField.text;

    [self.viewModel.registerValid subscribeNext:^(NSNumber *valid) {
        self.registerButton.enabled = valid.boolValue;
         @strongify(self);
         if (!valid.boolValue)
             self.registerButton.backgroundColor = SHColor.disabledColor;
         else
             self.registerButton.backgroundColor = SHColor.tintColor;
    }];
    self.registerButton.rac_command = self.viewModel.registerCommand;
    [self.viewModel.registerCommand.errors subscribeNext:^(id x) {
        [self userRegisterFailure:x];
    }];
    
    [[self.viewModel.registerCommand.executionSignals
      flattenMap:^RACStream *(RACSignal *signals) {
          return [[signals materialize] filter:^BOOL(RACEvent *event) {
              return event.eventType == RACEventTypeNext;
          }];
      }] subscribeNext:^(id _) {
          [self userRegisterSucceed];
      }];
}

- (void)userRegisterSucceed {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alertView;
    alertView = [[UIAlertView alloc]
                 initWithTitle:@"注册成功" message:@"用户注册成功" delegate:self
                 cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)userRegisterFailure:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *alertView;
    
    alertView = [[UIAlertView alloc]
                 initWithTitle:@"注册失败" message:error.localizedDescription
                 delegate:self
                 cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendVerificationCode:(id)sender {
    if (self.sendVerificationCodeLabel.enabled == NO)
        return;
    
    [self.verificationCodeBackend
     requestVerificationCode:self.viewModel.registration.mobile
     forResource:@"UserRegister"
     completion:^(NSError *error) {
        
    }];
    self.sendVerificationCodeLabel.enabled = NO;
    
    [self timerCountdown];
    self.timer =
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self
                                   selector:@selector(timerCountdown)
                                   userInfo:nil repeats:YES];

}

- (void)timerCountdown {
    NSString *countdownTitle;
    countdownTitle = [NSString stringWithFormat:@"%d秒后可重新发送",
                      (int)self.calculagraph];
    self.sendVerificationCodeLabel.text = countdownTitle;
    self.calculagraph -= 1;
    
    if (-2 == self.calculagraph) {
        self.calculagraph = 60;
        self.sendVerificationCodeLabel.enabled = YES;
        self.sendVerificationCodeLabel.text = @"发送验证码";
        [self.timer invalidate];
    }
}

- (IBAction)sigupDidTap:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
