//
//  UserLoginViewController.h
//  ShanHuo
//
//  Created by 小悟空 on 6/14/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHUserLoginViewModel.h"

@interface SHUserLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet GKTextField *mobileTextField;
@property (weak, nonatomic) IBOutlet GKTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *authenticateButton;
@property (strong, nonatomic) SHUserLoginViewModel *viewModel;
@property (strong, nonatomic) id<GKUserService> service;
@end
