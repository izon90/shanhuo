//
//  UserSignupController.h
//  ShanHuo
//
//  Created by 小悟空 on 6/15/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHUserSignupViewModel.h"

@interface SHUserSignupController : UIViewController

@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) NSInteger calculagraph;
@property (strong, nonatomic) IBOutlet UILabel *sendVerificationCodeLabel;
@property (strong, nonatomic) IBOutlet GKTextField *mobileTextField;
@property (strong, nonatomic) IBOutlet GKTextField *passwordTextField;
@property (strong, nonatomic) IBOutlet GKTextField *verificationCodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) id<GKVerificationCodeBackend> verificationCodeBackend;
@property (strong, nonatomic) id<GKUserService> userService;
@property (strong, nonatomic) SHUserSignupViewModel *viewModel;
- (IBAction)sendVerificationCode:(id)sender;
- (IBAction)sigupDidTap:(id)sender;
@end
