//
//  SHUserSignupViewModel.h
//  ShanHuo
//
//  Created by 小悟空 on 6/18/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHUserSignupViewModel : NSObject

@property (strong, nonatomic) GKUserRegistration *registration;
@property (strong, nonatomic) RACSignal *mobileValid;
@property (strong, nonatomic) RACSignal *passwordValid;
@property (strong, nonatomic) RACSignal *verificationCodeValid;
@property (strong, nonatomic) RACSignal *registerValid;
@property (strong, nonatomic) RACCommand *registerCommand;
@property (strong, nonatomic) id<GKUserService> service;
@end
