//
//  SHUserLoginViewModel.h
//  ShanHuo
//
//  Created by 小悟空 on 6/20/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Validations.h"

@interface SHUserLoginViewModel : NSObject

@property (strong, nonatomic) GKUserAuthentication *authentication;
@property (strong, nonatomic) RACSignal *mobileValid;
@property (strong, nonatomic) RACSignal *passwordValid;
@property (strong, nonatomic) RACSignal *loginValid;
@property (strong, nonatomic) RACCommand *loginCommand;
@property (strong, nonatomic) MobileValidation *mobileValidation;
@property (strong, nonatomic) PasswordValidation *passwordValidation;
@property (strong, nonatomic) id<GKUserService> service;
@end
