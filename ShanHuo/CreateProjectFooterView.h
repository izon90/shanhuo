//
//  CreateProjectFooterView.h
//  ShanHuo
//
//  Created by heyong on 8/1/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateProjectFooterView : UIView

@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

@end
