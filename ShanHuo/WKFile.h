//
//  SHFile.h
//  ShanHuo
//
//  Created by 小悟空 on 7/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHBaseFile.h"

@interface WKFile : SHBaseFile

@property (strong, nonatomic) NSString *fileID;

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) UIImage *image;

@end
