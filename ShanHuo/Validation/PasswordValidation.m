//
//  PasswordValidation.m
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "PasswordValidation.h"

@implementation PasswordValidation

- (BOOL)valid:(id)value {
    NSString *password = (NSString *)value;
    NSInteger minLength = 4;
    if (password.length < minLength)
        return false;
    
    return true;
}
@end
