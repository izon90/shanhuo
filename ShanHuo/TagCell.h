//
//  TagCell.h
//  FreelanceProject
//
//  Created by heyong on 7/20/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagCell : UITableViewCell

@property (copy, nonatomic) NSArray *tags;
@property (copy, nonatomic) NSNumber *tagsViewHeight;

@end
