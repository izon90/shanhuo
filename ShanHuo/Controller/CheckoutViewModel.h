//
//  CheckoutViewModel.h
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    GKPaymentTypeAlipay,
    GKPaymentTypeWechat
} GKPaymentType;

@interface CheckoutViewModel : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSDate *paymentDate;
@property (strong, nonatomic) NSDecimalNumber *price;
@property (assign, nonatomic) GKPaymentType paymentType;
@property (assign, nonatomic) BOOL needInvoice;

@property (strong, nonatomic) RACSignal *checkoutValid;
@property (strong, nonatomic) RACCommand *checkoutCommand;
@end
