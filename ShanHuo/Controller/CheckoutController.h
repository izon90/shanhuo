//
//  CheckoutController.h
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckoutViewModel.h"

@interface CheckoutController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *paymentDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkoutButton;
@property (strong, nonatomic) CheckoutViewModel *viewModel;
@end
