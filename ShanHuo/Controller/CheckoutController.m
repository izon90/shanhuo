//
//  CheckoutController.m
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "CheckoutController.h"
#import "WeixinRequest.h"

@implementation CheckoutController

- (void)wechatpay {
    
}

- (void)alipay {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.editing = YES;
    self.tableView.allowsSelectionDuringEditing = YES;
    
    self.viewModel = [[CheckoutViewModel alloc] init];
    
    self.viewModel.title = @"闪活App开发";
    self.viewModel.paymentDate = [[NSDate alloc] init];
    self.viewModel.price = [NSDecimalNumber decimalNumberWithString:@"9999.99"];
    
    @weakify(self)
    RAC(self.titleLabel, text) =  RACObserve(self.viewModel, title);
    
    [RACObserve(self.viewModel, price) subscribeNext:^(NSDecimalNumber *price) {
        @strongify(self)
        self.priceLabel.text = [NSString stringWithFormat:@"%.2f元",
                                price.floatValue];
    }];
    
    [RACObserve(self.viewModel, paymentDate)
     subscribeNext:^(NSDate *paymentDate) {
        @strongify(self)
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
         NSString *formatted = [formatter stringFromDate:paymentDate];
        self.paymentDateLabel.text = formatted;
    }];
    
    self.checkoutButton.rac_command = self.viewModel.checkoutCommand;
    [self.viewModel.checkoutValid subscribeNext:^(NSNumber *valid) {
        @strongify(self);
        
        if (valid.boolValue)
            self.checkoutButton.backgroundColor = [SHColor tintColor];
        else
            self.checkoutButton.backgroundColor = [SHColor disabledColor];
    }];
    [self.tableView
     selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]
     animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return false;
            break;
            
        default:
            return true;
            break;
    }
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 1: 
            switch (indexPath.row) {
                case 0:
//                    [[WeixinRequest shareRequest] sendPay_demo];
                    [self wechatpay];
                    [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES];
                    break;
                    
                default:
                    [self alipay];
                    [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES];
                    break;
            }
            break;
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section {
    if (2 == section)
        return 80.0f;
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView
viewForFooterInSection:(NSInteger)section {
    if (2 == section) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 80)];
        
        GKButton *button = [[GKButton alloc] initWithFrame:CGRectMake((tableView.frame.size.width - 240.0) / 2, 20.0, 240.0, 40.0)];
        [button setTitle:@"确定支付" forState:UIControlStateNormal];
        button.backgroundColor = SHColor.tintColor;
        button.cornerRadius = 3.0f;
        
        [view addSubview:button];
        return view;
    }
    return nil;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
//           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//    switch (indexPath.section) {
//        case 0:
//            return UITableViewCellEditingStyleNone;
//            break;
//            
//        default:
//            break;
//    }
//    return;
//}
@end
