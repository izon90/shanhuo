//
//  CheckoutViewModel.m
//  ShanHuo
//
//  Created by 小悟空 on 6/23/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "CheckoutViewModel.h"

@implementation CheckoutViewModel

- (RACSignal *)checkoutValid {
    if (_checkoutValid != nil) {
        return _checkoutValid;
    }
    _checkoutValid = [RACObserve(self, paymentType) map:^id(NSNumber *type) {
        return @1;
    }];
    return _checkoutValid;
}

- (RACCommand *)checkoutCommand {
    if (_checkoutCommand != nil) {
        return _checkoutCommand;
    }
    @weakify(self)
    _checkoutCommand =
    [[RACCommand alloc]
     initWithEnabled:self.checkoutValid
     signalBlock:^RACSignal *(id input) {
        
//        @strongify(self)
         return [RACSignal empty];
         
     }];
    return _checkoutCommand;
}
@end
