//
//  SHBaseFile.h
//  ShanHuo
//
//  Created by 小悟空 on 7/18/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHBaseFile : NSObject

@property (strong, nonatomic) NSString *preview;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *path;
@property (assign, nonatomic) long long size;
@property (strong, nonatomic) NSDate *createdAt;
@end
