//
//  TagTableViewController.m
//  ShanHuo
//
//  Created by 小悟空 on 6/29/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "TagTableViewController.h"

@interface TagTableViewController ()

@end

@implementation TagTableViewController
- (id)initWithParent:(Tag *)tag
           selection:(void(^)(Tag *tag, NSError *error))selection {
    self = [self initWithNibName:@"TagTableViewController" bundle:nil];
    if (self) {
        self.backend = TagBackend.new;
        self.service = TagService.new;
        self.selection = selection;
        if (nil != tag && 0 < tag.children.count) {
            self.tags = tag.children;
        } else {
            [self.service tagsWithParentID:tag.tagID
                              completion:^(NSArray *tags, NSError *error)
            {
                self.tags = tags;
                [self.tableView reloadData];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }];
        }
        [self.tableView reloadData];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择标签";
    
    if (nil == self.tags)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return self.tags.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (nil == cell) {
        cell =
        [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                               reuseIdentifier:@"TagTableViewCell"];
    }
    
    Tag *tag = self.tags[indexPath.row];
    cell.textLabel.text = tag.fullname;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Tag *tag = self.tags[indexPath.row];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.service tagsWithParentID:tag.tagID
                      completion:^(NSArray *tags, NSError *error)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        // 标签没有下一级内容
        if (nil == tags || 0 == tags.count) {
            [self.navigationController popViewControllerAnimated:YES];
            if (self.selection) {
                self.selection(tag, nil);
            }
        } else {
            TagTableViewController *tagController =
            [[TagTableViewController alloc]
             initWithParent:tag
             selection:^(Tag *tag, NSError *error)
             {
                 self.selectedChild = tag;
                 [self.navigationController popViewControllerAnimated:YES];
                 if (self.selection) {
                     self.selection(tag, nil);
                 }
             }];
            
            [self.navigationController pushViewController:tagController
                                                 animated:YES];
        }
    }];


}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
