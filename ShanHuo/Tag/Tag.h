//
//  Tag.h
//  ShanHuo
//
//  Created by 小悟空 on 6/29/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tag : NSObject

@property (assign, nonatomic) NSInteger tagID;
@property (weak, nonatomic) Tag *parent;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *fullname;
@property (strong, nonatomic) NSMutableArray *children;
@end
