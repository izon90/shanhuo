//
//  TagTableViewController.h
//  ShanHuo
//
//  Created by 小悟空 on 6/29/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tag.h"
#import "TagBackend.h"
#import "TagService.h"

@interface TagTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *tags;
@property (strong, nonatomic) TagBackend *backend;
@property (strong, nonatomic) TagService *service;
@property (copy, nonatomic) void(^selection)(Tag *tag, NSError *error);
@property (strong, nonatomic) Tag *selectedChild;
- (id)initWithParent:(Tag *)tag
           selection:(void(^)(Tag *tag, NSError *error))selection;
@end
