//
//  TagBackend.h
//  ShanHuo
//
//  Created by 小悟空 on 6/29/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tag.h"

@interface TagBackend : GKBackend

- (void)requestTagsWithParent:(NSInteger)parentID
                   completion:(void(^)(NSArray *tags, NSError *error))completion;
@end
