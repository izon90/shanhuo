//
//  TagBackend.m
//  ShanHuo
//
//  Created by 小悟空 on 6/29/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "TagBackend.h"

@implementation TagBackend

- (void)requestTagsWithParent:(NSInteger)parentID
                   completion:(void(^)(NSArray *tags, NSError *error))completion
{
    NSString *tagURL;
    if (0 == parentID)
        tagURL = [self URLWithResource:@"api/tag"];
    else
        tagURL = [NSString stringWithFormat:@"%@/%d",
                  [self URLWithResource:@"api/tag"], (int)parentID];
    [self.manager
     GET:tagURL
     parameters:nil
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         if (completion)
             completion([self tagsWithJSON:responseObject], nil);
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         if (error) {
//             DDLogError(@"%@", error.localizedDescription);
//             DDLogError(@"%@", operation.responseString);
         }
         if (completion)
             completion(nil, error);
     }];
}

- (Tag *)tagWithJSON:(NSDictionary *)json {
    Tag *tag = [[Tag alloc] init];
    
    tag.name = [json objectForKey:@"name"];
    tag.fullname = [json objectForKey:@"fullname"];
    if ([tag.fullname isKindOfClass:[[NSNull null] class]]) {
        if (nil == tag.parent) {
            tag.fullname = tag.name;
        } else {
            tag.fullname = [NSString stringWithFormat:@"%@ - %@",
                            tag.parent.name, tag.name];
        }
    }
    tag.tagID = [[json objectForKey:@"id"] integerValue];
    
    return tag;
}

- (NSArray *)tagsWithJSON:(NSArray *)json {
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    for (NSDictionary *item in json) {
        [tags addObject:[self tagWithJSON:item]];
    }
    return tags;
}
@end
