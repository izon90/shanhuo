//
//  TagService.m
//  ShanHuo
//
//  Created by 小悟空 on 6/30/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "TagService.h"

@interface TagService()

@property (strong, nonatomic) NSMutableDictionary *indexes;
@end

@implementation TagService

- (id)init {
    self = [super init];
    if (self) {
        self.tags = [[NSMutableArray alloc] init];
        self.indexes = [[NSMutableDictionary alloc] init];
        self.backend = TagBackend.new;
    }
    return self;
}

- (Tag *)findWithTagID:(NSInteger)tagID {
    return self.indexes[[NSString stringWithFormat:@"%d", (int)tagID]];
}

- (BOOL)hasTag:(NSInteger)tagID {
    if (nil == [self findWithTagID:tagID])
        return NO;
    else
        return YES;
}

- (void)indexTags:(NSArray *)tags {
    for (Tag *tag in tags)
        [self.indexes setObject:tag
                         forKey:[NSString stringWithFormat:@"%d",
                                 (int)tag.tagID]];
}

- (void)tagsWithParentID:(NSInteger)parentID
              completion:(void(^)(NSArray *tags, NSError *error))completion
{
    Tag *parent = [self findWithTagID:parentID];
    BOOL cached = nil != parent;
    if (cached) {
        Tag *parent = [self findWithTagID:parentID];
        if (0 < parent.children.count) {
//            DDLogVerbose(@"Tag[%d] 从内存加载", (int)parentID);
            cached = YES;
            if (completion)
                completion(parent.children, nil);
            return;
        }
    }
    
    [self.backend requestTagsWithParent:parentID
                             completion:^(NSArray *tags, NSError *error)
     {
         if (!cached)
             [self indexTags:tags];
         if (parent != nil)
             [parent.children addObjectsFromArray:tags];
         if (completion)
             completion(tags, nil);
     }];
}

+ (instancetype)shared
{
    static TagService *_shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
    });
    return _shared;
}
@end
