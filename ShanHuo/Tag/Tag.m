//
//  Tag.m
//  ShanHuo
//
//  Created by 小悟空 on 6/29/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "Tag.h"

@implementation Tag

- (id)init {
    self = [super init];
    if (self) {
        self.children = [[NSMutableArray alloc] init];
    }
    return self;
}
@end
