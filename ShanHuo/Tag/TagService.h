//
//  TagService.h
//  ShanHuo
//
//  Created by 小悟空 on 6/30/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TagBackend.h"

@interface TagService : NSObject

@property (strong, nonatomic) NSMutableArray *tags;
@property (strong, nonatomic) TagBackend *backend;

- (void)tagsWithParentID:(NSInteger)parentID
              completion:(void(^)(NSArray *tags, NSError *error))completion;
+ (instancetype)shared;
@end
