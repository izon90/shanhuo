//
//  ChangeNameController.h
//  ShanHuo
//
//  Created by heyong on 7/27/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChangeNameController;

@protocol ChangeNameControllerDelegate <NSObject>

- (void)changeNameController:(ChangeNameController *)controller didChangeName:(NSString *)name;

@end

@interface ChangeNameController : UITableViewController

@property (weak, nonatomic) id <ChangeNameControllerDelegate> delegate;

@end
