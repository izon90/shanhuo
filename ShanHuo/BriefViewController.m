//
//  MyPostViewController.m
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "BriefViewController.h"
#import "PostCell.h"
#import "SHNetworkEngine.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "SHBrief.h"
#import "SHTag.h"
#import <RongIMKit/RongIMKit.h>
#import <MJRefresh/MJRefresh.h>
#import "CompleteBriefViewController.h"

@interface BriefViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *briefs;
@property (copy, nonatomic) NSNumber *currentPage;

@end

@implementation BriefViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.910f green:0.918f blue:0.922f alpha:1.0f];
    
    self.title = @"我发布的需求";
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, view.frame.size.width - 30, view.frame.size.height)];
    label.textColor = [UIColor colorWithRed:0.686f green:0.690f blue:0.698f alpha:1.0f];
    label.text = @"发布时间：2015-06-3 18：23";
    [view addSubview:label];
    
    self.currentPage = @(1);

    [self loadData];
    
    self.tableView.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        NSInteger page = [self.currentPage integerValue];
        page += 1;
        self.currentPage = @(page);
        [self loadData];
    }];
}

- (void)loadData {
    __weak typeof(self) wself = self;
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getUserBriefsWithUserID:self.user.userID page:self.currentPage block:^(NSArray *objects, NSError *error) {
        if (objects && [objects count] > 0) {
            if ([self.currentPage integerValue] == 1) {
                wself.briefs = [objects mutableCopy];
            } else {
                [wself.briefs addObjectsFromArray:objects];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.currentPage integerValue] > 1) {
                    [self.tableView.footer endRefreshing];
                }
                [wself.tableView reloadData];
                [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = @"没有数据";
                hud.margin = 10.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            });
        }
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = @"网络错误";
                hud.margin = 10.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            });
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.briefs count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostCell *cell = (PostCell *)[tableView dequeueReusableCellWithIdentifier:@"PostCell" forIndexPath:indexPath];
    SHBrief *brief = self.briefs[indexPath.row];
    NSArray *tags = brief.tags;
    [cell setBrief:brief];
    
    CGRect previousLabelFrame = CGRectZero;
    BOOL previousLabelExist = NO;
    
    for (int i = 0; i < [tags count]; i++) {
        SHTag *tagObject = tags[i];
        NSString *tag = tagObject.fullname;
        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag];
        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, tag.length)];
        CGRect rect = [attribtedTag boundingRectWithSize:CGSizeMake(cell.tagView.frame.size.width, 20) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        UILabel *label = [[UILabel alloc] init];

        if (previousLabelExist) {
            label.frame = CGRectMake(previousLabelFrame.origin.x + previousLabelFrame.size.width + 5, 0, rect.size.width+10, 20);
        } else {
            label.frame = CGRectMake(0, 0, rect.size.width+10, 20);
        }
        
        label.text = tag;
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        label.textAlignment = NSTextAlignmentCenter;
        
        previousLabelFrame = label.frame;
        previousLabelExist = YES;
        
        [cell.tagView addSubview:label];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 70)];
    
    if ([self.user.userID integerValue] != [App shared].currentUser.userID) {
        UIButton *contactButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, (view.frame.size.width-2)/2, 60)];
        contactButton.backgroundColor = [UIColor whiteColor];
        contactButton.tintColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        [contactButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [contactButton setTitle:@"联系他" forState:UIControlStateNormal];
        [contactButton addTarget:self action:@selector(contactAtion:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:contactButton];
        
        UIButton *favoriteButton = [[UIButton alloc] initWithFrame:CGRectMake(contactButton.frame.size.width + 2, 0, (view.frame.size.width-2)/2, 60)];
        favoriteButton.backgroundColor = [UIColor whiteColor];
        favoriteButton.tintColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        [favoriteButton setTitle:@"收藏" forState:UIControlStateNormal];
        [favoriteButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [favoriteButton addTarget:self action:@selector(favoriteAtion:) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:favoriteButton];
    } else {
        UIButton *completeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 60)];
        completeButton.backgroundColor = [UIColor whiteColor];
        completeButton.tintColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        [completeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [completeButton setTitle:@"完成需求" forState:UIControlStateNormal];
        [completeButton addTarget:self action:@selector(completeAction:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:completeButton];
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, view.frame.size.width - 30, view.frame.size.height)];
    label.textColor = [UIColor colorWithRed:0.686f green:0.690f blue:0.698f alpha:1.0f];
    SHBrief *brief = self.briefs[section];
    label.text = [[SHBrief dateFormatter] stringFromDate:brief.updatedAt];
    [view addSubview:label];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 220;
}

- (void)completeAction:(UIButton *)sender {
    CompleteBriefViewController *completeBriefViewController = [[CompleteBriefViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:completeBriefViewController animated:YES];
}

- (void)contactAtion:(UIButton *)sender {
//    RCConversationViewController *conversationVC = [[RCConversationViewController alloc]init];
//    conversationVC.conversationType = ConversationType_PRIVATE; //会话类型，这里设置为 PRIVATE 即发起单聊会话。
//    conversationVC.targetId = [self.user.userID stringValue]; // 接收者的 targetId，这里为举例。
//    conversationVC.userName = self.user.name; // 接受者的 username，这里为举例。
//    //    conversationVC.title = @"hello"; // 会话的 title。
//    [self.tabBarController.tabBar setHidden:YES];
//    // 把单聊视图控制器添加到导航栈。
//    [self.navigationController pushViewController:conversationVC animated:YES];
}

- (void)favoriteAtion:(UIButton *)sender {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
