//
//  SHRequest.m
//  ShanHuo
//
//  Created by heyong on 15/8/6.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import "SHBriefRequest.h"

@implementation SHBriefRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"requestID": @"id",
             @"owernID": @"owner_id",
             @"briefID": @"brief_id",
             @"userID": @"user_id",
             @"status": @"status"
             };
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if (value == nil) {
            return [NSNumber numberWithInt:0];
        } else {
            return value;
        }
    }];
}

@end
