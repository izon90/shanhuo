//
//  PersonViewController.h
//  FreelanceProject
//
//  Created by heyong on 7/22/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHBrief.h"

@interface PersonViewController : UIViewController

@property (nonatomic, strong) SHBrief *brief;

@end
