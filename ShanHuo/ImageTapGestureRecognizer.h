//
//  ImageTapGestureRecognizer.h
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageTapGestureRecognizer : UITapGestureRecognizer

@property (strong, nonatomic) NSURL *movieURL;
@property (copy, nonatomic) NSNumber *isMovie;

@end
