//
//  BriefViewModel.h
//  ShanHuo
//
//  Created by 小悟空 on 7/9/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WKBrief.h"
#import "BriefBackend.h"
#import "SHFileBackend.h"

@interface BriefViewModel : NSObject

@property (strong, nonatomic) WKBrief *brief;
@property (strong, nonatomic) BriefBackend *backend;
@property (strong, nonatomic) SHFileBackend *fileBackend;

@property (strong, nonatomic) RACSignal *contentValid;
@property (strong, nonatomic) RACSignal *deadlineValid;
@property (strong, nonatomic) RACSignal *tagValid;
@property (strong, nonatomic) RACSignal *priceValid;
@property (strong, nonatomic) RACSignal *allValid;

@property (strong, nonatomic) RACCommand *postCommand;
@end
