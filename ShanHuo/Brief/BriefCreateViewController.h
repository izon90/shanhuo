//
//  BriefViewController.h
//  ShanHuo
//
//  Created by 小悟空 on 7/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SHQueue.h"
//#import "SHQueueItem.h"
#import "SHAdditionViewCell.h"
#import "GKLinearLayoutView.h"
#import "SHFileBackend.h"
#import "BriefViewModel.h"

@interface BriefCreateViewController : UIViewController
<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextView *contentTextView;
@property (strong, nonatomic) IBOutlet GKLinearLayoutView *additions;
@property (strong, nonatomic) IBOutlet GKLinearLayoutView *tagsView;
@property (strong, nonatomic) IBOutlet UIButton *selectTagButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *priceTextField;
@property (strong, nonatomic) IBOutlet UITextField *deadlineTextField;
@property (strong, nonatomic) IBOutlet UIButton *postButton;
@property (strong, nonatomic) IBOutlet UIButton *tagButton;
@property (strong, nonatomic) NSMutableArray *tagViews;
@property (strong, nonatomic) UIButton *addAdditionButton;
@property (strong, nonatomic) SHFileBackend *fileBackend;
@property (strong, nonatomic) BriefViewModel *viewModel;
@property (assign, nonatomic) long long totalBytesWritten;
@property (assign, nonatomic) long long totalBytesExpectedToWriten;
- (IBAction)selectTagDidTap:(id)sender;
- (IBAction)selectDateTimeDidTap:(id)sender;
@end
