//
//  Brief.h
//  ShanHuo
//
//  Created by 小悟空 on 7/9/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tag.h"

@interface WKBrief : NSObject

@property (assign, nonatomic) NSInteger briefID;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSMutableArray *tags;
@property (strong, nonatomic) NSDate *deadline;
@property (strong, nonatomic) NSDecimalNumber *price;
@property (strong, nonatomic) NSMutableArray *files;

@property (strong, nonatomic) NSMutableArray *uploadFiles;
@end
