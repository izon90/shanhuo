//
//  BriefBackend.m
//  ShanHuo
//
//  Created by 小悟空 on 7/10/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "BriefBackend.h"

@implementation BriefBackend

- (void)requestTouchBriefWithUser:(GKUser *)user
                       completion:(void(^)(WKBrief *brief, NSError *error))completion {
    
    NSString *url = [self URLWithResource:@"api/brief/touch"];

    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", [App shared].currentUser.accessToken.accessToken] forHTTPHeaderField:@"Authorization"];
    [self.manager
     POST:url
     parameters:nil
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         WKBrief *brief = [self briefWithJSON:responseObject];
         
         completion(brief, nil);
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         completion(nil, error);
     }];
}

- (void)requestUpdateBrief:(WKBrief *)brief
                completion:(void(^)(WKBrief *brief, NSError *error))completion {
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", [App shared].currentUser.accessToken.accessToken] forHTTPHeaderField:@"Authorization"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    
    NSDictionary *parameters =
  @{ @"content": brief.content,
     @"price": [NSString stringWithFormat:@"%.2f", brief.price.floatValue],
     @"start_at":[formatter stringFromDate:brief.deadline]
    };
    NSString *url =
    [self URLWithResource:[NSString stringWithFormat:@"api/brief/%d", brief.briefID]];
    [self.manager
     PUT:url
     parameters:parameters
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         WKBrief *brief = [self briefWithJSON:responseObject];
         
         completion(brief, nil);

     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"%@", operation.responseString);
         NSLog(@"%@", error.localizedDescription);
         completion(nil, error);
     }];
}

- (NSString *)safe:(id)x
{
    if ([x isKindOfClass:[NSNull class]])
        return @"";
    return x;
}

- (WKBrief *)briefWithJSON:(NSDictionary *)JSON {
    // 合并Brief对象
    // 更新集合，删除远程没有的项，
    
    WKBrief *brief = WKBrief.new;
    brief.briefID = [[JSON objectForKey:@"id"] integerValue];
    brief.content = [self safe:JSON[@"content"]];
    
    id price = JSON[@"price"];
    if (![price isKindOfClass:[NSNull class]]) {
        brief.price =
            [[NSDecimalNumber alloc] initWithFloat:[price floatValue]];
    }
    brief.files = [[NSMutableArray alloc] init];
    for (NSDictionary *item in JSON[@"files"]) {
        WKFile *file = [[WKFile alloc] init];
        
        file.fileID = item[@"id"];
        file.path = item[@"path"];
        file.size = [item[@"size"] integerValue];
        file.name = item[@"name"];
        file.preview = item[@"preview"];
        
        [brief.files addObject:file];
    }
    
    return brief;
}
@end
