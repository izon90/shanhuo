//
//  Brief.m
//  ShanHuo
//
//  Created by 小悟空 on 7/9/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "WKBrief.h"

@implementation WKBrief

- (id)init {
    self = [super init];
    if (self) {
        self.tags = [[NSMutableArray alloc] init];
        self.uploadFiles = [[NSMutableArray alloc] init];
    }
    return self;
}
@end
