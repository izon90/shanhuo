//
//  SHAdditionViewCell.m
//  ShanHuo
//
//  Created by 小悟空 on 7/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHAdditionViewCell.h"
#import <Masonry/Masonry.h>

@implementation SHAdditionViewCell

- (id)initWithFile:(SHUploadFile *)file {
    self = [self init];
    if (self) {
        self.file = file;
        
        [self setup];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setup {
    self.filenameButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.filenameButton setTitle:self.file.name
                         forState:UIControlStateNormal];
    self.filenameButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    self.filenameButton.titleLabel.textColor = [UIColor blackColor];
    [self.filenameButton setTitle:self.file.name
                         forState:UIControlStateNormal];
    
    self.filenameLabel = [[UILabel alloc] init];
    self.filenameLabel.text = self.file.name;
    self.filenameLabel.font = [UIFont systemFontOfSize:12.0];
    self.typeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.removeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.removeButton setImage:[UIImage imageNamed:@"file_cancel"]
                       forState:UIControlStateNormal];
    
    [self addSubview:self.typeButton];
    [self addSubview:self.filenameLabel];
    [self addSubview:self.removeButton];
    
    [self.filenameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.typeButton.mas_trailing).offset(5);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
    }];

    [self.typeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@12);
        make.height.equalTo(@12);
        make.leading.equalTo(self.mas_leading);
        make.top.equalTo(self.mas_top);
    }];
    
    [self.removeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.filenameLabel.mas_trailing).offset(5);
        make.top.equalTo(self.mas_top);
        make.width.equalTo(@14);
        make.height.equalTo(@14);
        make.trailing.equalTo(self.mas_trailing);
    }];
    
    NSString *extension = [[self.file.name pathExtension] lowercaseString];
    NSString *typeAssetName;
    if ([extension isEqualToString:@"png"] ||
        [extension isEqualToString:@"jpg"]) {
        
        typeAssetName = @"file_type_image";
    } else {
        typeAssetName = @"file_type_normal";
    }
    [self.typeButton setImage:[UIImage imageNamed:typeAssetName]
               forState:UIControlStateNormal];
    
    [self.removeButton addTarget:self action:@selector(removeButtonDidTap:)
                forControlEvents:UIControlEventTouchUpInside];
}

- (void)removeButtonDidTap:(id)sender {
    if (self.removeAddition)
        self.removeAddition(self);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
