//
//  SHAdditionViewCell.h
//  ShanHuo
//
//  Created by 小悟空 on 7/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHUploadFile.h"
#import "GKLinearLayoutViewItem.h"

@interface SHAdditionViewCell : UIView

@property (strong, nonatomic) UIButton *filenameButton;
@property (strong, nonatomic) UILabel *filenameLabel;
@property (strong, nonatomic) UIButton *removeButton;
@property (strong, nonatomic) UIButton *typeButton;
@property (strong, nonatomic) SHUploadFile *file;
@property (weak, nonatomic) GKLinearLayoutViewItem *item;
@property (copy, nonatomic) void(^removeAddition)(SHAdditionViewCell *cell);
- (id)initWithFile:(SHUploadFile *)file;
@end
