//
//  BriefViewController.m
//  ShanHuo
//
//  Created by 小悟空 on 7/2/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "BriefCreateViewController.h"
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import "TagTableViewController.h"
#import <Masonry/Masonry.h>

@interface BriefCreateViewController ()

@end

@implementation BriefCreateViewController

- (id)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.fileBackend = SHFileBackend.new;
    self.viewModel = [[BriefViewModel alloc] init];
    self.tagViews = NSMutableArray.new;
}

- (void)hideKeyboardTapped {
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *hideKeyboardTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboardTapped)];
    self.scrollView.userInteractionEnabled = YES;
    [self.scrollView addGestureRecognizer:hideKeyboardTap];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.minimumDate = [[NSDate alloc] init];
    [datePicker addTarget:self action:@selector(deadlineDatePickerValueChange:)
         forControlEvents:UIControlEventValueChanged];
    self.deadlineTextField.inputView = datePicker;
    
    self.scrollView.canCancelContentTouches = NO;
    self.scrollView.exclusiveTouch = NO;
    
    CGSize contentSize =
    CGSizeMake(self.view.frame.size.width - 20, 166.0);
    self.contentTextView.contentSize = contentSize;
    
    [self.contentTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo([NSNumber numberWithFloat:contentSize.width]);
        make.height.equalTo([NSNumber numberWithFloat:contentSize.height]);
    }];
    
    self.additions.orientation = CSLinearLayoutViewOrientationVertical;
    self.addAdditionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.addAdditionButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    self.addAdditionButton.titleLabel.textColor = GKColorFromRGB(0xacacac);
    [self.addAdditionButton addTarget:self action:@selector(addAdditionDidTap:)
                     forControlEvents:UIControlEventTouchUpInside];
    
    NSString *addAdditionLabelText = @"点击选择添加文件";
    NSMutableAttributedString *attributed =
    [[NSMutableAttributedString alloc] initWithString:addAdditionLabelText];
    [attributed addAttribute:NSUnderlineStyleAttributeName
                       value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                       range:NSMakeRange(0, addAdditionLabelText.length)];
    
    [self.addAdditionButton setAttributedTitle:attributed
                                      forState:UIControlStateNormal];
    
    GKLinearLayoutViewItem *item =
    [GKLinearLayoutViewItem layoutItemForView:self.addAdditionButton];
    item.padding = GKLinearLayoutMakePadding(5.0, 0.0, 0.0, 0.0);
    [self.additions addItem:item];
    
    RAC(self.viewModel.brief, content) = self.contentTextView.rac_textSignal;
    [self.priceTextField.rac_textSignal subscribeNext:^(NSString *price) {
        float iValue = 0.0;
        
        if (price.length > 0 &&
            [[NSScanner scannerWithString:price] scanFloat:&iValue]) {
            self.viewModel.brief.price = [[NSDecimalNumber alloc] initWithFloat:iValue];
        }
    }];
    self.postButton.rac_command = self.viewModel.postCommand;
//    [self.viewModel.allValid subscribeNext:^(NSNumber *valid) {
//        if (valid.boolValue) {
//            self.postButton.backgroundColor = [SHColor tintColor];
//            self.postButton.enabled = YES;
//        } else {
//            self.postButton.backgroundColor = [SHColor disabledColor];
//            self.postButton.enabled = NO;
//        }
//    }];
    
    self.tagsView.orientation = GKLinearLayoutViewOrientationHorizontal;
    
    self.tagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tagButton.titleLabel.textColor = GKColorFromRGB(0x515151);
    self.tagButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.tagButton setTitleColor:GKColorFromRGB(0x515151)
                         forState:UIControlStateNormal];
    [self.tagButton setTitle:@"点击选择标签" forState:UIControlStateNormal];
    [self.tagButton addTarget:self action:@selector(selectTagDidTap:)
             forControlEvents:UIControlEventTouchUpInside];
    
    GKLinearLayoutViewItem *tagItem =
    [GKLinearLayoutViewItem layoutItemForView:self.tagButton];
    [self.tagsView addItem:tagItem];
    
    [self.tagButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@86);
        make.height.equalTo(@21);
    }];
    
    @weakify(self)
    [[self.viewModel.postCommand.executionSignals
      flattenMap:^RACStream *(RACSignal *signals) {
          return [[signals materialize] filter:^BOOL(RACEvent *event) {
              return event.eventType == RACEventTypeNext;
          }];
      }] subscribeNext:^(RACEvent *event) {
          @strongify(self);
          
          
      }];
    
    self.viewModel.brief.content = @"123";
    self.viewModel.brief.price = [[NSDecimalNumber alloc] initWithString:@"1111.11"];
    self.viewModel.brief.deadline = [[NSDate alloc] init];
}


- (void)deadlineDatePickerValueChange:(UIDatePicker *)datePicker {
    self.viewModel.brief.deadline = datePicker.date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYY年MM月dd日";
    
    NSString *formatted = [formatter stringFromDate:datePicker.date];
    self.deadlineTextField.text = formatted;
}

- (void)addAdditionDidTap:(id)sender {
    
    UIImagePickerController *controller;
    controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.navigationBar.tintColor = [UIColor whiteColor];
    controller.navigationBar.barTintColor = [SHColor tintColor];
    controller.delegate = self;
    controller.mediaTypes = @[(NSString *)kUTTypeMovie,
                              (NSString *)kUTTypeImage];
    
    [self.navigationController presentViewController:controller animated:YES
                                          completion:^{
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {

    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSString *extension =
    [info[UIImagePickerControllerMediaType] pathExtension];
    SHUploadFile *file = [[SHUploadFile alloc] init];
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        CGImageRef imageRef = representation.fullResolutionImage;
        UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
        file.size = representation.size;
        self.totalBytesExpectedToWriten += file.size;
        
        if ([extension isEqualToString:@"movie"]) {
            file.previewData = UIImageJPEGRepresentation(image, 0.6);
            file.path = info[UIImagePickerControllerMediaURL];
        } else {
            
            UIImage *original = info[UIImagePickerControllerOriginalImage];
            float ratio = 640 / original.size.width;
            CGSize targetImageSize = CGSizeMake(640.0, original.size.height * ratio);
            UIGraphicsBeginImageContext(targetImageSize);
            [original drawInRect:CGRectMake(0.0, 0.0, targetImageSize.width,
                                            targetImageSize.height)];
            UIImage *scaled = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            file.previewData = UIImageJPEGRepresentation(scaled, 0.6);
            file.data = UIImageJPEGRepresentation(original, 0.6);
        }
        file.name = [representation filename];
        
        [self addAdditionWithFile:file];
        [self.viewModel.brief.uploadFiles addObject:file];
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)addAdditionWithFile:(SHUploadFile *)file {
    SHAdditionViewCell *cell =
    [[SHAdditionViewCell alloc] initWithFile:file];

    cell.removeAddition = ^(SHAdditionViewCell *cell) {
        [self.additions removeItem:cell.item];
    };

    GKLinearLayoutViewItem *item =
    [GKLinearLayoutViewItem layoutItemForView:cell];

    item.padding = GKLinearLayoutMakePadding(5.0, 0.0, 0.0, 0.0);
    cell.item = item;
    [self.additions insertItem:item atIndex:self.additions.items.count - 1];
}

- (void(^)(NSUInteger bytesWritten,
             long long totalBytesWritten,
             long long totalBytesExpectedToWrite))uploadProgressBlock {
    return
    ^(NSUInteger bytesWritten,
      long long totalBytesWritten,
      long long totalBytesExpectedToWrite) {
        
        self.totalBytesWritten += totalBytesWritten;
    };
}

- (void)preview:(UIImage *)image {
    
}

- (IBAction)selectTagDidTap:(id)sender {
    TagTableViewController *controller =
    [[TagTableViewController alloc]
     initWithParent:nil selection:^(Tag *tag, NSError *error) {
         if (nil == error) {
             [self addTag:tag];
         }
    }];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)addTag:(Tag *)tag {
    [self.viewModel.brief willChangeValueForKey:@"tags"];
    [self.viewModel.brief.tags addObject:tag];
    [self.viewModel.brief didChangeValueForKey:@"tags"];
    
    UIButton *tagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [tagButton setBackgroundColor:GKColorFromRGB(0x3dc7ae)];
    [tagButton setTitleColor:[UIColor whiteColor]
                    forState:UIControlStateNormal];
    [tagButton setTitle:tag.fullname forState:UIControlStateNormal];
    tagButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    
    GKLinearLayoutViewItem *item =
    [GKLinearLayoutViewItem layoutItemForView:tagButton];
    item.padding = GKLinearLayoutMakePadding(0.0, 0.0, 0.0, 5.0);
    
    NSUInteger numberOfTags = self.viewModel.brief.tags.count;
    if (0 < numberOfTags)
        [self.tagsView insertItem:item atIndex:numberOfTags - 1];
}

- (IBAction)selectDateTimeDidTap:(id)sender {
//    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
//    [self.view addSubview:datePicker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
