//
//  BriefViewModel.m
//  ShanHuo
//
//  Created by 小悟空 on 7/9/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "BriefViewModel.h"

@implementation BriefViewModel

- (id)init {
    self = [super init];
    if (self) {
        self.brief = [[WKBrief alloc] init];
        self.backend = [[BriefBackend alloc] init];
        self.fileBackend = [[SHFileBackend alloc] init];
    }
    return self;
}

- (RACSignal *)contentValid {
    if (_contentValid != nil) {
        return _contentValid;
    }
    
    _contentValid =
    [RACObserve(self.brief, content) map:^id(NSString *content) {
        NSNumber *valid = content.length == 0 ? @0 : @1;
        if (!valid.boolValue)
            NSLog(@"内容校验不通过");
        return valid;
    }];
    
    return _contentValid;
}

- (RACSignal *)deadlineValid {
    if (_deadlineValid != nil)
        return _deadlineValid;
    
    _deadlineValid =
    [RACObserve(self.brief, deadline) map:^id(NSDate *deadline) {
        NSNumber *valid = deadline == nil ? @0 : @1;
        if (!valid.boolValue)
            NSLog(@"截止日期校验不通过");
        return valid;
    }];
    
    return _deadlineValid;
}

- (RACSignal *)tagValid {
    if (_tagValid != nil)
        return _tagValid;
    
    _tagValid =
    [RACObserve(self.brief, tags) map:^id(NSMutableArray *tags) {
        NSNumber *valid = tags.count == 0 ? @0 : @1;
        if (!valid.boolValue)
            NSLog(@"标签校验不通过");
        return valid;
    }];
    
    return _tagValid;
}

- (RACSignal *)priceValid {
    if (_priceValid != nil)
        return _priceValid;
    
    _priceValid =
    [RACObserve(self.brief, price) map:^id(NSDecimalNumber *price) {
        NSNumber *valid = nil == price ? @0 : @1;
        if (!valid.boolValue)
            NSLog(@"价格校验不通过");
        return valid;
    }];
    
    return _priceValid;
}

- (RACSignal *)allValid {
    if (_allValid != nil)
        return _allValid;
    
    _allValid =
    [RACSignal combineLatest:@[self.contentValid, self.deadlineValid,
                               self.tagValid, self.priceValid]
                      reduce:^id(NSNumber *contentValid,
                                 NSNumber *deadlineValid, NSNumber *tagValid,
                                 NSNumber *priceValid)
     {
         return contentValid.boolValue && deadlineValid.boolValue &&
         tagValid.boolValue && priceValid.boolValue ? @1 : @0;
     }];
    
    return _allValid;
}

- (RACCommand *)postCommand {
    if (_postCommand != nil) {
        return _postCommand;
    }

    _postCommand =
//    [[RACCommand alloc] initWithEnabled:self.allValid
// signalBlock:^RACSignal *(id input)
    [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input)
     
     {
         NSLog(@"Brief post button executed");
         return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
             NSLog(@"Post Command signal");
             [self.backend requestTouchBriefWithUser:[[App shared] currentUser]
                                          completion:^(WKBrief *brief, NSError *error)
             {
                 
                 NSLog(@"Brief touch request");
                 
                 if (nil != error) {
                     [subscriber sendError:error];
                     return;
                 }
                 self.brief.briefID = brief.briefID;
                 __block NSInteger loaded = 0;
                 if (self.brief.uploadFiles.count > 0) {
                     for (SHUploadFile *file in self.brief.uploadFiles) {
                         file.resourceID = brief.briefID;
                         [self.fileBackend requestUploadFile:file user:[[App shared] currentUser] uploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                             
                         } completion:^(NSArray *files, NSError *error) {
                             NSLog(@"upload finish");
                             loaded += 1;
                             
                             if (loaded == self.brief.uploadFiles.count) {
                                 [self update:subscriber];
                             }
                         }];
                     }
                 } else {
                     [self update:subscriber];
                 }
                 
                 
            }];
             return nil;
         }];
    }];
    return _postCommand;
}

- (void)update:(id<RACSubscriber>) subscriber {
    [self.backend requestUpdateBrief:self.brief
                          completion:^(WKBrief *brief, NSError *error)
     {
         if (nil == error) {
             [subscriber sendNext:brief];
             [subscriber sendCompleted];
         } else {
             [subscriber sendError:error];
         }
     }];
}
@end
