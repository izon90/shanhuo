//
//  BriefBackend.h
//  ShanHuo
//
//  Created by 小悟空 on 7/10/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "GKBackend.h"
#import "WKBrief.h"
#import "WKFile.h"

@interface BriefBackend : GKBackend

- (void)requestTouchBriefWithUser:(GKUser *)user
                       completion:(void(^)(WKBrief *brief, NSError *error))completion;
- (void)requestUpdateBrief:(WKBrief *)brief
completion:(void(^)(WKBrief *brief, NSError *error))completion;
@end
