//
//  SHConversationCell.h
//  ShanHuo
//
//  Created by heyong on 15/8/5.
//  Copyright (c) 2015年 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RongIMKit.h>

@class SHConversationCell;

typedef void(^SHConversationCellFavoriteBlock)(SHConversationCell *cell, BOOL favorite);

@interface SHConversationCell : RCConversationBaseCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (assign, nonatomic) BOOL isFavorite;

@property (copy, nonatomic) SHConversationCellFavoriteBlock favoriteBlock;

@end
