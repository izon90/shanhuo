//
//  MyPostViewController.h
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHUser.h"

@interface BriefViewController : UIViewController

@property (strong, nonatomic) SHUser *user;

@end
