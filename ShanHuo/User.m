//
//  User.m
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "User.h"

NSString * const kAvatarImageURL = @"avatarImageURL";
NSString * const kUserName = @"username";
NSString * const kUserLocation = @"location";
NSString * const kRating = @"rating";

@implementation User

- (instancetype)initWithUserInfo:(NSDictionary *)info {
    if (self == [super init]) {
        if (NotNull(info)) {
            _avatarImageURL = [NSURL URLWithString:info[kAvatarImageURL]];
            _userName = info[kUserName];
            _userLocation = info[kUserLocation];
            _rating = @([info[kRating] intValue]);
        }
    }
    return self;
}

@end
