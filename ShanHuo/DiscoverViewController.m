//
//  DiscoverViewController.m
//  FreelanceProject
//
//  Created by heyong on 7/22/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "DiscoverViewController.h"
#import "DiscoverCell.h"
#import "PushView.h"
#import "PersonViewController.h"
#import <RongIMKit/RongIMKit.h>
#import "ChatListViewController.h"
#import "SHNetworkEngine.h"
#import "SHBrief.h"
#import "SHTag.h"
#import "BriefCreateViewController.h"
#import <MJRefresh/MJRefresh.h>

static NSString * const DiscoverCellIdentifier = @"DiscoverCell";

@interface DiscoverViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *datasource;
@property (strong, nonatomic) PushView *pushView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *previousSelectedIndexPath;

@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(perPersonViewControllerDidRemoveFromSuperView:) name:@"PerPersonViewControllerDidRemoveFromSuperView" object:nil];
    
    self.title = @"快配";
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];

    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topLayoutGuide attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    [self.view addConstraints:@[top, left, right, bottom]];
    
    UINib *discoverCellNib = [UINib nibWithNibName:@"DiscoverCell" bundle:nil];
    [self.tableView registerNib:discoverCellNib forCellReuseIdentifier:DiscoverCellIdentifier];
    
    [self.view insertSubview:self.pushView aboveSubview:self.tableView];
    self.pushView.hidden = YES;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
    }];
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.hidden = YES;
    self.tableView.header = header;
    
    [self.tableView.header beginRefreshing];
}

- (void)loadData {
    __weak typeof(self) wself = self;
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] getUserBriefsWithUserID:@([App shared].currentUser.userID) page:@(1) block:^(NSArray *objects, NSError *error) {
        if (objects) {
            wself.datasource = objects;
            dispatch_async(dispatch_get_main_queue(), ^{
                [wself.tableView reloadData];
                [MBProgressHUD hideHUDForView:wself.tableView animated:YES];
                [self.tableView.header endRefreshing];
            });
        }
    }];
}

- (void)perPersonViewControllerDidRemoveFromSuperView:(NSNotification *)notif {
    [self.tableView removeGestureRecognizer:(UITapGestureRecognizer *)notif.object];
}

- (void)tableViewTapped:(UITapGestureRecognizer *)sender {
    if ([self.childViewControllers count] > 0) {
        for (UIViewController *controller in self.childViewControllers) {
            if ([NSStringFromClass([PersonViewController class]) isEqualToString:@"PersonViewController"]) {
                [controller willMoveToParentViewController:nil];
                [controller.view removeFromSuperview];
                [controller removeFromParentViewController];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PerPersonViewControllerDidRemoveFromSuperView" object:sender];
            }
        }
    }
}

- (PushView *)pushView {
    if (!_pushView) {
        _pushView = [[[NSBundle mainBundle] loadNibNamed:@"PushView" owner:self options:nil] lastObject];
        _pushView.frame = CGRectMake(40, 270, self.view.bounds.size.width-80, 200);
        _pushView.layer.borderColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f].CGColor;
        _pushView.layer.borderWidth = 0.5;
        [_pushView.viewButton addTarget:self action:@selector(viewPerson:) forControlEvents:UIControlEventTouchUpInside];
        [_pushView.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pushView;
}

- (void)viewPerson:(UIButton *)sender {
    SHBrief *brief = self.datasource[self.previousSelectedIndexPath.row];
    PersonViewController *personViewController = [[PersonViewController alloc] initWithNibName:@"PersonViewController" bundle:nil];
    personViewController.brief = brief;
    [self addChildViewController:personViewController];
    personViewController.view.frame = CGRectMake(40, 150, self.view.bounds.size.width-80, 344);
    personViewController.view.layer.borderColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f].CGColor;
    personViewController.view.layer.borderWidth = 0.5;
    [self.view addSubview:personViewController.view];
    [personViewController didMoveToParentViewController:self];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableViewTapped:)];
    [self.tableView addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.datasource count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DiscoverCell *cell = (DiscoverCell *)[tableView dequeueReusableCellWithIdentifier:@"DiscoverCell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == [self.datasource count]) {
        cell.requestLabel.text = @"发布新需求";
        cell.dateLabel.text = @"";
    } else {
        SHBrief *brief = self.datasource[indexPath.row];
        cell.requestLabel.text = brief.content;
        NSArray *tags = brief.tags;
        
        CGRect previousLabelFrame = CGRectZero;
        BOOL previousLabelExist = NO;
        if ([tags count] > 0) {
            for (int i = 0; i < [tags count]; i++) {
                SHTag *tagObject = tags[i];
                NSString *tag = tagObject.fullname;
                NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag];
                [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, tag.length)];
                CGRect rect = [attribtedTag boundingRectWithSize:CGSizeMake(cell.tagView.frame.size.width, 20) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                
                UILabel *label = [[UILabel alloc] init];
                
                if (previousLabelExist) {
                    label.frame = CGRectMake(previousLabelFrame.origin.x + previousLabelFrame.size.width + 5, 0, rect.size.width+10, 20);
                } else {
                    label.frame = CGRectMake(0, 0, rect.size.width+10, 20);
                }
                
                label.text = tag;
                label.font = [UIFont systemFontOfSize:14];
                label.textColor = [UIColor whiteColor];
                label.backgroundColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
                label.textAlignment = NSTextAlignmentCenter;
                
                previousLabelFrame = label.frame;
                previousLabelExist = YES;
                
                [cell.tagView addSubview:label];
            }
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20+44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 20+44, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    UIButton *chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseButton.frame = CGRectMake((self.tableView.frame.size.width-280)/2, 30, 280, 44);
    [chooseButton setTitle:@"开始选取" forState:UIControlStateNormal];
    [chooseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [chooseButton addTarget:self action:@selector(chooseAction:) forControlEvents:UIControlEventTouchUpInside];
    [chooseButton setBackgroundColor:[UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f]];
    chooseButton.clipsToBounds = YES;
    chooseButton.layer.cornerRadius = 2;
    [view addSubview:chooseButton];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.previousSelectedIndexPath) {
        DiscoverCell *cell = (DiscoverCell *)[tableView cellForRowAtIndexPath:self.previousSelectedIndexPath];
        [cell.checkButton setImage:[UIImage imageNamed:@"unselected"] forState:UIControlStateNormal];
    }
    DiscoverCell *selectedCell = (DiscoverCell *)[tableView cellForRowAtIndexPath:indexPath];
    [selectedCell.checkButton setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
    
    self.previousSelectedIndexPath = indexPath;
}

- (void)chooseAction:(UIButton *)sender {
    if (self.previousSelectedIndexPath && self.previousSelectedIndexPath.row < [self.datasource count]) {
        [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
        
        SHBrief *brief = self.datasource[self.previousSelectedIndexPath.row];
        [[SHNetworkEngine sharedEngine] getUserMatchCountWithBriefID:brief.briefID block:^(NSNumber *totalCount, NSNumber *matchCount, NSError *error) {
            NSMutableAttributedString *messageAtributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"系统正在为你的需求推送信息\n本次共推送给 %@ 人\n已匹配到合适的 %@ 人", totalCount, matchCount]];
            [messageAtributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f]} range:NSMakeRange(20, 4)];
            [messageAtributedString addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f]} range:NSMakeRange(33, 2)];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.pushView.messageLabel.attributedText = messageAtributedString;
                self.pushView.hidden = NO;
                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
            });
        }];
    } else if (self.previousSelectedIndexPath.row == [self.datasource count]) {
        BriefCreateViewController *briefCreate = [self.storyboard instantiateViewControllerWithIdentifier:@"BriefCreateViewController"];
        [self.navigationController pushViewController:briefCreate animated:YES];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"请选一个需求" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"好" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:ok];
    }
}

- (void)cancelAction:(UIButton *)sender {
    self.pushView.hidden = YES;
}

@end
