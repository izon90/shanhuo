//
//  SHNetworkEngine.m
//  ShanHuo
//
//  Created by heyong on 7/25/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "SHNetworkEngine.h"
#import <AFNetworking/AFNetworking.h>
#import "SHUser.h"
#import <Mantle/Mantle.h>
#import "SHBrief.h"
#import "MediaMovie.h"
#import "MediaImage.h"
#import "SHBriefRequest.h"
#import <DDLog.h>
#import "SHContactRequest.h"


@interface SHNetworkEngine()

@property (strong, nonatomic) AFHTTPSessionManager *manager;

@end

@implementation SHNetworkEngine

+ (instancetype)sharedEngine {
    static SHNetworkEngine *_engine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [[SHNetworkEngine alloc] init];
    });
    return _engine;
}

- (instancetype)init {
    if (self = [super init]) {
        NSURL *baseURL = [NSURL URLWithString:GKConfig.config.host];
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", [App shared].currentUser.accessToken.accessToken] forHTTPHeaderField:@"Authorization"];
    }
    return self;
}

- (void)getAllUsersWithPage:(NSNumber *)page block:(UsersBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/users?page=%@", page];
    NSLog(@"%@", path);
    
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *users = [NSMutableArray arrayWithCapacity:[responseObject count]];
            for (NSDictionary *object in responseObject) {
                SHUser *user = [MTLJSONAdapter modelOfClass:[SHUser class] fromJSONDictionary:object error:nil];
                [users addObject:user];
            }
            block(users, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"%@", error);
        block(nil, error);
    }];
}

- (void)getAllBriefsWithPage:(NSNumber *)page block:(BriefsBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/brief?page=%@", page];
    NSLog(@"%@", path);
    
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@", responseObject);
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *briefs = [NSMutableArray arrayWithCapacity:[responseObject count]];
            for (NSDictionary *object in responseObject) {
                SHBrief *brief = [MTLJSONAdapter modelOfClass:[SHBrief class] fromJSONDictionary:object error:nil];
                [briefs addObject:brief];
            }
            block(briefs, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"%@", error);
        block(nil, error);
    }];
}

- (void)getProjectsWithPage:(NSNumber *)page block:(ProjectsBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/project?page=%@", page];
    NSLog(@"%@", path);
    
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *projects = [NSMutableArray arrayWithCapacity:[responseObject count]];
            for (NSDictionary *object in responseObject) {
                SHProject *project = [MTLJSONAdapter modelOfClass:[SHProject class] fromJSONDictionary:object error:nil];
                [projects addObject:project];
            }
            block(projects, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"%@", error);
        block(nil, error);
    }];
}

- (void)getUserBriefsWithUserID:(NSNumber *)userID page:(NSNumber *)page block:(BriefsBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/user/brief/%@", userID];
    NSLog(@"%@", path);
    
    [self.manager GET:path parameters:@{@"page": page} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *briefs = [NSMutableArray arrayWithCapacity:[responseObject count]];
            for (NSDictionary *object in responseObject) {
                SHBrief *brief = [MTLJSONAdapter modelOfClass:[SHBrief class] fromJSONDictionary:object error:nil];
                [briefs addObject:brief];
            }
            block(briefs, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)getUserInfoWithUserID:(NSNumber *)userID block:(UserBlock)block {
    NSString *path = [NSString stringWithFormat:@"/api/user/%@", userID];
    NSLog(@"%@", path);
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHUser *user = [MTLJSONAdapter modelOfClass:[SHUser class] fromJSONDictionary:responseObject error:nil];
            block(user, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)getUserProjectsWithUserID:(NSNumber *)userID block:(ProjectsBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/user/project/%@", userID];
    NSLog(@"%@", path);
    
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *projects = [NSMutableArray arrayWithCapacity:[responseObject count]];
            for (NSDictionary *object in responseObject) {
                SHProject *project = [MTLJSONAdapter modelOfClass:[SHProject class] fromJSONDictionary:object error:nil];
                [projects addObject:project];
            }
            block(projects, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)updateUserInfoWithName:(NSString *)name age:(NSNumber *)age gender:(NSNumber *)gender provinceID:(NSNumber *)provinceID cityID:(NSNumber *)cityID districtID:(NSNumber *)districtID block:(UserBlock)block {
    NSDictionary *params = @{@"name": name,
                             @"age": age,
                             @"gender": gender,
                             @"province_id": provinceID ? provinceID : @"",
                             @"city_id": cityID ? cityID : @"",
                             @"district_id": districtID ? districtID : @""
                             };
    [self.manager POST:@"api/user/profile" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHUser *user = [MTLJSONAdapter modelOfClass:[SHUser class] fromJSONDictionary:(NSDictionary *)responseObject error:nil];
            block(user, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)getuserMathWithBriefID:(NSNumber *)briefID renew:(NSNumber *)renew block:(UsersBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/user/match/%@", briefID];
    NSLog(@"%@", path);
    [self.manager GET:path parameters:@{@"renew": renew} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *users = [NSMutableArray arrayWithCapacity:[responseObject count]];
            for (NSDictionary *object in responseObject) {
                SHUser *user = [MTLJSONAdapter modelOfClass:[SHUser class] fromJSONDictionary:object error:nil];
                [users addObject:user];
            }
            block(users, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)getUserMatchCountWithBriefID:(NSNumber *)briefID block:(UserMatchCountBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/user/match/count/%@", briefID];
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber *count = @([responseObject[@"total"] intValue]);
            NSNumber *matchCount = @([responseObject[@"match_count"] intValue]);
            block(count, matchCount, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)uploadUserAvatarWithImage:(UIImage *)image block:(SuccessBlock)block {
    [self.manager POST:@"api/user/avatar" parameters:@{@"name": @"avatar.jpg"} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSData *data = UIImageJPEGRepresentation(image, 0.5);
        [formData appendPartWithFileData:data name:@"avatar" fileName:@"avatar.jpg" mimeType:@"image/jpg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        block(YES, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(NO, error);
        DDLogError(@"%@", error);
    }];
}

- (void)createNewBriefWithContent:(NSString *)content price:(NSNumber *)price startAt:(NSDate *)startat block:(BriefBlock)block {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd HH-mm-ss";
    
    NSDictionary *params = @{@"content": content, @"price": price, @"start_at": [dateFormatter stringFromDate:startat]};
    [self.manager PUT:@"api/brief" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHBrief *brief = [MTLJSONAdapter modelOfClass:[SHBrief class] fromJSONDictionary:responseObject error:nil];
            block(brief, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)touchNewBriefWithBlock:(BriefBlock)block {
    [self.manager POST:@"api/brief/touch" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHBrief *brief = [MTLJSONAdapter modelOfClass:[SHBrief class] fromJSONDictionary:responseObject error:nil];
            block(brief, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)uploadBriefFileWithFiles:(NSArray *)files resourceID:(NSNumber *)resourceID resourceType:(NSString *)type preview:(UIImage *)previewImage block:(ProjectFileBlock)block {
    [self.manager POST:@"api/project/file" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (int i = 0; i < [files count]; i++) {
            NSURL *fileURL = files[i];
            [formData appendPartWithFileURL:fileURL name:@"file" error:nil];
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"%@", error);
    }];
}

- (void)rateUserID:(NSNumber *)userID rank:(NSNumber *)rank content:(NSString *)content block:(UserBlock)block {
    NSDictionary *params = @{
                             @"user_id": userID,
                             @"rank": rank,
                             @"content": content
                             };
    [self.manager POST:@"/api/user/rank" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHUser *user = [MTLJSONAdapter modelOfClass:[SHUser class] fromJSONDictionary:responseObject error:nil];
            block(user, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)uploadProjectFileWithFiles:(NSArray *)files projectID:(NSNumber *)projectID block:(FilesBlock)block {
    [self.manager POST:@"api/project/file" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id object in files) {
            if ([object isKindOfClass:[MediaImage class]]) {
                MediaImage *mediaImage = (MediaImage *)object;
                [formData appendPartWithFileData:mediaImage.data name:@"files[]" fileName:mediaImage.filename ? mediaImage.filename : @"image" mimeType:@"image/jpeg"];
                [formData appendPartWithFileData:mediaImage.data name:@"previews[]" fileName:mediaImage.filename ? mediaImage.filename : @"image" mimeType:@"image/jpeg"];
            } else if ([object isKindOfClass:[MediaMovie class]]) {
                MediaMovie *mediaMovie = (MediaMovie *)object;
                NSError *error = nil;
                [formData appendPartWithFileURL:mediaMovie.mediaURL name:@"files[]" fileName:mediaMovie.filename ? mediaMovie.filename : @"movie" mimeType:@"video/quicktime" error:&error];
                [formData appendPartWithFileData:mediaMovie.thumbnailData name:@"previews[]" fileName:mediaMovie.filename ? mediaMovie.filename : @"movie" mimeType:@"image/jpeg"];
                if (error) {
                    NSLog(@"append movie error: %@", error);
                }
            }
        }
        NSString *projectIDString = [NSString stringWithFormat:@"%@", projectID];
        [formData appendPartWithFormData:[projectIDString dataUsingEncoding:NSUTF8StringEncoding] name:@"project_id"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            NSMutableArray *files = [NSMutableArray new];
            for (NSDictionary *dict in responseObject) {
                SHFile *file = [MTLJSONAdapter modelOfClass:[SHFile class] fromJSONDictionary:dict error:nil];
                [files addObject:file];
            }
            block(files, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)createProjectTouchWithTitle:(NSString *)title content:(NSString *)content block:(ProjectBlock)block {
    NSDictionary *params = @{
                             @"title": title,
                             @"content": content
                             };
    [self.manager POST:@"api/project/touch" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHProject *project = [MTLJSONAdapter modelOfClass:[SHProject class] fromJSONDictionary:responseObject error:nil];
            block(project, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)updateProjectWithProjectID:(NSNumber *)projectID title:(NSString *)title content:(NSString *)content block:(ProjectBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/project/%@", projectID];
    NSDictionary *params = @{
                             @"title": title,
                             @"content": content
                             };
    [self.manager PUT:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"project update success");
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHProject *project = [MTLJSONAdapter modelOfClass:[SHProject class] fromJSONDictionary:responseObject error:nil];
            block(project, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)requestToJoinBrief:(NSNumber *)briefID status:(NSNumber *)status block:(RequestBlock)block {
    [self.manager POST:@"api/brief/supplier" parameters:@{@"brief_id": briefID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHBriefRequest *request = [MTLJSONAdapter modelOfClass:[SHBriefRequest class] fromJSONDictionary:responseObject error:nil];
            block(request, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)approveOrDenyRequestToJoinBrief:(NSNumber *)briefID status:(NSNumber *)status completion:(RequestBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/brief/supplier/%@", briefID];
    [self.manager PUT:path parameters:@{@"status": status} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHBriefRequest *request = [MTLJSONAdapter modelOfClass:[SHBriefRequest class] fromJSONDictionary:responseObject error:nil];
            block(request, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)userContactRequestWithSenderUserId:(NSNumber *)senderUserId targetUserId:(NSNumber *)targetUserId status:(NSNumber *)status completion:(ContactUserBlock)block {
    NSDictionary *params;
    if ([status intValue] == 1 || [status intValue] == 2) {
        params = @{@"request": senderUserId, @"status": status};
    } else if ([status intValue] == 0) {
        params = @{@"answer": targetUserId, @"status": status};
    }
    
    DDLogVerbose(@"userContactRequest params: %@", params);
    [self.manager POST:@"api/user/contact/request" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)[task response];
        if (response.statusCode == 200) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                SHContactRequest *request = [MTLJSONAdapter modelOfClass:[SHContactRequest class] fromJSONDictionary:responseObject error:nil];
                block(request, nil);
            } else if ([responseObject isKindOfClass:[NSArray class]]) {
                
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)[task response];
        if (response.statusCode == 403) {
            block(nil, [NSError errorWithDomain:@"com.shanhuo" code:403 userInfo:@{@"error": @"联系他人的可用额度用尽"}]);
        } else {
            block(nil, error);
        }
        DDLogError(@"%@", error);
    }];
}

- (void)userContactRequestQueryWithUserId:(NSNumber *)userId block:(ContactUserBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/user/contact/request/%@", userId];
    [self.manager GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHContactRequest *request = [MTLJSONAdapter modelOfClass:[SHContactRequest class] fromJSONDictionary:responseObject error:nil];
            block(request, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
        DDLogError(@"%@", error);
    }];
}

- (void)getJoinBriefStatusWithOwerID:(NSNumber *)ownerID userID:(NSNumber *)userID briefID:(NSNumber *)briefID block:(RequestBlock)block {
    NSString *path = [NSString stringWithFormat:@"api/brief/supplier/%@", briefID];
    [self.manager GET:path parameters:@{@"owner": ownerID, @"user": userID} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            SHBriefRequest *request = [MTLJSONAdapter modelOfClass:[SHBriefRequest class] fromJSONDictionary:responseObject error:nil];
            block(request, nil);
        } else if ([responseObject isKindOfClass:[NSArray class]]) {
            if ([(NSArray *)responseObject count] > 0) {
                NSDictionary *dict = ((NSArray *)responseObject)[0];
                SHBriefRequest *request = [MTLJSONAdapter modelOfClass:[SHBriefRequest class] fromJSONDictionary:dict error:nil];
                block(request, nil);
            } else {
                block(nil, [NSError errorWithDomain:@"com.shanhuo" code:1000 userInfo:nil]);
            }
        } else {
            block(nil, [NSError errorWithDomain:@"com.shanhuo" code:1000 userInfo:nil]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil, error);
    }];
    
}

- (void)attachTag:(Tag *)tag toUser:(GKUser *)user completion:(void(^)(Tag *tag, NSError *error))completion {
    NSLog(@"Attach tag to user");// %d", (int)user.userID);
    NSString *tag_id = [NSString stringWithFormat:@"%d", (int)tag.tagID];
    [self.manager POST:@"api/user/tag" parameters:@{@"tag": tag_id} success:^(NSURLSessionDataTask *task, id responseObject) {
        completion(tag, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error.localizedDescription);
        completion(tag, error);
    }];
}

@end
