//
//  ProfileViewController.m
//  FreelanceProject
//
//  Created by heyong on 7/21/15.
//  Copyright (c) 2015 heyong. All rights reserved.
//

#import "ProfileViewController.h"
#import "User.h"
#import "UserCell.h"
#import "TagCell.h"
#import "NormalCell.h"
#import <RongIMKit/RongIMKit.h>
#import "BriefViewController.h"
#import "SHTag.h"
#import "SHNetworkEngine.h"
#import "SHConversationViewController.h"
#import "UIImageView+WebCache.h"
#import "ProjectViewController.h"

@interface ProfileViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;

@property (strong, nonatomic) UIView *tagsView;
@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) CGFloat tagsViewHeight;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tabBarController.tabBar setHidden:YES];
    
    self.edgesForExtendedLayout = UIRectEdgeBottom;
    
    self.title = self.user.name ? self.user.name : self.user.username;
    
    // 去掉group table view top space
    self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
    
    [self createTableViewDatasource];
    [self.tableView reloadData];
}

// 创建table view测试数据
- (void)createTableViewDatasource {
    NSDictionary *userInfo = @{kAvatarImageURL: @"avatar.png", kUserName: @"诺提勒斯", kUserLocation: @"上海", kRating: @"0"};
    User *user = [[User alloc] initWithUserInfo:userInfo];
    NSMutableArray *mutableDatasource = [NSMutableArray arrayWithObject:user];

    NSArray *tags = @[@"IT/互联网", @"广告/媒体", @"网页设计", @"H5页面", @"APP界面", @"游戏界面", @"商业插画", @"漫画设计"];
    [mutableDatasource addObject:tags];
    
    NSArray *tasks = @[@"他发布的需求", @"他的案例", @"谁看过他"];
    [mutableDatasource addObjectsFromArray:tasks];
    
    self.dataSource = [NSArray arrayWithArray:mutableDatasource];
}


// 添加tag label
- (void)addTagsView:(UIView *)tagsView {
    // 整个tag view的padding
    UIEdgeInsets tagsViewPadding = UIEdgeInsetsMake(50, 20, 10, 20);
    
    // 记录上一个添加到cell contentView的label，用于计算下一个label的frame
    CGRect previousTagLabelFrame = CGRectZero;
    
    // 是否是第一个添加的tag label
    BOOL previousTagLabelExist = NO;
    
    // 获取测试tags
//    NSArray *tags = self.dataSource[1];
    NSArray *tags = self.user.tags;
    
    for (int i = 0; i < [tags count]; i++) {
        SHTag *tagObject = tags[i];
        NSString *tag = tagObject.fullname;
        
        // 计算除tag文字的宽度，高度固定为20
        NSMutableAttributedString *attribtedTag = [[NSMutableAttributedString alloc] initWithString:tag];
        [attribtedTag addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, tag.length)];
        CGRect rect = [attribtedTag boundingRectWithSize:CGSizeMake(tagsView.frame.size.width - 10, 20) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        CGFloat labelWidth = rect.size.width+10;
        
        // 初始化tag label
        UILabel *label = [[UILabel alloc] init];
        
        // 如果前一个的label已经加入contentView
        if (previousTagLabelExist) {
            // 获取已经添加的label的总宽度
            CGFloat existedWidth = previousTagLabelFrame.origin.x + previousTagLabelFrame.size.width + labelWidth + 5;
            // 如果总宽度大于cell contentView的宽度，那么就把下一个label放在下一行
            if (existedWidth > tagsView.frame.size.width) {
                // 算出下一行label的frame
                label.frame = CGRectMake(tagsViewPadding.left, previousTagLabelFrame.origin.y + previousTagLabelFrame.size.height + 5, labelWidth, 20);
            } else {
                // 如果没有超出contentView的剩余宽度空间，那么就继续在当前行添加label
                label.frame = CGRectMake(tagsViewPadding.left + previousTagLabelFrame.origin.x + previousTagLabelFrame.size.width + 5, previousTagLabelFrame.origin.y, labelWidth, 20);
            }
        } else {
            // 第一个label的宽度
            label.frame = CGRectMake(tagsViewPadding.left, tagsViewPadding.top, labelWidth, 20);
        }
        label.font = [UIFont systemFontOfSize:14];
        label.text = tag;
        // 居中显示
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor colorWithRed:0.204f green:0.741f blue:0.624f alpha:1.0f];
        label.textColor = [UIColor whiteColor];
        [tagsView addSubview:label];
        
        // 记录下最近的label的frame，用于计算下一个label的frame
        previousTagLabelFrame = label.frame;
        previousTagLabelExist = YES;
    }
    // 计算出tags添加完以后，总高度，用于heightForRowAtIndexPath
    self.tagsViewHeight = previousTagLabelFrame.size.height + previousTagLabelFrame.origin.y;
    // table view cell高度改变了，reload当前section
    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        // 用户信息cell
        UserCell *userCell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
        userCell.user = self.user;
        [userCell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@images/avatar/%@.jpg", GKConfig.config.host, self.user.userID]] placeholderImage:[UIImage imageNamed:@"defaultAvatar"]];
        return userCell;
    } else if (indexPath.section == 1) {
        // Tag Cell
        TagCell *tagCell = [tableView dequeueReusableCellWithIdentifier:@"TagCell" forIndexPath:indexPath];
        NSArray *tags = self.dataSource[indexPath.section];
        [tagCell setTags:tags];
        [self addTagsView:tagCell.contentView];
        return tagCell;
    } else {
        // 普通Cell
        NormalCell *normalCell = [tableView dequeueReusableCellWithIdentifier:@"NormalCell" forIndexPath:indexPath];
        NSString *task = self.dataSource[indexPath.section];
        [normalCell setTask:task];
        return normalCell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section > 1) {
        NSString *task = self.dataSource[indexPath.section];
        if ([task isEqualToString:@"他的案例"]) {
            [self performSegueWithIdentifier:@"showCase" sender:self];
        } else if ([task isEqualToString:@"他发布的需求"]) {
            [self performSegueWithIdentifier:@"showMyPost" sender:self];
        } else if ([task isEqualToString:@"退出登录"]) {
            
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 50;
    if (indexPath.section == 0) {
        height = 100;
    } else if (indexPath.section == 1) {
        CGFloat tagsViewHeight = self.tagsViewHeight;
        return MAX(tagsViewHeight+10, 64);
    }
    return height;
}

#pragma mark - Actions

- (IBAction)contactAction:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [[SHNetworkEngine sharedEngine] userContactRequestQueryWithUserId:self.user.userID block:^(SHContactRequest *request, NSError *error) {
        if (request) {
            NSNumber *status = request.status;
            if (status) {
                if (status.intValue == 1) {
                    RCConversationViewController *conversationVC = [[RCConversationViewController alloc]init];
                    conversationVC.conversationType = ConversationType_PRIVATE;
                    conversationVC.targetId = [NSString stringWithFormat:@"%@", self.user.userID];
                    conversationVC.userName = self.user.name;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                        [self.navigationController pushViewController:conversationVC animated:YES];
                    });
                } else if (status.intValue == 0) {
                    [self alertWithTitle:@"是否发送请求" cancelButton:YES okBlock:^(id sender) {
                        [self sendContactRequest];
                    }];
                } else if (status.intValue == 2) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                        [self alertWithTitle:@"对方拒绝" cancelButton:NO okBlock:^(UIAlertController *sender) {
                            [sender dismissViewControllerAnimated:YES completion:nil];
                        }];
                    });
                }
            } else {
                [self alertWithTitle:@"是否发送请求" cancelButton:YES okBlock:^(id sender) {
                    [self sendContactRequest];
                }];
            }
        }
    }];
}

- (void)sendContactRequest {
    NSInteger currentUserID = [App shared].currentUser.userID;
    [[SHNetworkEngine sharedEngine] userContactRequestWithSenderUserId:@(currentUserID) targetUserId:self.user.userID status:@(0) completion:^(SHContactRequest *request, NSError *error) {
        if (request) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                [self alertWithTitle:@"请求已经发出" cancelButton:NO okBlock:^(UIAlertController *sender) {
                    [sender dismissViewControllerAnimated:YES completion:nil];
                }];
            });
        } else {
            NSString *errorMessage;
            if (error.code == 403) {
                errorMessage = error.userInfo[@"error"];
            } else {
                errorMessage = @"网络错误";
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
                [self alertWithTitle:errorMessage cancelButton:NO okBlock:^(UIAlertController *sender) {
                    [sender dismissViewControllerAnimated:YES completion:nil];
                }];
            });
        }
    }];
}

- (void)alertWithTitle:(NSString *)title cancelButton:(BOOL)cancelButton okBlock:(void(^)(id sender))okBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    if (cancelButton) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
            if ([MBProgressHUD HUDForView:self.tableView]) {
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
            }
        }];
        [alert addAction:cancelAction];
    }
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        okBlock(alert);
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)favoriteAction:(id)sender {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[BriefViewController class]]) {
        BriefViewController *myPostViewController = (BriefViewController *)segue.destinationViewController;
        myPostViewController.user = self.user;
    } else if ([segue.destinationViewController isKindOfClass:[ProjectViewController class]]) {
        ProjectViewController *projectViewController = (ProjectViewController *)segue.destinationViewController;
        projectViewController.user = self.user;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
