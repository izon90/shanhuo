//
//  GKLinearLayoutView.h
//  ShanHuo
//
//  Created by 小悟空 on 7/5/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKLinearLayoutViewItem.h"

@interface GKLinearLayoutView : UIView

@property (strong, nonatomic) NSMutableArray *items;
@property (assign, nonatomic) GKLinearLayoutViewOrientation orientation;

- (void)addItem:(GKLinearLayoutViewItem *)item;
- (void)insertItem:(GKLinearLayoutViewItem *)item atIndex:(NSUInteger)index;
- (void)removeItem:(GKLinearLayoutViewItem *)item;
@end

GKLinearLayoutItemPadding GKLinearLayoutMakePadding(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right);
