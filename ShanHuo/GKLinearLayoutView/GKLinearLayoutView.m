//
//  GKLinearLayoutView.m
//  ShanHuo
//
//  Created by 小悟空 on 7/5/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "GKLinearLayoutView.h"
#import <Masonry/Masonry.h>

@interface GKLinearLayoutView()

@end

@implementation GKLinearLayoutView

- (id)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.items = [[NSMutableArray alloc] init];
    assert(self.userInteractionEnabled);
}

- (void)layoutSubviews {
    UIView *child, *previousChild;
    GKLinearLayoutViewItem *previousItem;
    NSArray *constraints;
    
    for (NSLayoutConstraint *constraint in self.constraints) {
        if (constraint.firstItem != self.superview ||
            constraint.secondItem != self.superview) {
            [self removeConstraint:constraint];
        }
    }
    
    for (int i = 0; i < self.items.count; i++) {
        GKLinearLayoutViewItem *item = self.items[i];
        child = item.view;
        
        if (i == 0) {
            constraints =
            [child mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.mas_top);
                make.leading.equalTo(self.mas_leading);
            }];
            
        }
        if (self.orientation == GKLinearLayoutViewOrientationVertical) {
            if (i > 0) {
                constraints =
                [child mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.leading.equalTo(self.mas_leading);
                    make.top.equalTo(previousChild.mas_bottom)
                        .offset(item.padding.top);
                }];
            }
            if (i == self.items.count -1) {
                [child mas_updateConstraints:^(MASConstraintMaker *make) {
                   make.bottom.equalTo(self.mas_bottom); 
                }];
            }
        } else if (self.orientation ==GKLinearLayoutViewOrientationHorizontal) {
            if (i == 0) {
                constraints =
                [child mas_updateConstraints:^(MASConstraintMaker *make) {
                   make.bottom.equalTo(self.mas_bottom);
                }];
            }
            if (i > 0) {
                constraints =
                [child mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.leading.equalTo(previousChild.mas_trailing)
                    .offset(previousItem.padding.right + item.padding.left);
                    make.top.equalTo(self.mas_top);
                    make.bottom.equalTo(self.mas_bottom);
                }];
            }
        }
        previousChild = child;
        previousItem = item;
    }
}

- (void)addItem:(GKLinearLayoutViewItem *)item {
    [self.items addObject:item];
    [self addSubview:item.view];
}

- (void)insertItem:(GKLinearLayoutViewItem *)item atIndex:(NSUInteger)index {
    [self.items insertObject:item atIndex:index];
    [self insertSubview:item.view atIndex:index];
}

- (void)removeItem:(GKLinearLayoutViewItem *)item {
    [self.items removeObject:item];
    [item.view removeFromSuperview];
}

GKLinearLayoutItemPadding GKLinearLayoutMakePadding(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right) {
    GKLinearLayoutItemPadding padding;
    padding.top = top;
    padding.left = left;
    padding.bottom = bottom;
    padding.right = right;
    
    return padding;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
