//
//  GKLinearLayoutViewItem.h
//  ShanHuo
//
//  Created by 小悟空 on 7/5/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKLinearLayoutViewOrientation.h"

@interface GKLinearLayoutViewItem : NSObject

@property (strong, nonatomic) UIView *view;
@property (assign, nonatomic) GKLinearLayoutItemPadding padding;
+ (instancetype)layoutItemForView:(UIView *)aView;
@end
