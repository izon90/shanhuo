//
//  GKLinearLayoutViewOrientation.h
//  ShanHuo
//
//  Created by 小悟空 on 7/5/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#ifndef ShanHuo_GKLinearLayoutViewOrientation_h
#define ShanHuo_GKLinearLayoutViewOrientation_h

typedef enum {
    GKLinearLayoutViewOrientationVertical,
    GKLinearLayoutViewOrientationHorizontal
} GKLinearLayoutViewOrientation;

typedef struct {
    CGFloat top;
    CGFloat left;
    CGFloat bottom;
    CGFloat right;
} GKLinearLayoutItemPadding;

#endif
