//
//  GKLinearLayoutViewItem.m
//  ShanHuo
//
//  Created by 小悟空 on 7/5/15.
//  Copyright (c) 2015 GOKU. All rights reserved.
//

#import "GKLinearLayoutViewItem.h"

@implementation GKLinearLayoutViewItem

- (id)initWithView:(UIView *)view {
    self = [self init];
    if (self) {
        self.view = view;
    }
    return self;
}

+ (instancetype)layoutItemForView:(UIView *)aView {
    GKLinearLayoutViewItem *item =
    [[GKLinearLayoutViewItem alloc] initWithView:aView];
    return item;
}
@end
