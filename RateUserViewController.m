//
//  RateUserViewController.m
//  
//
//  Created by heyong on 7/27/15.
//
//

#import "RateUserViewController.h"
#import "HCSStarRatingView.h"
#import "UITextView+Placeholder.h"
#import "SHNetworkEngine.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <SZTextView/SZTextView.h>

@interface RateUserViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet SZTextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton;

@end

@implementation RateUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ratingView.minimumValue = 0;
    self.ratingView.maximumValue = 5;
    self.ratingView.allowsHalfStars = NO;
    self.ratingButton.layer.cornerRadius = 1;
    self.ratingButton.clipsToBounds = YES;
    
    self.textView.placeholder = @"请输入评价";
    [self.ratingButton addTarget:self action:@selector(rating:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)rating:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SHNetworkEngine sharedEngine] rateUserID:@([App shared].currentUser.userID) rank:@(self.ratingView.value) content:self.textView.text block:^(SHUser *user, NSError *error) {
        if (user) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
